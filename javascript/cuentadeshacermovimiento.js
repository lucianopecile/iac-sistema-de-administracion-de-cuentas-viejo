function enviarDatos(form)
{
	form.submit();
}

function cancelarEnvio()
{
    document.location='index.php?controlador=movimiento&&accion=mostrarcuentadeshacermovimiento';
    return true;
}

function cargarDatosCuota(idcuota, fechapago)
{
	if(idcuota > 0){
		if(!esFecha(fechapago)){
			alert("La cuota no tiene movimientos");
			return false;
		}
		var url = "base.php?controlador=movimiento&&accion=deshacermovimiento&&idcuota="+idcuota;
		open(url, "desahcermovimiento", "width=650, height=500, toolbar=no, top=200, left=200");
		return true;
	}else{
		alert("No se seleccionó correctamente la cuota");
		return false;
	}
}
