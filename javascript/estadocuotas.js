function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}


function validarDatos(form)
{
	/*/habilito los valores del form
	form.solicitante.disabled = false;
	form.nroexpediente.disabled = false;
	form.anioexpediente.disabled = false;
	form.nrocuenta.disabled = false;
	form.valorliquidacion.disabled = false;
	form.interes.disabled = false;
	form.total.disabled = false;
	form.cobrado.disabled = false;
	form.saldo.disabled = false;
	form.deudavencida.disabled = false;
	form.deudanovencida.disabled = false;
	form.mora.disabled = false;
	form.deudatotal.disabled = false;
*/
    return true;
}

function cancelarEnvio()
{
    document.location = 'index.php?controlador=consulta&&accion=mostrarcuenta';
    return true;
}

function verMovimiento(idcuota, nrocuota)
{
	var url = "base.php?controlador=consulta&&accion=vermovimiento&&idcuota="+idcuota+"&&nrocuota="+nrocuota;
	document.open(url,"movimiento","width=700, height=400, toolbar=no, top=200, left=200");
	return true;
}

function consultarEstadoDia(form)
{
	var idcuenta = form.idcuenta.value;
	var fecha = form.fechaestado.value;
	if(!esFecha(fecha)){
		alert("La fecha ingresada no es v�lida");
		form.fechaestado.focus();
		return false;
	}
	document.location = "index.php?controlador=consulta&&accion=estadocuenta&&idcuenta="+idcuenta+"&&fechamora="+fecha;
	return true;
}
