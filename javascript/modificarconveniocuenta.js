function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
    if(form.convenio.value == ''){
        alert("Debe ingresar un n�mero de convenio");
        form.convenio.focus();
        return false;
    }
    if(form.cuentacorriente.value == ''){
        alert("Debe ingresar un n�mero de cuenta corriente");
        form.cuentacorriente.focus();
        return false;
    }
    return true;
}

function cancelarEnvio()
{
    document.location = "index.php?controlador=index&&accion=nada";
    return true;
}