function enviarDatos(form)
{
	if (validarDatos(form))
		form.submit();
}
	
function validarDatos(form)
{
	if (!validarApellido(form))
		return(false);
	if (!validarNombre(form))
		return(false);
	if(!validarDoc(form))
		return(false);
	if(!validarLocalidad(form))
		return(false);
	if (!validarFechas(form))
		return(false);
	return(true); 
}

function validarApellido(form)
{
	var i;
	q=form.apellido.value;

	if ((form.apellido.value.length < 1))
	{ 
		alert("Debe ingresar un apellido para el poblador"); 
		form.apellido.focus();
		return (false); 
	} else {
		for (i = 0; i < q.length; i++ )   
			if ( q.charAt(i) != " " )   
				return (true);   

	    alert("No se acepta el apellido"); 
		form.apellido.focus();
		return(false);
	}
}

function validarNombre(form)
{
	var i;
	q=form.nombres.value;

	if ((form.apellido.value.length < 1))
	{ 
		alert("Debe ingresar nombre para el poblador"); 
		form.apellido.focus();
		return (false); 
	} else {
		for ( i = 0; i < q.length; i++ )   
			if ( q.charAt(i) != " " )  
				return (true);
	
		alert("No se acepta el nombre"); 
		form.nombres.focus();
		return(false);
	}
}

function validarDoc(form)
{ 
     var tipodoc=form.idtipodoc.value;
     var documento=form.documento.value;

	 if(tipodoc > 0 && documento > 0){
		 return(true);
     } else {
		 alert("Debe ingresar el n�mero y el tipo de documento"); 
		 form.documento.focus();
		 return(false);
	 }
}

function validarLocalidad(form)
{
	if ((form.idlocalidad.value < 1))
	{ 
		alert("Debe seleccionar la localidad de residencia");
		form.idlocalidad.focus();
		return(false);
	}
	return(true);
}

function validarFechas(form)
{
	fechanac = form.fechanac.value;
	
	if (fechanac == "")
		return true;
	if(esFecha(fechanac))
		return(true);
	else{
		alert("La fecha es incorrecta");
		form.fechanac.focus();
		return(false);
	}
}

function validarPoblador(form)
{
	if ((form.idtipodoc.value < 1))
	{ 
		alert("Debe ingresar el tipo de documento");
		form.idtipodoc.focus();
		return(false);
	}
	if ((form.idlocalidad.value < 1))
	{ 
		alert("Debe seleccionar la localidad de residencia");
		form.idlocalidad.focus();
		return(false);
	}
	if ((form.idestadocivil.value < 1))
	{
		alert("Debe seleccionar el estado civil de la persona");
		form.idestadocivil.focus();
		return(false);
	}
	return(true);
}

function cancelarEnvio(form)
{
	document.location = "./index.php?controlador=poblador&&accion=mostrarpoblador";
	return(true);
}

// CONTROLES PARA EL MANEJO DE CHECKBOXs EN LA VISTA
function comprobarCheck()
{
	var vivepadre;
	var vivemadre;
 
	vivepadre = document.getElementById('vivepadre').value;
	vivemadre = document.getElementById('vivemadre').value;

	if(vivepadre == 1)
	{
		document.getElementById('checkvivepadre').checked="checked";
	}
	if(vivemadre == 1)
	{
		document.getElementById('checkvivemadre').checked="checked";
	} 	
	return(true);
}

function validarCheck(checkbox, campo)
{
	if (checkbox.checked)
		campo.value = 1;
	else
		campo.value = 0;
}


function desvincularConyuge(form)
{
	if (validarDatos(form))
	{
		form.setAttribute("action", "index.php?controlador=conyuge&&accion=desvincularconyuge");
	 	enviarDatos(form);
	}
}

function traerConyuge(form)
{    
	apellido = form.apellido.value;
	idpob = document.getElementById("idpoblador").value;
	url ="base.php?controlador=conyuge&&accion=elegirconyuge&&idpob="+idpob+"&&apellido="+apellido;  
	open(url, "listaconyuge", "width=600, height=400, toolbar=no, top=200, left=200 ");
	return true;
}


function cargarConyuge(idconyuge, idpob)
{    
	document.location = "./index.php?controlador=conyuge&&accion=vincularconyuge&&idconyuge="+idconyuge+"&&idpob="+idpob;
	return true;
}

/* CARGA DE POBLADORES EXTRANJEROS */

function cargarExtranjero(idpob)
{
	url = "base.php?controlador=extranjero&&accion=verextranjero&&idpob="+idpob;
	open(url, "", "width=650, height=600, toolbar=no, top=200, left=200, resizable=0");
	return true;
}

function traerExtranjero(id, carta, fechacarta, juzgado, visadopor, fechallegada, procedencia, medio)
{
	var titulo = "<h4>Datos de arribo</h4>";
	var linea = "<br/><h6></h6>";
	var campos = "<div class='campos'>" +
			"<br /><label>Carta de ciudadan&iacute;a: </label><label id='labelcarta'></label>" +
			"<br /><label>Fecha de emisi&oacute;n: </label><label id='labelfechacarta'></label>" +
			"<br /><label>Juzgado de emisi&oacute;n: </label><label id='labeljuzgado'></label>" +
			"<br />	<label>Visado por: </label><label id='labelvisadopor'></label>" +
			"<br />	<label>Fecha de llegada al pa&iacute;s:</label><label id='labelfechallegada'></label>" +
			"<br />	<label>Procedente desde: </label><label id='labelprocedencia'></label>" +
			"<br /><label>Medio: </label><label id='labelmedio'></label> </div>";
	var inputs = "<input id='carta' name='carta' type='hidden'/>"+
		"<input id='fechacarta' name='fechacarta' type='hidden'/>"+
		"<input id='juzgado' name='juzgado' type='hidden'/>" +
		"<input id='visadopor' name='visadopor' type='hidden'/>" +
		"<input id='fechallegada' name='fechallegada' type='hidden'/>" +
		"<input id='procedencia' name='procedencia' type='hidden'/>" +
		"<input id='medio' name='medio' type='hidden'/>";
	
    document.getElementById("extranjero").innerHTML = linea + titulo + campos + inputs + linea;
    form = document.getElementById("formulario");
    document.getElementById("labelcarta").innerHTML = form.carta.value = carta;
    document.getElementById("labelfechacarta").innerHTML = form.fechacarta.value = fechacarta;
    document.getElementById("labeljuzgado").innerHTML = form.juzgado.value = juzgado;
    document.getElementById("labelvisadopor").innerHTML = form.visadopor.value = visadopor;
    document.getElementById("labelfechallegada").innerHTML = form.fechallegada.value = fechallegada;
   	document.getElementById("labelprocedencia").innerHTML = form.procedencia.value = procedencia;
	document.getElementById("labelmedio").innerHTML = form.medio.value = medio;
	document.getElementById("idextranjero").value = id;
    
    return true;
}


// -- FUNCIONES AJAX --

/* ajax.Request */  
function ajaxRequest(url, data)
{
	var aj = new Ajax.Request(
	url,{
		method:'get',
		parameters: data,
		onComplete: getResponse
		}
	);
}
/* ajax.Response */  
function getResponse(oReq)
{
	$('result1').innerHTML = oReq.responseText;
	$('result2').innerHTML = oReq.responseText;
}  
