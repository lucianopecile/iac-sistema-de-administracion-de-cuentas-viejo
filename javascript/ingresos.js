function enviarDatos(form)
{
    if(validarDatos()){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos()
{
    return true;
}

function cancelarEnvio()
{
	document.location='index.php?controlador=index&&accion=nada';
	return true;
}

function consultarTransacciones(id)
{
	document.location = 'index.php?controlador=consulta&&accion=consultarmovimientos&&idarchivo='+id;
	return true;
}

function consultarTransaccionesExteriores(id,nombre)
{   
    
        var url = "./reports/consultamovimientosarchivoexternos.php?idarchivo="+id+"&&nombrearchivo="+nombre;
	open(url, "", "width=1200, height=800, toolbar=no, top=200, left=200");
	return true;
}

function filtrarListado(form)
{
	if(!validarFechas(form)){
		return false;
	}else{
		var desde = form.fechadesde.value;
		var hasta = form.fechahasta.value;
		document.location = 'index.php?controlador=consulta&&accion=consultararchivosingresos&&fechadesde='+desde+'&&fechahasta='+hasta;
		return true;
	}
}

function validarFechas(form)
{
	var desde = form.fechadesde.value;
	var hasta = form.fechahasta.value;

	if(!esFecha(desde)){
		alert("La fecha desde no es v�lida");
		form.fechadesde.focus();
		return false;
	}
	if(!esFecha(hasta)){
			alert("La fecha hasta no es v�lida");
			form.fechahasta.focus();
			return false;
	}
	if(!fechasiguales(desde, hasta) && compararfechas(hasta, desde)){
		alert("La fecha desde no puede ser posterior a la fecha hasta")
		form.fechadesde.focus();
		return false;
	}
	return true;
}