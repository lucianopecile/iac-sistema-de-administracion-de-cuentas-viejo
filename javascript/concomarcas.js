function moverClaves(form, tipo)
{
	switch(tipo)
	{
		case "seleccionar":
			var comboOrigen = form.vwZonasDisponibles;
			var comboDestino = form.vwZonasElegidas;
			break;
		case "deseleccionar":
			var comboOrigen = form.vwZonasElegidas;
			var comboDestino = form.vwZonasDisponibles;
			break;
	}
	var cantidad = comboOrigen.length;
	for (i = 0; i < cantidad; i++){
		if (comboOrigen.options[i].selected == true){
			// Agregar a Destino
			var mvalor= comboOrigen.options[i].value;
			if (!existeClave(comboDestino,mvalor)){
				var mtexto= comboOrigen.options[i].text;
				var nuevoItem = new Option(mtexto,mvalor);
				comboDestino.options[comboDestino.length] =nuevoItem;
				comboOrigen.options[i] = null;
				cantidad = cantidad -1;
				i = i -1;
			}
		}
	}
}

function existeClave(combo,clave)
{
	var j=0;
	for (j = 0; j <combo.length; j++){
		if (combo.options[j].value==clave)
			return true;
	}
	return false;
}

function enviarDatos(form)
{
	var cantidad = form.vwZonasElegidas.length;
	var i;
	for (i = 0; i < cantidad; i++)
		form.vwZonasElegidas.options[i].selected = true;
	if(validarFechas(form))
		form.submit();
}
	
function cancelarEnvio()
{
	document.location = "./index.php?controlador=index&&accion=nada";
	return true;
}

function validarFechas(form)
{
	var fechadesde = form.fechadesde.value;
	var fechahasta = form.fechahasta.value;

	if(esvacio(fechadesde) && esvacio(fechahasta))
		return true;

	if(!esvacio(fechadesde)){
		if(!esFecha(fechadesde)){
			alert("La fecha desde no es correcta");
			form.fechadesde.focus();
			return false;
		}
	}
	if(!esvacio(fechahasta)){
		if(!esFecha(fechahasta)){
			alert("La fecha hasta no es correcta");
			form.fechahasta.focus();
			return false;
		}
	}
	if(!compararfechas(fechadesde,fechahasta) && (fechadesde!=fechahasta)){
		alert("La fecha hasta no puede ser menor a la fecha desde");
		form.fechahasta.focus();
		return false;
	}
	return true;
}

/*
function realizarConsulta(form)
{
	if(!validarFechas(form))
		return false;
	
	var desde = form.fechadesde.value;
	var hasta = form.fechahasta.value;
	document.location = "index.php?controlador=consulta&&accion=consultarcomarcas&&fechadesde="+desde+"&&fechahasta="+hasta;
	return true;
}*/
