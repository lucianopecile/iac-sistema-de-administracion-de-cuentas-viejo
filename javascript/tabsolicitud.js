function enviarDatos(form)
{
	
   if (validarDatos(form)) {
	 	        document.getElementById('observacionant').disabled=false;
  
       form.submit();
   }
	
  
}
	
function validarDatos(form)
{
	if(validargrado()){
		if(validarpoblador()){
			if (validarfechas(form)){
				if(validarSuperficie(form)){
					if(validarUnidad(form)){
						if(validarLocalidad(form)){
							if(validarExpediente(form)){
								return (true);
							}
						}
					}
				}
			}
		}
	}
	return (false);   
}



function validargrado(){
	
	
	if(parseInt(document.getElementById('idgradotenencia').value)>0){
		return(true);
	}
	else{
		alert("Debe elegir el grado de tenencia ");
	    return(false);
	}
}
function validarlocalidad(){
	
	
	if(parseInt(document.getElementById('idlocalidad').value)>0){
		return(true);
	}
	else{
		alert("Debe elegir la Localidad");
	    return(false);
	}
}

function validarpoblador(){
	
	
	if(parseInt(document.getElementById('idpoblador').value)>0){
		return(true);
	}
	else{
		alert("Debe elegir el Poblador");
	    return(false);
	}
}

function validarSuperficie(form)
{
	if(form.superficie.value > 0)
		return true;
	else{
		alert("Debe ingresar la superficie de la tierra");
		return false;
	}
}

function validarUnidad(form)
{
	if(form.idunidadsol.value > 0)
		return true;
	else{
		alert("Debe seleccionar la unidad de medida de la superfice");
		return false;
	}
}

function validarLocalidad(form)
{
	if(form.idlocalidad.value > 0)
		return true;
	else{
		alert("Debe seleccionar la localidad donde a donde pertenece la solicitud");
		return false;
	}
}

function validarExpediente(form)
{
	if((form.nroexpediente.value > 0) && (form.anioexpediente.value > 0))
		return true;
	else{
		alert("Debe ingresar el n�mero y a�o de expediente");
		return false;
	}
}
	
function validarfechas(form)
{
	fechasol=form.fechasolicitud.value;
		if(esFecha(fechasol)){
		    return(true);
		}
	
       else{
	      alert("La fecha de la solicitud no es correcta");
	      form.fechasolicitud.focus();
          return(false);
   }
}



function cancelarEnvio(form)
{
	
   document.location = "./index.php?controlador=index&&accion=nada";
   return(true);
}



function comprobarCheck()
{
 var residente;
 var empleado;
 
 residente=document.getElementById('residente').value;
 empleado=document.getElementById('empleadopublico').value;

  if(empleado==1){

	  document.getElementById('checkempleadopublico').checked="checked";
	  }
   if(residente==1){
	  document.getElementById('checkresidente').checked="checked";
	  } 	
  
   return(true);
}

function validarCheck(este,valormodif)
{
 
	if (este.checked){
		
	 valormodif.value=1;
	 
    }
	else{
	
	valormodif.value=0;
  }
			
}

function buscarpoblador(form)
{    
     var nombrepob = form.poblador.value;
     form.idpoblador.value=0;
	 form.poblador.value="";
	 form.tipodoc.value="";
	 form.documento.value=0;
	 var dirurl ="base.php?controlador=poblador&&accion=elegirpoblador&&nombrepob="+nombrepob;
	 open(dirurl, "listapobl", "width=450, height=250, toolbar=no, top=200, left=200 ");
   
	return true;
}

function BuscarDatos(idcli,datocli,tdoc,docum,apelnomb)
{    
	 window.document.formulario.idpoblador.value=idcli;
	 window.document.formulario.poblador.value=apelnomb;
	 window.document.formulario.tipodoc.value=tdoc;
	 window.document.formulario.documento.value=docum;
}

/*FUNCIONES AJAX*/

function ajaxRequest(url, data)
{
	var aj = new Ajax.Request(
	url,{
		method:'get',
		parameters: data,
		onComplete: getResponse
		}
	);
}
/* ajax.Response */  
function getResponse(oReq)
{
	var result = oReq.responseText.search("_{true}_");
    //si el nro de expediente no esta repetido result==-1
	if(result != -1)
		if(!confirm("Ya existe un expediente con ese mismo n�mero. \n �Desea usarlo de todos modos?"))
			document.getElementById("nroexpediente").focus();
	return true;
}  





