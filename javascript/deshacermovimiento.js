function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function cancelarEnvio()
{
	self.close();
}

function deshacerUltimoMov(idmov, fechamov, fechamora, fechavenc, interesmora)
{
	// falla si la fecha del procesamiento (fechamov) es menor a la del calculo de mora
	//si (fechamov >= fechamora) OR (fechamov < fechavenc AND fechamora == fechavenc AND intreresmora == 0)
	if((fechasiguales(fechamora, fechamov) || compararfechas(fechamora, fechamov)) ||
		(fechasiguales(fechamora, fechavenc) && compararfechas(fechamov, fechavenc) && interesmora == 0)){
		document.location = "./base.php?controlador=movimiento&&accion=eliminarultimomovimiento&&idmov="+idmov;
		location.reload(true);
		return true;
	}else{
		alert("La cuota ya recibi� modificaciones en los montos de inter�s por morosidad. Ya no es posible deshacer el movimiento.");
		return false;
	}
}