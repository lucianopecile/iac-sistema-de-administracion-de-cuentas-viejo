function enviarDatos(form)
{
	if (validarDatos(form))
		form.submit();

    return false;
}
	
function validarDatos(form)
{
	if(form.cantcuotas.value <= 0){
		alert("La cantidad de cuotas debe ser mayor a 0");
		form.cantcuotas.focus();
		return false;
	}
    if(!validarInteres(form)){
		return false;
    }
	if(!validarLiquidacion(form)){
		return false;
	}
	if(!validarCantidadCuotas(form))
		return false;
	document.getElementById("observacionant").disabled = false;
	return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cuenta&&accion=mostrarajustarcuenta";
	return true;
}

function extraerIdPeriodo(form)
{
	var datosperiodo = new String(form.ListaPeriodo.value);
	var donde = datosperiodo.search("-");
	var largo = datosperiodo.length;

	if(donde == -1){
		alert("no hay '-'");
		return false;
	}else{
		form.idperiodo.value = datosperiodo.substr(0, donde);
		form.cantmeses.value = datosperiodo.substr(donde+1, largo);
		return true;
	}	  
}

function selecionarItemLista(combo, clave)
{   
	var j=0;
	for (j = 0; j < combo.length; j++) {
        if (devolverIdPeriodo(combo[j].value) == clave)
			combo[j].selected=true;
	}
}

function devolverIdPeriodo(idcant)
{
	var datosperiodo = new String(idcant);
	donde=datosperiodo.search("-");
	largo=datosperiodo.length;
	if(donde == -1)
		return(false);
	else
		return(datosperiodo.substr(0,donde));
}

function validarInteres(form)
{
    var valor = form.tasainteres.value.replace(",",".");
    valor *= 1;
    if((valor < 0) || (valor > 10)){
		alert("El interes no puede superar el 10%");
		form.tasainteres.focus();
        return false;
    }
    form.tasainteres.value = valor;
    return true;
}

function validarLiquidacion(form)
{
    var valor = form.valorliquidacion.value;
	var capital_cobrado = form.capitalcobrado.value*1;
	if(valor.indexOf(',') >= 0){
		alert("Este valor no se puede ingresar en formato con ',' (coma)");
		form.valorliquidacion.focus();
		return false;
	}
	valor = valor*1;
	if(!(valor > 0)){
		alert("Debe liquidar la cuenta primero");
		form.valorliquidacion.focus();
		return false;
	}
	if(valor < capital_cobrado){
		alert("El nuevo valor de liquidaci�n no puede ser inferior a lo ya cobrado en las cuotas");
		form.valorliquidacion.focus();
		return false;
	}
    return true;
}

function validarCantidadCuotas(form)
{
	var q = form.cantcuotas.value*1;
	var q_ultima = form.ultimacuotapaga.value*1;
	var q_pagas = form.cantcuotaspagas.value*1;
    var valor_liq = form.valorliquidacion.value*1;
	var capital_cobrado = form.capitalcobrado.value*1;
	if(q <= 0 || q > 120){
		alert("La cantidad de cuotas debe ser mayor a cero y menor a 120");
		form.cantcuotas.focus();
		return false;
	}
	if(q < q_ultima){
		alert("No es posible recalcular la cuenta por una cantidad de cuotas menor a la �ltima cuota con un pago");
		form.cantcuotas.focus();
		return false;
	}
	if(q < q_pagas){
		alert("No es posible reliquidar la cuenta por una cantidad de cuotas menor o igual a la cantidad de cuotas pagadas");
		form.cantcuotas.focus();
		return false;
	}
	//si el nuevo valor de liquidacion es mayor a lo cobrado
	if(valor_liq > capital_cobrado && q == q_pagas){
		alert("No es posible reliquidar la cuenta por una cantidad de cuotas menor o igual a la cantidad de cuotas pagadas");
		form.cantcuotas.focus();
		return false;
	}
	return true;
}
