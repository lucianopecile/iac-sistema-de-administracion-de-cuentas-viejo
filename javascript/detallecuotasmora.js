function enviarDatos(form)
{
	if (validarDatos(form))
	    form.submit();
}
	
function validarDatos()
{
	return true;   
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cuota&&accion=detallecuotasmora";
	return true;
}

function buscarCuenta(form)
{
	var anio = form.anioexpediente.value;
	var nro = form.nroexpediente.value;
    var nrocuenta = form.nrocuenta.value;
	var url = "base.php?controlador=cuenta&&accion=elegircuenta&&anioexpediente="+anio+"&&nroexpediente="+nro+"&&nrocuenta="+nrocuenta;
	open(url, "listacuentas", "width=620, height=250, toolbar=no, top=200, left=200 ");
	return true;
}

function mostrarDatos(idcuenta)
{    
	document.formcuota.idcuenta.value = idcuenta;
	document.location = "./index.php?controlador=cuota&&accion=detallecuotasmora&&idcuenta="+idcuenta;
}

function calcularMora(form)
{
	var idcuenta = form.idcuenta.value;
	var fechacalculo = form.fechamora.value;
    var cuotaincial = form.cuotainicial.value*1;
    var cuotafinal = form.cuotafinal.value*1;
    if (!validarNroCuotas(cuotaincial, cuotafinal)){
        return false;
    }
	if(idcuenta > 0){
        if(!esFecha(fechacalculo)){
            alert("Debe ingresar una fecha válida para el cálculo de la deuda");
            return false;
        }
        document.location = "./index.php?controlador=cuota&&accion=detallecuotasmora&&idcuenta="+idcuenta+"&&cuotainicial="+cuotaincial+"&&cuotafinal="+cuotafinal+"&&fechamora="+fechacalculo;
	}else{
		alert("Debe seleccionar la cuota para calcular el interés");
		return false;
	}
	return true;
}

function validarNroCuotas(cuotaincial, cuotafinal)
{
    if ((cuotaincial < 0) || (cuotafinal < 0)){
        alert("Debe ingresar el n�mero correcto de las cuotas para el cálculo de morosidad");
        return false;
    }
    if(cuotafinal < cuotaincial){
        alert("La cuota inicial no puede ser superior a la cuota final");
        return false;
    }
    return true;
}