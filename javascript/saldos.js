function enviarDatos(form)
{
    if(validarDatos()){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos()
{
    return true;
}

function cancelarEnvio()
{
	document.location='index.php?controlador=index&&accion=nada';
	return true;
}

function consultarSaldosFecha(form)
{
	var fecha = form.fechamora.value;
	if(!esvacio(fecha) && !esFecha(fecha)){
		alert("Debe ingresar una fecha v�lida para el c�lculo de las deudas");
		form.fechamora.focus();
		return false;
	}
	document.location = "index.php?controlador=consulta&&accion=consultasaldos&&fechamora="+fecha;
	return true;
}
