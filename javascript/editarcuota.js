function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
	if(!validarMora(form))
        return false;

	var hoy = diadehoy();
	var fechavto = form.fechavenc.value;
	var fechamora = form.fechacalculomora.value;
	//si la cuota esta vencida le asigno la fecha de hoy, sino le asigno la fecha de vto. o le dejo el �ltimo calculo
	//si la cuota NO esta vencida
	if(fechasiguales(hoy, fechavto) || compararfechas(hoy, fechavto)){
		if(compararfechas(fechamora, fechavto)){
			form.fechacalculomora.value = fechavto;
		}
	}else{	//si ya vencio
		form.fechacalculomora.value = hoy;
	}
	document.getElementById("observacionant").disabled = false;
    return true;
}

function cancelarEnvio(idcuenta)
{
    document.location = "./index.php?controlador=cuota&&vercuotas&&idcuenta="+idcuenta;
    return true;
}

function validarMora(form)
{
	var valor = redondeoCincoCent(form.interesmora.value*1);
	if(valor < 0){
		alert("El inter�s de la cuota no puede ser menor a cero");
        form.interesmora.focus();
		return false;
	}
	form.interesmora.value = valor;
    return true;
}
