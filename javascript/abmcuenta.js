function enviarDatos(form, op)
{
    var operacion = document.getElementById('saveForm').value;
    if(!(operacion == "Eliminar")){
        if (validarDatos(form, op) ){
            form.submit();
        }
    }else{
        form.submit();
    }
}
	
function validarDatos(form, op)
{
    if(form.idsolicitud.value <= 0){
            alert("La cuenta debe estar asociada una solicitud");            
            form.botonbuscarsolicitud.focus();
            return false;
    }
    if(form.nrocuenta.value == ""){
            alert("Debe ingresar el número de cuenta");
            form.nrocuenta.focus();
            return false;
    }
    if(form.porcentajerecupero.value == ""){
            alert("Debe igresar un porcentaje de recupero de mensura, ejemplo: 0%");            
            form.porcentajerecupero.focus();
            return false;
    }
    if(form.formalizacion.value == ''){
            alert("Debe ingresar un porcentaje de formalización!");
            form.formalizacion.focus();
            return false;
    }
    if((form.perdifformalizacion.value < 0)||(form.perdifformalizacion.value == "")){
            alert("Debe ingresar un periodo diferido de facturacion para el plan de formalización!");
            form.perdifformalizacion.focus();
            return false;
    }
    if((form.perdifplan.value < 0)||(form.perdifplan.value == "")){
            alert("Debe ingresar un periodo diferido de facturacion para el plan de cuotas!");
            form.perdifplan.focus();
            return false;
    }
    if((form.cantcuotas.value == '0')||(form.cantcuotas.value == '')){
            alert("Debe ingresar una cantidad de cuotas del plan (minimo 1)!");
            form.cantcuotas.focus();
            return false;
    }
    if(!validarInteres(form)){
        return false;
    }
    if(!validarLiquidacion(form))
        return false;
    if(form.idperiodo.value <= 0){
        alert("Debe seleccionar el periodo de pago de las cuotas");
        form.ListaPeriodo.focus();
        return false;
    }
    if(op == 1){
        if(!validarFechas(form)){
            return false;
        }
    }else{
        //si es una modificaci�n se debe controlar la cantidad de cuotas contra las ya pagadas
        if(!validarCantCuotas(form))
            return false;
    }
    if(!validarFormalizacion(form))
        return false;
    if(!validarCuentaCorriente(form))
        return false;

    document.getElementById("observacionant").disabled = false;
    return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cuenta&&accion=mostrarcuenta";
	return true;
}

function buscarSolicitud(form)
{
	var nro = form.nroexpediente.value;
	var anio = form.anioexpediente.value;
	var url = "base.php?controlador=solicitud&&accion=elegirsolicitudsincuenta&&expediente="+nro+"&&anio="+anio;
	open(url, "listasolicitud", "width=700, height=300, toolbar=no, top=200, left=200");
	return true;
}

function extraerIdPeriodo(form)
{
    var datosperiodo = new String(form.ListaPeriodo.value);
    var donde = datosperiodo.search("-");
    var largo = datosperiodo.length;
   
    if(donde == -1){
        alert("no se selecciono ningun periodo");
        document.getElementById('primervencimientoformalizacion').value = null;
        document.getElementById('primervencimientoplan').value = null;
        return false;
    }else{        
        form.idperiodo.value = datosperiodo.substr(0, donde);
        form.cantmeses.value = datosperiodo.substr(donde+1, largo);
        calculaFechasVencimiento();
        return true;
    }                    
}

function calculaFechasVencimiento(){
    var fecha;
    var cantMeses = parseInt(document.getElementById('cantmeses').value);
    var fechaCuenta = document.getElementById('fechacuenta').value;
    var cantCuotasForm = parseInt(document.getElementById('cantcuotasformalizacion').value);
    var btnCheckFormalizacion = document.getElementById('checkformalizacion');
    var btnCheckCuotasJuntas = document.getElementById('checkcuotasjuntas');
    var periodoDifForm = parseInt(document.getElementById('perdifformalizacion').value);
    var periodoDifPlan = parseInt(document.getElementById('perdifplan').value);

    if (fechaCuenta !== ''){
        if (btnCheckFormalizacion.checked){
            document.getElementById('primervencimientoformalizacion').value = null;
            fecha = sumaFecha(fechaCuenta,cantMeses*periodoDifPlan);
            document.getElementById('primervencimientoplan').value = fecha;
        }else{
            if(periodoDifPlan == 0)
                periodoDifPlan = document.getElementById('perdifplan').value = 1;
            fecha = sumaFecha(fechaCuenta,cantMeses*periodoDifForm);
            document.getElementById('primervencimientoformalizacion').value = fecha;
            if (btnCheckCuotasJuntas.checked){
                document.getElementById('perdifplan').value = 0;
                document.getElementById('perdifplan').disabled = true;
                document.getElementById('primervencimientoplan').value = document.getElementById('primervencimientoformalizacion').value;
            }else{
                document.getElementById('perdifplan').disabled = false;
                //alert("Formalizacion y Plan van en fechas diferentes...");
                //alert("a la FechaFinForm: "+fecha+", le sumo la cantCuotasForm*CantMesesSel ("+ parseInt(cantCuotasForm)+"*"+parseInt(cantMeses)+")");
                //fecha = sumaFecha(document.getElementById('primervencimientoformalizacion').value, (parseInt(cantCuotasForm))*(parseInt(cantMeses)));
                //alert("La ultima cuota de formalizacion es: "+fecha+". A esa fecha, le sumo la multiplicacion del periodoDifPlan*cantMesesSel ("+parseInt(periodoDifPlan)+"*"+parseInt(cantMeses)+")");
                fecha = sumaFecha(fecha, periodoDifPlan * cantMeses * cantCuotasForm);
                document.getElementById('primervencimientoplan').value = fecha;
            }                
        }            
    }else{
        document.getElementById('primervencimientoformalizacion').value = null;
        document.getElementById('primervencimientoplan').value = null;
    }
    return true;
}

function sumaFecha(fechaOld,meses){
    var fechaaux = fechaOld.split('/');
    var dia = parseInt(fechaaux[0],10);
    var mes = parseInt(fechaaux[1],10);
    var anio = parseInt(fechaaux[2],10);
    //alert("dia: "+dia+", mes: "+mes+", anio: "+anio+", Meses a sumar: "+meses);
    var fechaO = new Date(anio,mes,dia); 
    
    switch(meses){
        case 1:
            mes = 1;break;
        case 2:
            mes = 2;break;
        case 3:
            mes = 3;break;
        case 6:
            mes = 6;break;
        case 12:
            mes = 12;break;
        default:
            mes = meses;break;
    }    
    fechaO.setMonth(fechaO.getMonth() + mes);    
    //alert("FechaO->getMonth(): "+fechaO.getMonth());
    if(fechaO.getMonth() === 0){
        //alert("month es 0");
        mes = 12;
        fechaO.setFullYear(anio);
    }else{
        mes = fechaO.getMonth();
    }
    //alert("fechaO: "+dia+'/'+fechaO.getMonth()+'/'+fechaO.getFullYear());
    var newdia; var newmes;
    newdia = (dia<10) ? '0'+dia : dia;    
    newmes = (mes<10) ? '0'+mes : mes;
    return newdia+'/'+newmes+'/'+fechaO.getFullYear();
}

function selecionarItemLista(combo, clave)
{   
    var j=0;
    for (j = 0; j < combo.length; j++) {
    if (devolverIdPeriodo(combo[j].value) == clave)
        combo[j].selected=true;
    }
}

function devolverIdPeriodo(idcant)
{
    var datosperiodo = new String(idcant);
    var donde=datosperiodo.search("-");
    //var largo=datosperiodo.length;
    if(donde == -1)
        return(false);
    else
        return(datosperiodo.substr(0,donde));
}

function cargarSolicitud(id, expediente, anio, solicitante, tipo)
{
	var form = document.getElementById("formulario");
	form.idsolicitud.value = id;
	form.nroexpediente.value = expediente;
	form.anioexpediente.value = anio;
	form.solicitante.value = solicitante;
	form.tipo.value = tipo;
	return true;
}


function liquidarCuenta(form)
{
	var idsol = form.idsolicitud.value;
	var tipo = form.tipo.value;
	var url;
	if(!(idsol > 0)){
		alert("Debe seleccionar primero el expediente para liquidar.");
		return false;
	}
	//caso solicitud rural
	if((tipo == "RURAL") || (tipo == '1')){
		url = "base.php?controlador=liquidacion&&accion=verliquidacionrural&&idsol="+idsol;
	}
	//caso solicitud urbana
	if((tipo == "URBANA") || (tipo == '2')){
		url = "base.php?controlador=liquidacion&&accion=verliquidacionurbana&&idsol="+idsol;
	}
	open(url, "liquidacion", "width=650, height=800, scrollbars=yes, toolbar=no, top=150, left=200");
	return true;
}

function cargarLiquidacion(total, valorhectarea)
{
    var form = document.getElementById("formulario");
    form.valorliquidacion.value = total;
    form.valorvisible.value = total;
    formatomiles(form.valorvisible,'');
    form.valorhectareafinal.value = valorhectarea;
    return true;
}

function validarLiquidacion(form)
{
    var valor = form.valorliquidacion.value;
	var capital_cobrado = form.capitalcobrado.value*1;
	if(valor.indexOf(',') >= 0){
		alert("Este valor no se puede ingresar en formato con ',' (coma)");
		form.valorliquidacion.focus();
		return false;
	}
	valor = valor*1;
	if(!(valor > 0)){
		alert("Debe liquidar la cuenta primero");
		form.valorliquidacion.focus();
		return false;
	}
	if(valor < capital_cobrado){
		alert("El nuevo valor de liquidaci�n no puede ser inferior a lo ya cobrado en las cuotas");
		form.valorliquidacion.focus();
		return false;
	}
    return true;
}

function validarCuentaCorriente(form)
{
	var ctacorriente = form.cuentacorriente.value;
	if(esvacio(ctacorriente)){
		alert("Debe ingresar la Cuenta Bancaria para el pago de cuotas"); 
		form.cuentacorriente.focus();
        return false; 
	}
	q = ignoreSpaces(ctacorriente);
	if (!esDigitoN(q.charAt(q.length-1))){
		 alert("El número de cuenta debe terminar en un digito");
 		 form.cuentacorriente.focus();
 		 return false; 
	}
	return true;
}

function validarFechas(form)
{
    var fechaC = form.fechacuenta.value;
    if(!esFecha(fechaC)){
        alert("Debe ingresar una fecha de cuenta válida");
        form.fechacuenta.focus();
        return false;
    }
    var fechaV = form.primervencimientoplan.value;
    if(!esFecha(fechaV)){
        alert("Debe ingresar una fecha de primer vencimiento del plan válida");
        form.primervencimiento.focus();
        return false;
    }
    if(!compararfechas(fechaC, fechaV)){
        alert("La fecha de la cuenta no puede ser superior al primer vencimiento de las cuotas");
        form.primervencimientoplan.focus();
        return false;
    }
    return true;
}

function validarInteres(form)
{
    var valor = form.tasainteres.value.replace(",",".");
    valor *= 1;
    if((valor < 0) || (valor > 100)){
		alert("El interes no puede superar el 10%");
		form.tasainteres.focus();
        return false;
    }
    form.tasainteres.value = valor;
    return true;
}

function validarFormalizacion(form)
{
    var valor = form.formalizacion.value.replace(",",".");
    valor *= 1;
    if(valor != 0){
        if((valor < 10) || (valor > 99)){
            alert("El porcentaje de formalizaci�n debe ser mayor al 10% o cero");
            form.formalizacion.focus();
            return false;
        }
    }
    form.formalizacion.value = valor;
    return true;
}

function validarCantCuotas(form)
{
	var q = form.cantcuotas.value*1;
	var q_ultima = form.ultimacuotapaga.value*1;
	var q_pagas = form.cantcuotaspagas.value*1;
    var valor_liq = form.valorliquidacion.value*1;
	var capital_cobrado = form.capitalcobrado.value*1;
	if(q <= 0 || q > 120){
		alert("La cantidad de cuotas debe ser mayor a cero y menor a 120");
		form.cantcuotas.focus();
		return false;
	}
	if(q < q_ultima){
		alert("No es posible recalcular la cuenta por una cantidad de cuotas menor a la �ltima cuota con un pago");
		form.cantcuotas.focus();
		return false;
	}
	if(q < q_pagas){
		alert("No es posible reliquidar la cuenta por una cantidad de cuotas menor o igual a la cantidad de cuotas pagadas");
		form.cantcuotas.focus();
		return false;
	}
	//si el nuevo valor de liquidacion es mayor a lo cobrado
	if(valor_liq > capital_cobrado && q == q_pagas){
		alert("No es posible reliquidar la cuenta por una cantidad de cuotas menor o igual a la cantidad de cuotas pagadas");
		form.cantcuotas.focus();
		return false;
	}
	return true;
}

function formatomiles(objeto,prefix)
{
    var num = objeto.value*1;
    prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? ',' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while(regx.test(splitLeft)){
        splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
    }
    objeto.value=prefix + '  '+splitLeft + splitRight;
}

function modificarObservacion(form)
{
	var idcuenta = form.idcta.value;
	var obs = form.observacion.value;
	if(obs == "")
		return false;
	document.location = "./index.php?controlador=cuenta&&accion=modificarobservacion&&idcuenta="+idcuenta+"&&observacion="+obs;
	return true;
}

/* FUNCIONES AJAX */
function ajaxRequest(url, data)
{
	var aj = new Ajax.Request(
	url,{
		method:'get',
		parameters: data,
		onComplete: getResponse
		}
	);
}
/* ajax.Response */  
function getResponse(oReq)
{
	$('result1').innerHTML = oReq.responseText;
}  

//verifica si esta tildado el check
function sinFormalizacion(idInput)
{
    var inputFormalizacion = document.getElementById(idInput);
    var btnCheck = document.getElementById('checkformalizacion');
    var inputCuotasFormalizacion = document.getElementById('cantcuotasformalizacion');    
    
    if (btnCheck.checked){
        inputFormalizacion.value = '0,00';
        inputFormalizacion.disabled = true;
        inputCuotasFormalizacion.disabled = true;
        inputCuotasFormalizacion.value = 0;
        document.getElementById('perdifformalizacion').value = 0;
        calculaFechasVencimiento();
    }else{
        inputFormalizacion.value = '';
        inputFormalizacion.disabled = false;        
        inputCuotasFormalizacion.disabled = false;
        inputCuotasFormalizacion.value = 1;
        document.getElementById('perdifformalizacion').value = 1;
        calculaFechasVencimiento();
        inputFormalizacion.focus();
    }        
}

//verifica si hay formalizacion y es mayor a 0
function conFormalizacion(idInput)
{
    var inputFormalizacion = document.getElementById(idInput);
    var btnCheck = document.getElementById('checkformalizacion');
    var inputCuotasFormalizacion = document.getElementById('cantcuotasformalizacion');
    
    if(inputFormalizacion.value > 0){                
        btnCheck.checked = false;
        inputCuotasFormalizacion.disabled = false;        
    }else{
        btnCheck.checked = true;
        inputCuotasFormalizacion.disabled = true;        
    }        
}

//verifica si esta tildado el check del cobro de cuotas de formalizacion y plan
function cuotasParalelas()
{    
    var btnCheck = document.getElementById('checkcuotasjuntas');    
    
    if (btnCheck.checked){
        document.getElementById('perdifplan').value = 0;
        document.getElementById('perdifplan').disabled = true;
        document.getElementById('primervencimientoplan').value = document.getElementById('primervencimientoformalizacion').value;
    }else{
        calculaFechasVencimiento();
    }
}

function sumarintereses()
{
    var check = document.getElementById('sumarinteres');
        
    check.value = 0;
    if(check.checked){
        check.value = 1;
    }
}
