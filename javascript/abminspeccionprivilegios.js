function comprobarCheck()
{
	var espejoagua = document.getElementById('espejoagua').value;
	var rioarroyo = document.getElementById('rioarroyo').value;
	var altascumbres = document.getElementById('altascumbres').value;
	var centrourbano = document.getElementById('centrourbano').value;
	var accesos = document.getElementById('accesos').value;
		
	if(espejoagua == 1)
		document.getElementById('checkespejosagua').checked = "checked";
	if(rioarroyo == 1)
		document.getElementById('checkrioarroyo').checked = "checked";
	if(altascumbres == 1)
		document.getElementById('checkaltascumbres').checked = "checked";
	if(centrourbano == 1)
		document.getElementById('checkcentrourbano').checked = "checked";
	if(accesos == 1)
		document.getElementById('checkaccesos').checked = "checked";
	return(true);
}

function validarCheck(este, valormodif)
{
	if (este.checked)
		valormodif.value = 1;
	else
		valormodif.value = 0;
}

function enviarDatos(form)
{
	if (validarDatos()) 
	{
		form.observacionant.disabled = false;
		form.submit();
	}
}

function validarDatos()
{
	return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=inspeccionprivilegios&&accion=mostrarinspeccion";
	return true;
}

function verValoresTierra()
{
	url = "base.php?controlador=valortierra&&accion=popupvalortierra";
	open(url, "", "width=950, height=550, toolbar=yes, top=200, left=200 ");
	return true;
}

function cargarValores(valor)
{
	form = document.getElementById("formulario");
	form.valorhectarea.value = valor;
	formatLabelNumber(valor, '$', "labelvalor");
	return true;
}

function formatLabelNumber(num, prefix, label)
{
	prefix = prefix || '';
	num += '';
	var splitStr = num.split('.');
	var splitLeft = splitStr[0];
	var splitRight = splitStr.length > 1 ? ',' + splitStr[1] : '';
	var regx = /(\d+)(\d{3})/;

	while (regx.test(splitLeft))
		splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');

	document.getElementById(label).innerHTML = prefix + '  '+splitLeft + splitRight;
}

function comprobarValoresTierra()
{
	form = document.getElementById("formulario");
	valor = form.valorhectarea.value;
	formatLabelNumber(valor, '$', "labelvalor");
	return true;
}

function comprobarSuperficie()
{
	var sup = document.getElementById("superficie").value;
	document.getElementById("sup").innerHTML = comprobar(sup);
	var ver = document.getElementById("supveranada").value;
	document.getElementById("veranada").innerHTML = comprobar(ver);
	var inv = document.getElementById("supinvernada").value;
	document.getElementById("invernada").innerHTML = comprobar(inv);
	return true;
}

function comprobar(valor)
{
	if(valor == 0){
		return 0;
	}else{
		var arr = valor.split('.');
		var nuevo_valor = arr[0];
		nuevo_valor = nuevo_valor+'-'+arr[1].substring(0,2)+'-'+arr[1].substring(2,4)+'-'+arr[1].substring(4);
		return nuevo_valor;
	}
}
