<?php


// OPERACIONES 

define ('ALTA', 1);  
define ('MODIFICAR', 2);  
define ('BAJA', 3);  
define ('VER', 4);


// TIPO DE SOLICITUDES
define ('RURAL', 1);  
define ('URBANA', 2);

//ESTADO DE ARCHIVO DE COBROS
define('EXITO', "Éxito");
define('ERRORES',"Con errores");
define('ANULADO',"Anulado");

//TIPO DE COBRO O DE REGISTRO DE MOVIMIENTO
define('MANUAL', 1);
define('ELECTRONICO', 2);

//TIPO DE MOVIMIENTO
define('NORMAL', 1);
define('PARCIAL', 2);
define('GLOBAL', 3);
define('DESCUENTO', 4);

//CERO DESCARTANDO DECIMALES MENOR A 0.1
define('CERO_DECIMAL', 0.05);
?>