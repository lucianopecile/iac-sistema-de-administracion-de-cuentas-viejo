<?php
class FrontController
{
	static function main()
	{
		//Incluimos algunas clases:
       
		require 'config.php'; //Archivo con configuraciones.
 
		
		if(! empty($_GET['controlador'])){
		    $control=$_GET['controlador'];
			$controllerName = 'control'.$_GET['controlador'];
		}	  
		else{
		     $control="accesolibre";
		     $controllerName = "controlaccesolibre";
		}	
 
		//Lo mismo sucede con las acciones, si no hay accion, tomamos nada como accion
		if(! empty($_GET['accion'])){
		      $accion= $_GET['accion'];
		      $actionName = $_GET['accion'];
		}	  
		else{
		     $accion="nada";
		     $actionName = "nada";
			 
        }
		$controllerPath = $config->get('controllersFolder') . $controllerName . '.php';
		
		//Incluimos el fichero que contiene nuestra clase controladora solicitada	
		if(is_file($controllerPath))
		      require $controllerPath;
		else{
		      $view = new View();
			  $mensaje= "El controlador no existe - 404 not found";
	          $data['mensaje']=$mensaje;
    	      $view->show1("mostrarerror.html", $data);
		      return;
        }

		if (is_callable(array($controllerName, $actionName)) == false) 
		{
			trigger_error ($controllerName . '->' . $actionName . '` no existe', E_USER_NOTICE);
			return false;
		}
		$comparar=trim( $controllerName);
		$nuevocontrol=$control;
		
		switch($nuevocontrol)
		{
			case "conyuge":
				$nuevocontrol="poblador";
				break;
			case "hijo":
				$nuevocontrol="poblador";
				break;
			case "extranjero":
				$nuevocontrol="poblador";
				break;
			case "solicitudurbana":
				$nuevocontrol="solicitud";
				break;
			case "solicitudrural":
				$nuevocontrol="solicitud";
				break;
			case "arrendamiento":
				$nuevocontrol="solicitud";
				break;
			case "tierraurbana":
				$nuevocontrol="solicitud";
				break;
			case "tierrarural":
				$nuevocontrol="solicitud";
				break;
			case "cuota":
				$nuevocontrol="cuenta";
				break;
			case "liquidacion":
				$nuevocontrol="cuenta";
				break;
			case "movimiento":
				$nuevocontrol="cuota";
				break;
			case "valortierra":
				$nuevocontrol="cuenta";
				break;
			default:
				$nuevocontrol=$control;
		}
		
		
		
		
		
		//Si todo esta bien, creamos una instancia del controlador y llamamos a la accion
	  if ((isset($_SESSION['s_username']))|| ($comparar=='controlaccesolibre')){
	       $parPerfil=$_SESSION["perfilusuario"];
    	   if (is_array($parPerfil)) {
  		    $tieneacceso=false;
		   	foreach($parPerfil as $obj)
		    { 
			
			  $IdUsr=$obj['id'];
			  $nombrecontrolador=$obj['controlador'];
			  $nombreaccion=$obj['accion'];
         	  if (!$tieneacceso){
              //  chequea si el controlador esta en el perfil del usuario o si el controlador es para cosultar el movimiento de una cuota y tiene acceso de tipo central
	               if($nuevocontrol==$nombrecontrolador)	
	  	              $tieneacceso=true;
		            else
			          $tieneacceso=false;
		     }
		     }
			}
		if ($tieneacceso  || ($comparar=='controlaccesolibre')){
       
		        $controller = new $controllerName();
		        $controller->$actionName();
		}
		else{
		      $view = new View();
			  $mensaje= "No tiene privilegios para realizar esta operacion";
	          $data['mensaje']=$mensaje;
    	      $view->show1("mostrarerror.html", $data);
		      return;
        }
		 
			  
		}
		
		
	}
}
?>