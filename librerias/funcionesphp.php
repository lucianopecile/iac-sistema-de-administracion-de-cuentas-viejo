<?PHP
function menor($n1,$n2)
{
if ($n1>$n2) return $n2; else return $n1;
}
function puntos_cm($medida,$resolucion=72)
{
return ($medida/(2.54))*$resolucion;
}


function array_columnas( $arr )
{
	if(($cantArg=func_num_args()) >1) {
		$ret=array();
		$arg_list = func_get_args();
		foreach($arr as $a) {
			$aux=array();
			for ( $i = 1; $i < $cantArg; $i++ ) {
				 $aux[] = $a[ $arg_list[$i] ];
			}
			$ret[] = $aux;
		}
		return $ret;
	} else {
		return array();
	}
}

function adate($parfecha)
{
        if( empty($parfecha) ) { 
			$auxFecha='null';
		} else {
			list($y,$m,$d) = split( '[/-]', $parfecha);
			$auxFecha = sprintf("%04s",$y).'-'.sprintf("%02s",$m).'-'.sprintf("%02s",$d);
		}
		return $auxFecha;
}

function fechaACadena($parfecha)
{
        if( empty($parfecha) ) 
		{ 
			$auxFecha='';
		} 
		else 
		{
			list($y,$m,$d,$h,$n,$s) = split( "[- :]", $parfecha);
			if (($y=="0000") && ($m=="00") && ($d=="00"))
				$auxFecha='';
			else
    			$auxFecha = sprintf("%02s",$d).'/'.sprintf("%02s",$m).'/'.sprintf("%04s",$y);
		}
		return $auxFecha;
}

function cadenaAFecha($parCadena)
{
    $parCadena=trim($parCadena);
	$aux="";
        if( !empty($parCadena) ) 
		{
			list($d,$m,$y) = explode( "/", $parCadena);
			$aux = sprintf("%04s",$y).'-'.sprintf("%02s",$m).'-'.sprintf("%02s",$d);
		}
		return $aux;
}

function diasdelmes($mes,$anio) 
{
   $mes=$mes*1;
  
    $arraymeses= array('0','31','29','31','30','31','30','31','31','30','31','30','31');
	
	$ultimo=0;

if ($mes==2){
    $valid = checkdate($mes,29,$anio);
    if(!$valid){
	  
	  $ultimo=28;
	  }
}	
 if($ultimo==0){
	    $ultimo=$arraymeses[$mes];
		}
  return($ultimo);


}

//=======================================================================================
 function sumaDia($fecha,$dia){
	if($fecha==0){
	   return 0;
	}   
	    // suma $dia dias a una fecha y retorna la fecha resultante
    	list($year,$mon,$day) = explode('-',$fecha);

		return date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));	
	}
	
	
	
	
function is_date($f) 
{
	if(empty($f)) 
	{
		return false;		
	} 
	elseif ( ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $f ) ) 
	{
		$a=strpos($f,'/');
		$b=strrpos($f,'/');
		if($a>0 && $a<$b) 
		{
			$d=substr($f,0,$a);
			$m=substr($f, $a+1, $b-($a+1));
			$y=substr($f,$b+1);
			if(is_numeric($d) && is_numeric($m) && is_numeric($y)) 
			{
				return checkdate($m,$d,$y);
			} 
			else 
			{
				return false;
			}
		}
		return false;
	} 
	else 
	{
		return false;
	}
}


function comparar_fechas($fecha1,$fecha2)
//si retorna mayor que 0 fecha 1 es mayor a fecha 2
{
	if(preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))
		list($dia1,$mes1,$a�o1)=split("/",$fecha1);

	if(preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
		list($dia1,$mes1,$a�o1)=split("-",$fecha1);

	if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
		list($dia2,$mes2,$a�o2)=split("/",$fecha2);

	if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
		list($dia2,$mes2,$a�o2)=split("-",$fecha2);

	$dif = mktime(0,0,0,$mes1,$dia1,$a�o1) - mktime(0,0,0, $mes2,$dia2,$a�o2);
	return ($dif);
}



// is it a timestamp?
function is_ts($f) {
	$re="/^(?:(?:(?:(?:[1-2][0-9]{3}) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12]";
	$re.="[0-9]|3[01]|0?[1-9]))|(?:(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]) *(?:";
	$re.="[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\.";
	$re.=", ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?i:(?:";
	$re.="j(?:an(?:uary)?|u(?:ne?|ly?)))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember";
	$re.=")?|feb(?:ruary)?|sep(?:tember)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[12][0-9]|3[01]|0?[1-9]";
	$re.=")|(?:(?i:[23]?1st|2?2nd|2?3rd|[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?:[";
	$re.="0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd|2?3rd|";
	$re.="[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?|ly?";
	$re.=")))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:temb";
	$re.="er)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3}))))|(?:(?:(?:(?:[1-2]";
	$re.="[0-9]{3}) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]))|(";
	$re.="?:(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\., ]) *(?:(?:[0-9";
	$re.="]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]";
	$re.=") *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?";
	$re.="|ly?)))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:";
	$re.="tember)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd";
	$re.="|2?3rd|[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9";
	$re.="]{3})))|(?:(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd|2?3rd|[4-9]th|1[0-9]th|20th|2";
	$re.="[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?|ly?)))|a(?:pr(?:il)?|ug(?:";
	$re.="ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:tember)?|oct(?:ober)?)) *(?";
	$re.=":[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))) *(?:(?:(?:1[0-2]|0?[1-9])(?: *(?:\:) *(?";
	$re.=":[1-5][0-9]|0?[0-9]))?(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))? *(?:(?i:[ap]m)))|(?:(?:2[0-3]|[01";
	$re.="]?[0-9])(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))?))))$/";
	return preg_match( $re, $f ) ;
}

function diasrendicion($tiempo,$periodo) 
{
  if($periodo==12){
     return($tiempo*365);
  }
  else{
      return($tiempo*30);
  }

}
function nombremes($parfecha){
	
    list($year,$mon,$day) = explode('-',$fechacontrato);

	switch ($mon){
	case 1 :
	       return("enero");
		   break;
	
	case 2 :
	       return("febrero");
		   break;
	case 3 :
	       return("marzo");
		   break;
	
	case 4 :
	       return("abril");
		   break;
    case 5 :
	       return("mayo");
		   break;		   
    case 6 :
	       return("junio");
		   break;			   

    case 7 :
	       return("julio");
		   break;	
	case 8 :
	       return("agosto");
		   break;	
	case 9 :
	       return("septiembre");
		   break;	
	case 10 :
	       return("octubre");
		   break;	
	case 11 :
	       return("noviembre");
		   break;	
	default :
	       return("diciembre");
	}	
	
}



function num2letras($num, $fem = true, $dec = true) { 
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande"); 
   $matuni[2]  = "dos"; 
   $matuni[3]  = "tres"; 
   $matuni[4]  = "cuatro"; 
   $matuni[5]  = "cinco"; 
   $matuni[6]  = "seis"; 
   $matuni[7]  = "siete"; 
   $matuni[8]  = "ocho"; 
   $matuni[9]  = "nueve"; 
   $matuni[10] = "diez"; 
   $matuni[11] = "once"; 
   $matuni[12] = "doce"; 
   $matuni[13] = "trece"; 
   $matuni[14] = "catorce"; 
   $matuni[15] = "quince"; 
   $matuni[16] = "dieciseis"; 
   $matuni[17] = "diecisiete"; 
   $matuni[18] = "dieciocho"; 
   $matuni[19] = "diecinueve"; 
   $matuni[20] = "veinte"; 
   $matunisub[2] = "dos"; 
   $matunisub[3] = "tres"; 
   $matunisub[4] = "cuatro"; 
   $matunisub[5] = "quin"; 
   $matunisub[6] = "seis"; 
   $matunisub[7] = "sete"; 
   $matunisub[8] = "ocho"; 
   $matunisub[9] = "nove"; 

   $matdec[2] = "veint"; 
   $matdec[3] = "treinta"; 
   $matdec[4] = "cuarenta"; 
   $matdec[5] = "cincuenta"; 
   $matdec[6] = "sesenta"; 
   $matdec[7] = "setenta"; 
   $matdec[8] = "ochenta"; 
   $matdec[9] = "noventa"; 
   $matsub[3]  = 'mill'; 
   $matsub[5]  = 'bill'; 
   $matsub[7]  = 'mill'; 
   $matsub[9]  = 'trill'; 
   $matsub[11] = 'mill'; 
   $matsub[13] = 'bill'; 
   $matsub[15] = 'mill'; 
   $matmil[4]  = 'millones'; 
   $matmil[6]  = 'billones'; 
   $matmil[7]  = 'de billones'; 
   $matmil[8]  = 'millones de billones'; 
   $matmil[10] = 'trillones'; 
   $matmil[11] = 'de trillones'; 
   $matmil[12] = 'millones de trillones'; 
   $matmil[13] = 'de trillones'; 
   $matmil[14] = 'billones de trillones'; 
   $matmil[15] = 'de billones de trillones'; 
   $matmil[16] = 'millones de billones de trillones'; 

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 
   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' coma'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' cero'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' una' : ' un'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'Cero ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'una'; 
         $subcent = 'as'; 
      }else{ 
         $matuni[1] = $neutro ? 'un' : 'uno'; 
         $subcent = 'os'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
         $t = ' ciento' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' mil'; 
         }elseif ($num > 1){ 
            $t .= ' mil'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . '?n'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ones'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   return ucfirst($tex); 
} 

function redondeoCincoCent($valor)
{
	return(floor(20*$valor))/20;
	//return floor($value * 20) / 20;
}

function orderMultiDimensionalArray($toOrderArray, $field, $inverse = false)
//$toOrderArray -> Array a ordenar
//$field -> Campo del array por el que queremos ordenarlo (entre comillas).
//$inverse -> Su valor ser� true o false. El valor true lo ordenar� de manera descendente y el false (valor por defecto)
//lo ordenar� de manera ascendente.
{
	$position = array();
	$newRow = array();
	foreach ($toOrderArray as $key => $row)
	{
		$position[$key]  = $row[$field];
		$newRow[$key] = $row;
	}
	if ($inverse)
	{
		arsort($position);
	}else{
		asort($position);
	}
	$returnArray = array();
	foreach ($position as $key => $pos)
	{
		$returnArray[] = $newRow[$key];
	}
	return $returnArray;
}


?>