<?php


class ModeloValorTierra
{
		private $intIdValorTierra;
		private $txtReceptividad; 				
		private $intDistancia50;			
		private $intDistancia100;			
		private $intDistancia150;				
		private $intDistancia200;
		private $intDistancia250;	
		private $intDistancia300;		
		private $intDistancia350;		
		private $intDistanciamas350;



    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
	
// ------------------------------------------------------------------------------------
	
    public function getIdValortierra()
	{
	    return $this->intIdValorTierra;
	} 

    public function putIdValortierra($parIdValortierra)
	{
	    $this->intIdValorTierra = $parIdValortierra;
	} 

// ------------------------------------------------------------------------------------

    public function getIdTipodoc()
	{
	    return $this->intIdTipodoc;
	} 

    public function putIdTipodoc($parIdTipodoc)
	{
	    $this->intIdTipodoc = $parIdTipodoc;
	}
	
// ------------------------------------------------------------------------------------

    public function getReceptividad()
	{
	    return $this->txtReceptividad;
	} 

    public function putReceptividad($parReceptividad)
	{
	    $this->txtReceptividad = $parReceptividad;
	} 


	
// ------------------------------------------------------------------------------------

    public function getDistancia50()
	{
	    return $this->intDistancia50;
	} 

    public function putDistancia50($parDistancia50)
	{
	    $this->intDistancia50 = $parDistancia50;
	}
// ------------------------------------------------------------------------------------

    public function getDistancia100()
	{
	    return $this->intDistancia100;
	} 

    public function putDistancia100($parDistancia100)
	{
	    $this->intDistancia100 = $parDistancia100;
	}

// ------------------------------------------------------------------------------------

    public function getDistancia150()
	{
	    return $this->intDistancia150;
	} 

    public function putDistancia150($parDistancia150)
	{
	    $this->intDistancia150 = $parDistancia150;
	}

// ------------------------------------------------------------------------------------

    public function getDistancia200()
	{
	    return $this->intDistancia200;
	} 

    public function putDistancia200($parDistancia200)
	{
	    $this->intDistancia200 = $parDistancia200;
	}

// ------------------------------------------------------------------------------------

    public function getDistancia250()
	{
	    return $this->intDistancia250;
	} 

    public function putDistancia250($parDistancia250)
	{
	    $this->intDistancia250 = $parDistancia250;
	}
// ------------------------------------------------------------------------------------

    public function getDistancia300()
	{
	    return $this->intDistancia300;
	} 

    public function putDistancia300($parDistancia300)
	{
	    $this->intDistancia300 = $parDistancia300;
	}
	
// ------------------------------------------------------------------------------------

    public function getDistancia350()
	{
	    return $this->intDistancia350;
	} 

    public function putDistancia350($parDistancia350)
	{
	    $this->intDistancia350 = $parDistancia350;
	}
// ------------------------------------------------------------------------------------

    public function getDistanciamas350()
	{
	    return $this->intDistanciamas350;
	} 

    public function putDistanciamas350($parDistanciamas350)
	{
	    $this->intDistanciamas350 = $parDistanciamas350;
	}

//====================================================================================================
	
	public function listadototal() 
    //retorna la consulta de todos los valores de la tierra
	{
    	$query = ('SELECT id, receptividad, distancia50, distancia100, distancia150, distancia200, distancia250, distancia300, distancia350, distanciamas350 FROM valorestierras ORDER BY id');
	
    	$result_all= mysql_query($query);
      
		while ($varvt = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrValorestierras[] = array("id"=>$varvt->id,
			                        "receptividad"=>$varvt->receptividad,
 									"distancia50"=>$varvt->distancia50,
 									"distancia100"=>$varvt->distancia100,
									"distancia150"=>$varvt->distancia150,
									"distancia200"=>$varvt->distancia200,
									"distancia250"=>$varvt->distancia250,
									"distancia300"=>$varvt->distancia300,
									"distancia350"=>$varvt->distancia350,
									"distanciamas350"=>$varvt->distanciamas350
												
							 	);
		} 
		return($arrValorestierras);	
	}
	


//==================================================================================================== 
	
	public function traervalortierra()
	//retorna el valor de la tierra  a partir de un id 
	{
		$query = ("SELECT id, receptividad, distancia50, distancia100, distancia150, distancia200, distancia250, distancia300, distancia350, distanciamas350 FROM valorestierras WHERE valorestierras.id = '$this->intIdValorTierra'");
	     
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}
	
//====================================================================================================
	
	public function borrarvalortierra()
	{	
		$query=("DELETE FROM valorestierras WHERE id = '$this->intIdValorTierra'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
	   
     	
//====================================================================================================	 

	public function modificarvalortierra()
	{
		$query = ("UPDATE valorestierras SET receptividad='$this->txtReceptividad', distancia50='$this->intDistancia50', distancia100='$this->intDistancia100',distancia150='$this->intDistancia150', distancia200='$this->intDistancia200',distancia250='$this->intDistancia250',distancia300='$this->intDistancia300',distancia350='$this->intDistancia350',distanciamas350='$this->intDistanciamas350' WHERE id = '$this->intIdValorTierra'");
		
		$result_all = mysql_query($query);
		return($result_all );
	}



//==================================================================================================== 

	public function altavalortierra()
	{
		$query = ("INSERT INTO valorestierras (receptividad, distancia50, distancia100, distancia150, distancia200, distancia250, distancia300, distancia350, distanciamas350) VALUES ('$this->txtReceptividad','$this->intDistancia50', '$this->intDistancia100','$this->intDistancia150','$this->intDistancia200','$this->intDistancia250','$this->intDistancia300','$this->intDistancia350','$this->intDistanciamas350')");
		$result_all = mysql_query($query);
	    return($result_all);
	}
	
//====================================================================================================
	
	public function listadoReciptividad() 
    //retorna la consulta de las posibles cantidades de reciptividad de las tierras
	{
    	$query = ('SELECT id, receptividad FROM valorestierras ORDER BY id');
	
    	$result_all= mysql_query($query);
      
		while ($varvt = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrReciptividad[] = array("id"=>$varvt->id,
			                        "receptividad"=>$varvt->receptividad	);
		} 
		return($arrReciptividad);	
	}
  
//==================================================================================================== 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdValorTierra($cons->id);
			$this->putReceptividad($cons->receptividad);
			$this->putDistancia50($cons->distancia50);
			$this->putDistancia100($cons->distancia100);
			$this->putDistancia150($cons->distancia150);
			$this->putDistancia200($cons->distancia200);
			$this->putDistancia250($cons->distancia250);
			$this->putDistancia300($cons->distancia300);
			$this->putDistancia350($cons->distancia350);
			$this->putDistanciamas350($cons->distanciamas350);

		}
	}
	


}
?>