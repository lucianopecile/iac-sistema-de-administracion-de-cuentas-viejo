<?php


class ModeloMovimientoExterno
{
   
        private $intId;
        private $intIdMovimientoExterno;
	private $intCobrado;
	private $fecFecha;
        private $fecFechaCobro; 
	private $intIdTipomov;
	private $intIdArchivo;

    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	

// ------------------------------------------------------------------------------------

	public function __construct()
	{
		$this->db_connect();
	}
// ------------------------------------------------------------------------------------

    public function getId()
	{
	    return $this->intId;
	}

    public function putId($parId)
	{
	    $this->intId = $parId;
	} 

// ------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------

    public function getIdMovimientoExterno()
	{
	    return $this->intIdMovimientoExterno;
	}

    public function putIdMovimientoExterno($parIdMovimientoExterno)
	{
	    $this->intIdMovimientoExterno = $parIdMovimientoExterno;
	} 

// ------------------------------------------------------------------------------------

    public function getCobrado()
	{
	    return $this->intCobrado;
	}

    public function putCobrado($parCobrado)
	{
	    $this->intCobrado = $parCobrado;
	}

// ------------------------------------------------------------------------------------

    public function getFecha()
	{
	    return $this->fecFecha;
	}  
    public function putFecha($parFecha)
	{
	    $this->fecFecha = $parFecha;
	}
// ------------------------------------------------------------------------------------

    public function getFechaCobro()
	{
	    return $this->fecFechaCobro;
	}  
    public function putFechaCobro($parFechaCobro)
	{
	    $this->fecFechaCobro = $parFechaCobro;
	}

// ------------------------------------------------------------------------------------

    public function getIdArchivo()
	{
	    return $this->intIdArchivo;
	}

    public function putIdArchivo($parIdArchivo)
	{
	    $this->intIdArchivo = $parIdArchivo;
	}

     
        
// ------------------------------------------------------------------------------------

    public function getDetalleBoleta()
	{
	    return $this->txtDetalleBoleta;
	}

    public function putDetalleBoleta($parDetalleBoleta)
	{
	    $this->txtDetalleBoleta = $parDetalleBoleta;
	}
// ------------------------------------------------------------------------------------

    public function getIdTipomov()
	{
	    return $this->intIdTipomov;
	}

    public function putIdTipomov($parIdTipomov)
	{
	    $this->intIdTipomov = $parIdTipomov;
	}

// ------------------------------------------------------------------------------------

//----------------------------------------------------------
	public function TraerTodos()
    //retorna la consulta de todos los movimientos sobre las cuotas
	{
		$query =('SELECT * FROM movimientocuotas ORDER BY fecha');
		$result_all = mysql_query($query);
		while ($vartd = mysql_fetch_object($result_all))
		{
			//llenar el array
			$arrMov[]=array($vartd->id,
							$vartd->cobrado,
							$vartd->fecha,
							$vartd->idIdMovimientoExterno,
							$vartd->idtipomov,
							$vartd->idarchivocobro,
                                                        $vartd->detalleboleta,
                                                        $vartd->fechacobro,
							);
		}
		return $arrMov;
	}

//============================================================================


	public function listadoTotal()
    //retorna la consulta de todos los movimientos de las cuotas
	{
		$query = ("SELECT * FROM movimientocuotas");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
				$arrMov[] = array("id"=>$vartd->id,
								"cobrado"=>$vartd->cobrado,
								"fecha"=>$vartd->fecha,
								"idcuota"=>$vartd->idIdMovimientoExterno,"fechacobro"=>$vartd->fechacobro
								);
                                
                                }
		}
		return  $arrMov;
	}


//============================================================================

	public function registrarmovimiento()
    {
		$query = ("INSERT INTO movimientosexternos (cobrado,fecha,idmovimientoexterno,idtipomov,idarchivocobro,detalleboleta,fechacobro)
				VALUES('$this->intCobrado','$this->fecFecha','$this->intIdMovimientoExterno','$this->intIdTipomov',$this->intIdArchivo,'$this->txtDetalleBoleta','$this->fecFechaCobro')");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
                
	}

//============================================================================

	public function borrarMovimiento()
    {
		$query = "DELETE FROM movimientocuotas WHERE id='$this->intId'";
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

       public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putId(0);
		$this->putCobrado(0);
       	$this->putFecha(0);
        $this->putFechaCobro(0);
       	$this->putIDTipomov(0);
        $this->putIdRegmov(0);   
	$this->putIdArchivo(0);
        $this->putDetalleBoleta('');
	}


//----------------------------------------------------------
	public function contarmovimientos($idarchivo)
    //retorna la consulta de todos los movimientos sobre las cuotas
	{
		$query =('SELECT count(*) as cantidad FROM movimientosexternos where idarchivocobro=  '.$idarchivo);
		$result_all = mysql_query($query);
                 
		if ($result_all)
		{
		        $vartd = mysql_fetch_object($result_all);
			return $vartd->cantidad;
	        }else return 0;
                
                }


public function listadoMovimientosArchivo()
    //retorna la consulta de todos los movimientos de las cuotas realizados en un mismo archivo de cobro
	{
		$query = "SELECT * FROM movimientosexternos WHERE idarchivocobro=".$this->intIdArchivo;
                  
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
		
                            
                            	$arrMov[]=array("id"=>$vartd->id,
							"cobrado"=>$vartd->cobrado,
							"fecha"=>$vartd->fecha,
							"fechacobro"=>$vartd->fechacobro,                                    
							"idIdMovimientoExterno"=>$vartd->idmovimientoexterno,
							"idtipomov"=>$vartd->idtipomov,
							"idarchivocobro"=>$vartd->idarchivocobro,
                                                        "detalleboleta"=>$vartd->detalleboleta
							);
                        }
		}
		return $arrMov;
	}
                
}

?>