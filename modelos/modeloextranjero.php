<?php


class ModeloExtranjero
{
    private $intIdExtranjero;
	private $intIdPoblador;
	private $txtCartaCiudadania;
	private $fecFechaCarta;
    private $txtJuzgado;
    private $txtVisadoPor;
    private $fecFechaLlegada;
	private $txtProcedencia;
	private $txtMedio;
	private $txtNombres;
	private $txtApellido;
	
    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
// ------------------------------------------------------------------------------------

    public function getIdExtranjero()
	{
	    return $this->intIdExtranjero;
	} 

    public function putIdExtranjero($parIdExtranjero)
	{
	    $this->intIdExtranjero = $parIdExtranjero;
	}
	
// ------------------------------------------------------------------------------------
	
    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador = $parIdPoblador;
	} 

// ------------------------------------------------------------------------------------

    public function getCartaCiudadania()
	{
	    return $this->txtCartaCiudadania;
	} 

    public function putCartaCiudadania($parCarta)
	{
	    $this->txtCartaCiudadania = $parCarta;
	} 

// ------------------------------------------------------------------------------------

	public function getFechaCarta()
	{
		return $this->fecFechaCarta;
	}
	
	public function putFechaCarta($parFechaCarta)
	{
		$this->fecFechaCarta = $parFechaCarta;
	}
	
// ------------------------------------------------------------------------------------

    public function getJuzgado()
	{
	    return $this->txtJuzgado;
	} 

    public function putJuzgado($parJuzgado)
	{
	    $this->txtJuzgado = $parJuzgado;
	}

// ------------------------------------------------------------------------------------

    public function getVisadoPor()
	{
	    return $this->txtVisadoPor;
	} 

    public function putVisadoPor($parVisadoPor)
	{
	    $this->txtVisadoPor = $parVisadoPor;
	}

// ------------------------------------------------------------------------------------

	public function getFechaLlegada()
	{
		return $this->fecFechaLlegada;
	}
	
	public function putFechaLlegada($parFechaLlegada)
	{
		$this->fecFechaLlegada = $parFechaLlegada;
	}
		
// ------------------------------------------------------------------------------------

    public function getProcedencia()
	{
	    return $this->txtProcedencia;
	} 

    public function putProcedencia($parProcedencia)
	{
	    $this->txtProcedencia = $parProcedencia;
	} 

// ------------------------------------------------------------------------------------

    public function getMedio()
	{
	    return $this->txtMedio;
	} 

    public function putMedio($parMedio)
	{
	    $this->txtMedio = $parMedio;
	} 

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	} 

// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido= $parApellido;
	} 
	
//=======================================================================================================================	 

	public function listado() 
    //retorna la consulta de todos los extranjeros
	{
    	$query = ('SELECT pobladores.id, pobladores.nombres, pobladores.apellido, extranjeros.id as idextranjero, extranjeros.idpoblador, extranjeros.cartaciudadania, extranjeros.procedencia FROM pobladores, extranjeros WHERE pobladores.id=extranjeros.id ORDER BY apellido');
	
    	$result_all = mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrPobladores[] = array("idpoblador"=>$varcli->id,
									"nombres"=>$varcli->apellido.", ".$varcli->nombres,
									"idextranjero"=>$varcli->idextranjero,
									"carta"=>$varcli->cartaciudadania,
									"procedencia"=>$varcli->procedencia
									);
		} 
		return($arrPobladores);	
	}

//=======================================================================================================================	 

	public function traerExtranjero()
	//retorna los datos de un poblador extranjero a partir de un id 
	{
		$query = ("SELECT pobladores.id as idpoblador, pobladores.nombres, pobladores.apellido, extranjeros.* FROM pobladores, extranjeros WHERE pobladores.id=extranjeros.idpoblador && extranjeros.id='$this->intIdExtranjero'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();

		if($result_all && $num_rows > 0)
		{
			$this->cargarresultados($result_all);
			return(true);	            
		} else {
	  		return(false);	
	  	}
	}

//=======================================================================================================================	 

	public function traerExtranjeroAsociado()
	//retorna los datos de un poblador extranjero a partir de un id de poblador 
	{
		$query = ("SELECT pobladores.id as idpoblador, pobladores.nombres, pobladores.apellido, extranjeros.* FROM pobladores, extranjeros WHERE extranjeros.idpoblador='$this->intIdPoblador' && pobladores.id=extranjeros.idpoblador");
		$result_all = mysql_query($query);

		if($result_all)
		{
			$this->cargarresultados($result_all);
			return(true);	            
		} else {
	  		return(false);	
	  	}
	}
	
//=======================================================================================================================	 
	
	public function borrarextanjero()
	{	
		$query = ("DELETE FROM extranjeros WHERE id = '$this->intIdExtranjero'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
     	
//=======================================================================================================================	 

	public function modificarextranjero()
	{
		$query = ("UPDATE extranjeros SET cartaciudadania='$this->txtCartaCiudadania',fechaexpedicion='$this->fecFechaCarta',
					juzgado='$this->txtJuzgado',visadopor='$this->txtVisadoPor',fechallegada='$this->fecFechaLlegada',procedencia='$this->txtProcedencia', 
					medio='$this->txtMedio' WHERE id = '$this->intIdExtranjero'");			
		$result_all = mysql_query($query);
		return($result_all);
	}

//=======================================================================================================================	 

	public function altaextranjero()
	{
		$query = ("INSERT INTO extranjeros (idpoblador, cartaciudadania, fechaexpedicion, juzgado, visadopor, fechallegada, procedencia, medio)
				VALUES ($this->intIdPoblador,'$this->txtCartaCiudadania','$this->fecFechaCarta','$this->txtJuzgado','$this->txtVisadoPor',
				'$this->fecFechaLlegada','$this->txtProcedencia', '$this->txtMedio')");
		$result_all = mysql_query($query);
	    if($result_all)
	    {
	    	define('IDEXT',mysql_insert_id()); /* obtengo el id del ultimo insert en la DB */
	    	return IDEXT;
	    } else {
			return 0;	    	
	    }
	}

//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putIdExtranjero(0);
		$this->putIdPoblador(0);
		$this->putCartaCiudadania("");
		$this->putFechaCarta("");
		$this->putJuzgado("");
		$this->putVisadoPor("");
		$this->putFechaLlegada("");
		$this->putProcedencia("");
		$this->putMedio("");
	}
  
//=======================================================================================================================	 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdExtranjero($cons->id);
			$this->putIdPoblador($cons->idpoblador);
			$this->putCartaCiudadania($cons->cartaciudadania);
			$this->putFechaCarta($cons->fechaexpedicion);
			$this->putJuzgado($cons->juzgado);
			$this->putVisadoPor($cons->visadopor);
			$this->putFechaLlegada($cons->fechallegada);
			$this->putProcedencia($cons->procedencia);
			$this->putMedio($cons->medio);
			$this->putNombres($cons->nombres);
			$this->putApellido($cons->apellido);
		}
	}
	
	

}
?>