<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Solicitudes rurales</h1>
	
	<ul>
		<li><a href=#alta>C&oacute;mo cargar una nueva solicitud rural</a></li>
		<li><a href=#tierra>Datos de la tierra rural</a></li>
		<li><a href=#mod>C&oacute;mo modificar una solicitud rural</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>En esta secci&oacute;n se detalla cu&aacute;les son los pasos a seguir para el manejo de solicitudes de tierra rural. Tanto para
		listados, nuevas solicitudes, modificaciones y eliminaciones.
	</p>
</div>

<div class="descripciones" id="alta">
	<h2>C&oacute;mo cargar una nueva solicitud rural</h2>
	<p>Para cargar una nueva solicitud de tierra rural debe acceder a la opci&oacute;n <b>nueva solicitud rural</b> del men&uacute; de
		opciones.<br/>
		Una vez all&iacute; ver&aacute; una pantalla como la siguiente:</p>
	<img alt="solicitud rural" src="css/images/solicitud-rural.png"></img>
	<p>Luego complete los campos con los datos de la solicitud de la siguiente forma:</p>
	<p>El primer paso es asignar a la solicitud el poblador solicitante. Para esto ingrese el apellido de la persona en el campo
		<i>poblador</i> y presione el bot&oacute;n <img src="css/images/boton-buscar-poblador.png" alt="boton buscar poblador"></img>.<br/>
		Luego seleccione el poblador solicitante de la lista presentada haciendo doble clic. Si el poblador que est&aacute; buscando no se 
		encuentra en la lista deber&aacute; primero darlo de alta en el sistema. Consulte la secci&oacute;n
		<a href=#alta>c&oacute;mo cargar un nuevo poblador</a>.<br/>
		A continuaci&oacute;n:
	</p>

	<ul>
		<li>Seleccione el empleado que recibi&oacute; o complet&oacute; la solicitud. Si no se encuentra en la lista puede pedir al
			administrador del sistema que lo de de alta.</li>
		<li>Ingrese la fecha de ingreso de la solicitud. Este dato es obligatorio.</li>
		<li>Ingrese la superficie declarada por el poblador en el formulario de la solicitud de tierra y la unidad de medida, normalmente
			deber&iacute;a ser <i>hect&aacute;reas</i>. Estos campos son obligatorios.</li>
		<li>Seleccione de la lista la localidad que figura en el formulario de solicitud, si no figura una localidad puede seleccionar el
			centro urbano m&aacute;s cercano a la tierra solicitada. Es un campo obligatorio de completar.</li>
		<li>Seleccione el grado de tenencia que ha solicitado el poblador o el grado de tenencia propuesto por el organismo. Es obligatorio
			de completar</li>
		<li>Ingrese el n&uacute;mero de expediente y a&ntilde;o asignado al mismo. Es obligatorio de completar.</li>
		<li>Ingrese si el poblador reside en la provincia y desde qu&eacute; a&ntilde;o.</li>
		<li>Ingrese si el poblador trabaja para la provincia y en qu&eacute; repartici&oacute;n.</li>
		<li>Si considera importante alg&uacute;n otro dato extra puede anotarlo como observaci&oacute;n en el cuadro de texto para
			observaciones.</li>
		<li>Por &uacute;ltimo presione el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"></img> para almacenar los
			datos.</li>
	</ul>
	<p>Si todo funcion&oacute; correctamente, la solicitud ya estar&aacute; almacenada en el sistema y ahora deber&aacute; proceder a
		detallar los datos de la tierra solicitada para que el alta est&eacute; completa. Consulte la secci&oacute;n
		<a href=#tierra>datos de la tierra rural</a> para m&aacute;s detalle.</p>
</div>

<div class="descripciones" id="tierra">
	<h2>Datos de la tierra rural</h2>
	<p>Una vez que ha completado y guardado los datos principales de la solicitud, puede cargar los datos de la tierra rural solicitada. Para
		esto, acceda desde el formulario solicitud rural a la pesta&ntilde;a <b>datos de la tierra</b> y se encontrar&aacute; una pantalla
		como la siguiente.
	</p>
	<img alt="datos tierra" src="css/images/tierra-rural.png"/>
	<p>Los datos de la tierra se deben completar del siguiente modo:</p>
	<ul>
		<li>Seleccione la <i>secci&oacute;n</i> y la <i>fracci&oacute;n</i> donde est&aacute; ubicada la tierra. Es un dato obligatorio si
			corresponde.</li>
		<li>Sino seleccione la <i>colonia</i> y el <i>ensanche</i>.</li>
		<li>Ingrese el n&uacute;mero de lote. Puede ingresar varios lotes. Es obligatorio de completar.</li>
		<li>Seleccione la legua donde se ubica la tierra si es necesario.</li>
		<li>Si necesita destacar alguna otra caracter&iacute;stica puede cargarlo en el campo <i>observaciones</i>. Por ej. si la tierra
			consiste en varias subdivisiones de lotes diferentes puede detallarlo como observaci&oacute;n.</li>
		<li>Luego se presenta un conjunto de preguntas referidas al formulario de solicitud de tierra completado por el poblador. Si cuenta
			con dichos datos puede cargarlos para ser consultados posteriormente.</li>
		<li>Por &uacute;ltimo presione el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"></img> para almacenar los
			datos.</li>
	</ul>
	<p>Si la carga se concret&oacute; con &eacute;xito, la solicitud rural completa ya se encuentra en el sistema y se podr&aacute; continuar
		a cargar un nuevo informe de inspecci&oacute;n rural.</p>
</div>

<div class="descripciones" id="mod">
	<h2>C&oacute;mo modificar una solicitud rural</h2>
	<p>Para modificar los datos de una solicitud existente debe acceder al formulario de la solicitud a trav&eacute;s de la opci&oacute;n
		<b>listar solicitudes rurales</b> del men&uacute; de opciones.<br/>
		Luego, seleccione del listado la solicitud que desea modificar y presione el bot&oacute;n
		<img src="css/images/boton-modificar.png" alt="boton modificar"></img>.<br/>
		Una vez all&iacute;, puede modificar cualquiera de los datos asociados a la solicitud. Recuerde que debe seleccionar un poblador
		solicitante y que los campos <i>superficie, localidad, grado de tenencia</i> y <i>expediente</i> son obligatorios de completar.<br/>
		Antes de abandonar el formulario presione <img src="css/images/boton-guardar.png" alt="boton guardar"></img>.<br/>
		Tambi&eacute;n desde all&iacute; puede acceder a la pesta&ntilde;a <b>datos de la tierra</b> y modificar la ubicaci&oacute;n de la
		tierra. Luego seleccione <img src="css/images/boton-guardar.png" alt="boton guardar"></img> para salvar los cambios.
	</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
