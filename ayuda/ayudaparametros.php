<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>
<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Par&aacute;metros varios</h1>
	
	<ul>
		<li><a href=#interes>C&oacute;mo modificar el inter&eacute;s del BCH</a></li>
		<li><a href=#destino>Destinos de tierras urbanas</a></li>
		<li><a href=#valores>Valores de tierras rurales</a></li>
	</ul>
</div>

<div class="descripciones" >
	<p>En esta secci&oacute;n se explica c&oacute;mo se modifican los par&aacute;metros varios que completan el sistema. A saber, el
		inter&eacute;s por morosidad definido anualmente por el Banco de la Provincia del Chubut (BCH), los destinos posibles para las
		tierras urbanas solicitadas y los valores de la hect&aacute;rea de una tierra rural en base a su capacidad ganadera y su distancia al
		punto de embarque m&aacute;s cercano.
	</p>
</div>

<div class="descripciones" id="interes">
	<h2>C&oacute;mo modificar el inter&eacute;s del BCH</h2>
	<p>Para modificar el inter&eacute;s por morosidad definido por el BCH se debe acceder a la opci&oacute;n
		<b>modificar inter&eacute;s	BCH</b>	del men&uacute;	principal.
	</p>
	<p>
		All&iacute; se encuentra definido el inter&eacute;s actual.<br/>
		Simplemente modifique manualmente el valor del campo <i>inter&eacute;s</i> y presione
		<img src="css/images/boton-guardar-cambios.png" alt="boton guardar cambios" />.
	</p>
</div>

<div class="descripciones" id="destino">
	<h2>Destinos de tierras urbanas</h2>
	<p>Los destinos de las tierras urbanas son los fines que se le dar&aacute; a cada tierra urbana que se solicite. Inicialmente los destinos
		posibles son <i>vivienda, comercio, industria, entidades de bien p&uacute;blico</i> y <i>turismo</i>. Esta funcionalidad permite
		agregar otros destinos posibles para las tierras urbanas seg&uacute;n otros datos como la localidad donde se encuentra o la fecha de
		establecimiento en la tierra.
	</p>
	<p>Ingrese a la opci&oacute;n <b>destino de tierra urbana</b> del men&uacute; de opciones y ver&aacute; la lista de destinos de tierra
		actuales.<br/>
		Presione el bot&oacute;n <img src="css/images/boton-agregar.png" alt="boton agregar" /> para agregar un nuevo destino.<br/>
	</p>
	<p>
		En la siguiente pantalla complete los campos con una descripci&oacute;n del destino de la tierra, la localidad donde ser&aacute;
		aplicado y el monto por metro cuadrado para las tierras con ese fin.<br/>
		Luego presione <img src="css/images/boton-guardar.png" alt="boton guardar" /> para almacenar los datos.
	</p>
	<p>Para modificar los valores de un destino ya cargado o para eliminar completamente del sistema alguno de ellos se utilizan los botones
		<img src="css/images/boton-modificar.png" alt="boton modificar" /> y <img src="css/images/boton-eliminar.png" alt="boton eliminar" />
		del mismo modo que para agregar un destino.
	</p>
</div>

<div class="descripciones" id="valores">
	<h2>Valores de tierras rurales</h2>
	<p>La tabla de valores de las tierras rurales est&aacute; generada en base a la tabla de valores de tierras de la resoluci&oacute;n 221/03
		del IAC que define el valor de la hect&aacute;rea de una tierra rural seg&uacute;n su distancia al punto de embarque m&aacute;s
		cercano y su capacidad ganadera.
	</p>
	<p>Para modificar los valores de dicha tabla ingrese a la opci&oacute;n <b>valores de tierra</b> del men&uacute;.<br/>
		All&iacute; ver&aacute; la tabla de valores completa.<br/>
		Seleccione el rengl&oacute;n que contiene el valor que desea modificar y presione el bot&oacute;n
		<img src="css/images/boton-modificar.png" alt="boton modificar" />.
	</p>
	<p>En la siguiente pantalla modifique manualmente los montos correspondientes seg&uacute;n las distancias y presione el bot&oacute;n
		<img src="css/images/boton-guardar.png" alt="boton guardar" /> para salvar los cambios.
	</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
