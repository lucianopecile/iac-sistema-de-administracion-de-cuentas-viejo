<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Cuentas</h1>
	
	<ul>
		<li><a href=#alta>C&oacute;mo cargar una nueva cuenta</a></li>
		<li><a href=#reliquidar>C&oacute;mo reliquidar/modificar una cuenta</a></li>
		<li><a href=#precio>C&oacute;mo definir el valor total de la tierra</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>En esta secci&oacute;n se detalla c&oacute;mo es el procedimiento a seguir para crear una cuenta asociada a un expediente y otras
		tareas m&aacute;s espec&iacute;ficas como liquidar la cuenta por el valor de la tierra, generar las cuotas, realizar modificaciones
		sobre los valores de la cuenta y reliquidar la cuenta en caso de que se necesite alguna alteraci&oacute;n en los montos o porcentajes.
	</p>
</div>

<div class="descripciones" id="alta">
	<h2>C&oacute;mo cargar una nueva cuenta</h2>
	<p>Para cargar una nueva cuenta debe tener la solicitud correspondiente almacenada en el sistema y si es una solicitud de tierra rural
		debe tener tambi&eacute;n el informe de inspecci&oacute;n de la tierra.<br/>
		Para dar de alta la cuenta acceda a la opci&oacute;n <b>cargar nueva cuenta</b> del men&uacute; de opciones y ver&aacute; el
		siguiente formulario.
	</p>
		<img alt="nueva cuenta" src="css/images/nueva-cuenta.png"></img>
	<p>
		En el formulario que se presenta seleccione el expediente al cual corresponde la cuenta, para esto presione el bot&oacute;n
		<img alt="boton buscar expediente" src="css/images/boton-buscar-expediente.png"/> y seleccione de la lista el expediente
		correspondiente.<br/>
		Luego, complete los datos de la siguiente manera:
	</p>
	<ul>
		<li>Ingrese el n&uacute;mero de cuenta. Es un campo obligatorio de completar.</li>
		<li>Ingrese la fecha de creaci&oacute;n de la cuenta. Esta fecha se utiliza para el calcular vencimiento del pago de la
			formalizaci&oacute;n por la adjudicaci&oacute;n. Es decir, la fecha de vencimiento del pago de formalizaci&oacute;n ser&aacute; 
			30 d&iacute;as despu&eacute;s de la fecha de la cuenta.</li>
		<li>Para calcular el <i>valor liquidaci&oacute;n</i> presione el bot&oacute;n
			<img alt="boton liquidar cuenta" src="css/images/boton-liquidar.png"/> y se abrir&aacute; la pantalla de liquidaci&oacute;n de
			cuenta. All&iacute; se muestran los detalles que componen la liquidaci&oacute;n y se permite agregar a la cuenta gastos extra por
			administraci&oacute;n, pastajes, inspecci&oacute;n, etc.</li>
		<li>Seleccione los conceptos necesarios tildando las casillas correspondientes y presione
			<img src="css/images/boton-guardar.png" alt="boton guardar"/> para almacenar los datos y luego el bot&oacute;n
			<img src="css/images/boton-enviar-liquidacion.png" alt="boton enviar liquidacion"/> para enviar el valor calculado al formulario
			de la cuenta.</li>
		<li>Defina la tasa de inter&eacute;s por financiaci&oacute;n de la cuenta. Por defecto es de 5,07% y no puede ser menor a 5% ni mayor
			a 10%.</li>
		<li>Defina el porcentaje de formalizaci&oacute;n de la cuenta. Por defecto es de 10% y debe ser mayor o igual a 10% y menor o igual a
			30%.</li>
		<li>Ingrese la cantidad de cuotas de la financiaci&oacute;n.</li>
		<li>Seleccione el periodo de pago de las cuotas, por ej. anual, mensual o trimestral.</li>
		<li>Ingrese la fecha de vencimiento de la primera cuota.</li>
		<li>Ingrese la el n&uacute;mero de cuenta corriente para el pago de las cuotas.</li>
		<li>Ingrese el n&uacute;mero de convenio con el Banco del Chubut.</li>
		<li>Si lo desea ingrese cualquier otra informaci&oacute;n que considere relevante como observaci&oacute;n.</li>
		<li>Por &uacute;ltimo presione el bot&oacute;n <img src="css/images/boton-generar-cuotas.png" alt="boton generar cuotas"/> para
			guardar los datos de la cuenta y generar las cuotas correspondientes.</li>
	</ul>
	<p>Si la cuenta se gener&oacute; con &eacute;xito el sistema listar&aacute; los datos de la cuenta y las cuotas generadas.</p>
</div>

<div class="descripciones" id="reliquidar">
	<h2>C&oacute;mo reliquidar/modificar una cuenta</h2>
	<p>En el caso de que haya cambios en las caracter&iacute;sticas de una tierra asociada a una cuenta que ya fue liquidada, es necesario
		volver a liquidarla en base a los nuevos valores.<br/>
		Para reliquidar una cuenta seleccione del men&uacute; la opci&oacute;n <b>reliquidar/eliminar cuentas</b>.<br/>
		Seleccione del listado la cuenta que desea volver a liquidar y presione el bot&oacute;n
		<img src="css/images/boton-reliquidar-cuenta.png" alt="boton reliquidar cuentas"/>.
	</p>
	<p>La siguiente pantalla es id&eacute;ntica a la pantalla de <i>nueva cuenta</i> y muestra los valores actuales de la cuenta.<br/>
		Desde all&iacute; es posible modificar el inter&eacute;s por financiaci&oacute;n, la cantidad de cuotas, el periodo de pago, el
		n&uacute;mero de cuenta, el n&uacute;mero de convenio y las observaciones.<br/>
		Por supuesto, tambi&eacute;n se puede volver a liquidar la cuenta presionando el bot&oacute;n
		<img alt="boton liquidar cuenta" src="css/images/boton-liquidar.png"/> y siguiendo los mismos pasos que para una
		liquidaci&oacute;n normal.<br/>
		Una vez modificados los valores necesarios debe volver a generar las cuotas en base a los nuevos valores de la cuenta. Para esto
		presione el bot&oacute;n <img src="css/images/boton-recalcluar-cuotas.png" alt="boton recalcular cuotas"/>.
	</p>
	<p>Es posible tambi&eacute;n, con el mismo procedimiento, volver a calcular las cuotas manteniendo el mismo monto de liquidaci&oacute;n
		pero modificando la tasa de inter&eacute;s o la cantidad de cuotas</p>
	<p>Luego del rec&aacute;lculo de las cuotas, el sistema presenta una pantalla con los datos de la cuenta y la lista de las cuotas con los
		nuevos valores.</p>
	<p>Es importante destacar que las cuotas de la cuenta que tengan al menos un pago recibido no ser&aacute;n modificadas. Es decir que, al
		nuevo valor de liquidaci&oacute;n se le debitar&aacute; el monto ya cobrado hasta la fecha y el resultado ser&aacute; refinanciado.
	</p>
</div>

<div class="descripciones" id="precio">
	<h2>C&oacute;mo definir el valor total de la tierra</h2>
	<p>Es posible definir manualmente el valor final de liquidaci&oacute;n de la cuenta.<br/>
		Para esto debe acceder a la opci&oacute;n <b>ajustar liquidaci&oacute;n manualmente</b> del men&uacute; de opciones.<br/>
		Luego seleccione de la lista la cuenta que desea modificar y presione el bot&oacute;n
		<img src="css/images/boton-ajustar-liquidacion.png" alt="boton ajustar liquidacion"/>.<br/>
		Una vez en la pantalla de modificaci&oacute;n podr&aacute; asignar manualmente un nuevo monto total de liquidaci&oacute;n para la
		cuenta, y si lo desea tambi&eacute;n podr&aacute; cambiar la tasa de inter&eacute;s por financiaci&oacute;n.<br/>
		Si es necesario puede escribir observaciones al respecto en el campo <i>observaciones</i>.
	</p>
	<p>Por &uacute;ltimo, debe obligatoriamente volver a calcular las cuotas mediante el bot&oacute;n
		<img src="css/images/boton-recalcular-cuotas.png" alt="boton recalcular cuotas"/> para generarlas nuevamente en base al nuevo monto
		de liquidaci&oacute;n.</p>
	<p>Al igual que para el proceso de reliquidaci&oacute;n, las cuotas de la cuenta que tengan al menos un pago recibido no ser&aacute;n
		modificadas. Es decir que, al nuevo valor de liquidaci&oacute;n se le debitar&aacute; el monto ya cobrado hasta la fecha y luego
		ser&aacute; refinanciado.</p>
</div>


<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
