<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Cuotas</h1>
	
	<ul>
		<li><a href=#importar>C&oacute;mo importar un archivo de cobros</a></li>
		<li><a href=#cobro>C&oacute;mo cobrar manualmente una cuota</a></li>
		<li><a href=#descuento>C&oacute;mo descontar el cobro de una cuota</a></li>
		<li><a href=#deshacer>C&oacute;mo deshacer el &uacute;ltimo movimiento de una cuota</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>Esta secci&oacute;n detalla paso a paso el funcionamiento de las herramientas disponibles para operar con el cobro de cuotas. Esto
	es, procesamiento de archivos de cobro, cobros manuales y reparaci&oacute;n de errores en los cobros.</p>
</div>

<div class="descripciones" id="importar">
	<h2>C&oacute;mo importar un archivo de cobros</h2>
	<p>Desde el men&uacute; principal escoja la opci&oacute;n <b>importar archivo</b>
	</p>
	<p>En la siguiente pantalla ver&aacute; un cuadro como el siguiente:
	</p>
	<img alt="cuadro importar" src="css/images/importar-archivo.png"/>
	<p>
		Presione el bot&oacute;n <img alt="boton-examinar" src="css/images/boton-examinar.png"/> y busque en el sistema el archivo de cobros
		que desea importar.<br/>
		Luego, pulse el bot&oacute;n <img alt="boton-enviar" src="css/images/boton-enviar.png"/> para importar y procesar el archivo al
		sistema.
	</p>
	<p>
		El sistema muestra los datos del archivo de cobros procesado, si son correctos, seleccione
		<img alt="boton aceptar" src="css/images/boton-aceptar.png"/> para completar la operaci&oacute;n.<br/>
		Para que el archivo de cobros sea importado correctamente debe poseer un nombre con extensi&oacute;n <b>txt</b>, un tama&ntilde;o de
		archivo inferior a 200 KB y el tipo de archivo debe ser <b>text/plain</b>.
	</p>
	<p>
		Tenga en cuenta que una vez que el archivo de cobros fue procesado, no se puede volver a importar. Si lo intenta el sistema
		mostrar&aacute; una advertencia y no aceptar&aacute; el archivo.
	</p>
</div>

<div class="descripciones" id="cobro">
	<h2>C&oacute;mo cobrar manualmente una cuota</h2>
	<p>Esta herramienta se utiliza para realizar el cobro total o parcial de una cuota de forma manual. Es decir, ingresando manualmente el
		monto cobrado y la fecha del cobro.
	</p>
	<p>Desde el men&uacute; principal acceda a la opci&oacute;n <b>cobro manual</b> en el submen&uacute; <b>cobros</b>.
	</p>
	<p>A continuaci&oacute;n seleccione la cuenta a la cual pertenece el cobro y luego seleccione la cuota que desea cobrar.
	</p>
	<p>En la siguiente pantalla ingrese el monto del cobro y la fecha del cobro en los campos correspondientes.
	</p>
	<img alt="cobro manual" src="css/images/cobro-manual.png"/>
	<p>Finalmente presione el bot&oacute;n <img alt="boton guardar cobro" src="css/images/boton-guardar-cobro.png"/> para registrar el cobro.
	</p>
	<p>Es importante destacar que el monto a cobrar ingresado no puede ser mayor a la deuda de la cuota. Es decir, no puede superar al saldo
	m&aacute;s el inter&eacute;s por mora.<br/>
	Tambi&eacute;n tenga en cuenta que si no emiti&oacute; una boleta para el cobro de la cuota, es posible que el inter&eacute;s por mora est&eacute; desactualizado.
	Primero actualice manualmente el inter&eacute;s punitorio de la cuota y luego realice el cobro.
	Vea la secci&oacute;n <a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#editar">c&oacute;mo editar	una
	cuota</a> para actualizar el inter&eacute;s.
	</p>
</div>

<div class="descripciones" id="descuento">
	<h2>C&oacute;mo descontar el cobro de una cuota</h2>
	<p>Esta funci&oacute;n se utiliza para descontar un monto determinado al cobro de una cuota. Estos descuentos se pueden necesitar si ocurri&oacute;
		alg&uacute;n error en el manejo de las cuotas que pueda provocar un cobro electr&oacute;nico superior a lo adeudado en la cuota.
	</p>
	<p>Desde el men&uacute; principal acceda a la opci&oacute;n <b>descontar cobro a una cuota</b>.
	</p>
	<p>A continuaci&oacute;n seleccione la cuenta a la cual pertenece el cobro y luego seleccione la cuota que desea descontar.
	</p>
	<p>En la siguiente pantalla ingrese el valor a descontar en el campo correspondiente. Este valor debe ser un n&uacute;mero positivo, y siendo
		que ser&aacute; restado al monto cobrado de la cuota, no puede ser superior a este monto.	<br/>
		La fecha del descuento ser&aacute; siempre la fecha actual y no puede ser alterada.
	</p>
	<img alt="descontar cobro" src="css/images/descontar-cobro.png"/>
	<p>El paso siguiente es ingresar en el cuadro de texto el motivo por el cual se debi&oacute; realizar el descuento. Este dato es obligatorio de
		completar en este caso.
	</p>
	<p>Finalmente presione el bot&oacute;n <img alt="boton descontar" src="css/images/boton-descontar.png"/> para efectuar el descuento.
	</p>
</div>

<div class="descripciones" id="deshacer">
	<h2>C&oacute;mo deshacer el &uacute;ltimo movimiento de una cuota</h2>
	<p>Esta funci&oacute;n se utiliza para deshacer o revertir los cambios realizados por un movimiento en la cuota. En otras palabras, para
		revertir un cobro electr&oacute;nico emitido por el banco. Puede ser de utilidad en caso de haber un error en el archivo de cobros enviado
		por el banco.
	</p>
	<p>Desde el men&uacute; principal acceda a la opci&oacute;n <b>deshacer &uacute;ltimo movimiento de una cuota</b>.
	</p>
	<p>A continuaci&oacute;n seleccione la cuenta y la cuota a la cual pertenece el movimiento.
	</p>
	<p>En la siguiente pantalla ver&aacute; un listado de los movimientos que se han generado para dicha cuota. <br/>
		Para deshacer el &uacute;ltimo movimiento simplemente presione el bot&oacute;n
		<img alt="boton deshacer movimiento" src="css/images/boton-deshacer-movimiento.png"/>.
	</p>
	<img alt="deshacer movimiento" src="css/images/deshacer-movimiento.png"/>
	<p>Este proceso eliminar&aacute; el &uacute;ltimo movimiento realizado sobre la cuota y la devolver&aacute; a su estado anterior autom&aacute;ticamente.
	</p>
	<p>Tenga en cuenta que una vez eliminado el movimiento no se puede volver a recuperar, ni tampoco es posible procesar dos veces el mismo
		archivo de cobros.
	</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
