<?PHP
function menor($n1,$n2)
{
if ($n1>$n2) return $n2; else return $n1;
}
function puntos_cm($medida,$resolucion=72)
{
return ($medida/(2.54))*$resolucion;
}
function puede($accion=0) 
{

	if(isset( $_SESSION['segProyectos']['usuario']['acciones'])) {
		$r = substr($_SESSION['segProyectos']['usuario']['acciones'],$accion,1);
		return $r;
	}

}

function array_columnas( $arr )
{
	if(($cantArg=func_num_args()) >1) {
		$ret=array();
		$arg_list = func_get_args();
		foreach($arr as $a) {
			$aux=array();
			for ( $i = 1; $i < $cantArg; $i++ ) {
				 $aux[] = $a[ $arg_list[$i] ];
			}
			$ret[] = $aux;
		}
		return $ret;
	} else {
		return array();
	}
}

function adate($parfecha)
{
        if( empty($parfecha) ) { 
			$auxFecha='null';
		} else {
			list($y,$m,$d) = split( '[/-]', $parfecha);
			$auxFecha = sprintf("%04s",$y).'-'.sprintf("%02s",$m).'-'.sprintf("%02s",$d);
		}
		return $auxFecha;
}

function fechaACadena($parfecha)
{
        if( empty($parfecha) ) 
		{ 
			$auxFecha='';
		} 
		else 
		{
			list($y,$m,$d,$h,$n,$s) = split( "[- :]", $parfecha);
			if (($y=="0000") && ($m=="00") && ($d=="00"))
				$auxFecha='';
			else
    			$auxFecha = sprintf("%02s",$d).'/'.sprintf("%02s",$m).'/'.sprintf("%04s",$y);
		}
		return $auxFecha;
}

function cadenaAFecha($parCadena)
{
    $parCadena=trim($parCadena);
	$aux="";
        if( !empty($parCadena) ) 
		{
			list($d,$m,$y) = explode( "/", $parCadena);
			$aux = sprintf("%04s",$y).'-'.sprintf("%02s",$m).'-'.sprintf("%02s",$d);
		}
		return $aux;
}

function diasdelmes($mes,$anio) 
{
   $mes=$mes*1;
  
    $arraymeses= array('0','31','29','31','30','31','30','31','31','30','31','30','31');
	
	$ultimo=0;

if ($mes==2){
    $valid = checkdate($mes,29,$anio);
    if(!$valid){
	  
	  $ultimo=28;
	  }
}	
 if($ultimo==0){
	    $ultimo=$arraymeses[$mes];
		}
  return($ultimo);


}

//=======================================================================================
 function sumaDia($fecha,$dia){
	if($fecha==0){
	   return 0;
	}   
	    // suma $dia dias a una fecha y retorna la fecha resultante
    	list($year,$mon,$day) = explode('-',$fecha);

		return date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));	
	}
	
	
	
	
function is_date($f) 
{
	if(empty($f)) 
	{
		return false;		
	} 
	elseif ( ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $f ) ) 
	{
		$a=strpos($f,'/');
		$b=strrpos($f,'/');
		if($a>0 && $a<$b) 
		{
			$d=substr($f,0,$a);
			$m=substr($f, $a+1, $b-($a+1));
			$y=substr($f,$b+1);
			if(is_numeric($d) && is_numeric($m) && is_numeric($y)) 
			{
				return checkdate($m,$d,$y);
			} 
			else 
			{
				return false;
			}
		}
		return false;
	} 
	else 
	{
		return false;
	}
}
// is it a timestamp?
function is_ts($f) {
	$re="/^(?:(?:(?:(?:[1-2][0-9]{3}) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12]";
	$re.="[0-9]|3[01]|0?[1-9]))|(?:(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]) *(?:";
	$re.="[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\.";
	$re.=", ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?i:(?:";
	$re.="j(?:an(?:uary)?|u(?:ne?|ly?)))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember";
	$re.=")?|feb(?:ruary)?|sep(?:tember)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[12][0-9]|3[01]|0?[1-9]";
	$re.=")|(?:(?i:[23]?1st|2?2nd|2?3rd|[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?:[";
	$re.="0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd|2?3rd|";
	$re.="[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?|ly?";
	$re.=")))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:temb";
	$re.="er)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3}))))|(?:(?:(?:(?:[1-2]";
	$re.="[0-9]{3}) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]))|(";
	$re.="?:(?:1[0-2]|0?[1-9]) *(?:[\/\-\., ]) *(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\., ]) *(?:(?:[0-9";
	$re.="]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:[12][0-9]|3[01]|0?[1-9]) *(?:[\/\-\., ]) *(?:1[0-2]|0?[1-9]";
	$re.=") *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))|(?:(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?";
	$re.="|ly?)))|a(?:pr(?:il)?|ug(?:ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:";
	$re.="tember)?|oct(?:ober)?)) *(?:[\/\-\., ]) *(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd";
	$re.="|2?3rd|[4-9]th|1[0-9]th|20th|2[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9";
	$re.="]{3})))|(?:(?:(?:[12][0-9]|3[01]|0?[1-9])|(?:(?i:[23]?1st|2?2nd|2?3rd|[4-9]th|1[0-9]th|20th|2";
	$re.="[4-9]th|30th))) *(?:[\/\-\., ]) *(?:(?i:(?:j(?:an(?:uary)?|u(?:ne?|ly?)))|a(?:pr(?:il)?|ug(?:";
	$re.="ust)?)|ma(?:y|r(?:ch)?)|(?:nov|dec)(?:ember)?|feb(?:ruary)?|sep(?:tember)?|oct(?:ober)?)) *(?";
	$re.=":[\/\-\., ]) *(?:(?:[0-9]{1,2})|(?:[1-2][0-9]{3})))) *(?:(?:(?:1[0-2]|0?[1-9])(?: *(?:\:) *(?";
	$re.=":[1-5][0-9]|0?[0-9]))?(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))? *(?:(?i:[ap]m)))|(?:(?:2[0-3]|[01";
	$re.="]?[0-9])(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))(?: *(?:\:) *(?:[1-5][0-9]|0?[0-9]))?))))$/";
	return preg_match( $re, $f ) ;
}



?>