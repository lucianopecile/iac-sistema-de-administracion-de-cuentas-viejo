<?php
class FrontController
{
	static function main()
	{
		//Archivo con configuraciones
		require 'config.php';
/*
		//Comprobamos si hay un controlador para llamar, sino llamamos a index
		if(!empty($_GET['controlador']))
		{
			$control=$_GET['controlador'];
			$controllerName = 'Control'.$_GET['controlador'];
		}else{
			$control="ayuda";
			$controllerName = "controlayuda";
		}	

		//armamos el path completo al controller
		$controllerPath = $config->get('controllersFolder') . $controllerName . '.php';
*/
		//
		$pagina=$_GET['pagina'];
		if (empty($pagina))
			$pagina='blanco.php';

		$control = "ayuda";
		$controllerName = "controlayuda";
		$controllerPath = "./controlayuda.php";
		//echo "CTRL PATH".$controllerPath;

		//Incluimos el fichero que contiene nuestra clase controladora solicitada	
		if(is_file($controllerPath))
			require $controllerPath;
		else
			die('El controlador no existe - 404 not found');

		//buscamos el controlador y la accion indicada, sino existe lanzamos un error
		//verHoja es la unica accion posible para este controlador.
		if (is_callable(array($controllerName, "ver_pagina")) == false) 
		{
			trigger_error ($controllerName . '->' . "ver_pagina" . 'no existe', E_USER_NOTICE);
			return false;
		}

		$comparar = trim($controllerName);

		//Si todo esta bien, creamos una instancia del controlador y llamamos a la accion
		$controller = new $controllerName();
		$controller->ver_pagina($pagina);
	}

}
?>