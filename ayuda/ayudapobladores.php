<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>
<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Pobladores</h1>
	
	<ul>
		<li><a href=#alta>C&oacute;mo cargar un nuevo poblador</a></li>
		<li><a href=#mod>C&oacute;mo modificar un poblador</a></li>
		<li><a href=#cony>C&oacute;mo cargar el c&oacute;nyuge de un poblador</a></li>
		<li><a href=#hijos>C&oacute;mo cargar los hijos de un poblador</a></li>
		<li><a href=#baja>C&oacute;mo eliminar un poblador o un c&oacute;nyuge</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>Esta secci&oacute;n explica c&oacute;mo realizar altas, bajas y modificaciones de los pobladores que realizan solicitudes de tierra.
	Tambi&eacute;n se detalla c&oacute;mo se dan de alta los c&oacute;nyuges de los pobladores y como se vinculan y desvinculan entre s&iacute;.
	</p>
</div>

<div class="descripciones" id="alta">
	<h2>C&oacute;mo cargar un nuevo poblador</h2>
	<p>Seleccione del men&uacute; la opci&oacute;n <b>cargar nuevo poblador</b> para acceder al formulario de datos de nuevo poblador y
		ver&aacute; la siguiente pantalla:</p>
	<img alt="datos poblador" src="css/images/datos-poblador.png"></img>
	<p>Complete all&iacute; los campos en blanco de la siguiente manera:</p>
	<ul>
		<li>Ingrese el apellido y nombres del poblador. Es obligatorio de completar.</li>
		<li>Seleccione de la lista el tipo de documento e ingrese el n&uacute;mero de documento. Es obligatorio de completar y debe ser un
			valor &uacute;nico, no puede haber dos personas con el mismo n&uacute;mero de documento.</li>
		<li>Ingrese el domicilio legal y el domicilio postal. Puede ser el mismo.</li>
		<li>Ingrese el n&uacute;mero de tel&eacute;fono.</li>
		<li>Seleccione de la lista la localidad donde reside la persona. Es un campo obligatorio.</li>
		<li>Ingrese la nacionalidad, la fecha de nacimiento y el lugar de nacimiento de la persona.</li>
		<li>Seleccione el estado civil correspondiente al poblador.</li>
		<li>Ingrese el nombre y nacionalidad del padre, y active la casilla <i>en vida</i> si a&uacute;n vive.</li>
		<li>Ingrese el nombre y nacionalidad de la madre, y active la casilla <i>en vida</i> si a&uacute;n vive.</li>
		<li>Por &uacute;ltimo presione el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"></img> para almacenar
			los datos.</li>
		<li>Si el poblador es extranjero, antes de guardar presione el bot&oacute;n 
			<img alt="boton extrajero" src="css/images/boton-extranjero.png"></img>, complete los datos de arribo al pa&iacute;s y presione
			<img src="css/images/boton-aceptar.png" alt="boton aceptar"></img>.</li>
	</ul>
	<p>Si no hubo errores el nuevo poblador est&aacute; cargado en la base de datos del sistema y podr&aacute; asignarle un 
		<a href=#cony>c&oacute;nyuge</a>, <a href=#hijos>hijos</a> o
	<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudarurales.php&&sector=#alta">crear solicitudes</a> a su nombre.</p>
</div>

<div class="descripciones" id="mod">
	<h2>C&oacute;mo modificar un poblador</h2>
	<p>En el men&uacute; de <b>solicitudes y pobladores</b> seleccione la opci&oacute;n <b>listar pobladores</b>. Luego seleccione de la 
		lista el poblador que desea modificar y haga doble clic sobre el mismo, o presione el bot&oacute;n
		<img src="css/images/boton-modificar.png" alt="boton modificar" />. <br/> Modifique los datos de los campos necesarios y presione el
	bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"></img>	para salvar los cambios.
	<br/>
	Recuerde los campos que son obligatorios. Si tiene alguna duda acerca de los datos consulte la secci&oacute;n
	<a href=#alta>c&oacute;mo cargar un nuevo poblador</a>.
	</p>
</div>

<div class="descripciones" id="cony">
	<h2>C&oacute;mo cargar el c&oacute;nyuge de un poblador</h2>
	<p>Para cargar los datos del c&oacute;nyuge de un poblador debe existir primero el poblador en el sistema. El c&oacute;nyuge puede ser
		dado de alta apenas despu&eacute;s que el poblador o en cualquier otro momento.<br/>
		Una vez que se tiene el poblador en la base de datos debe acceder al formulario de dicha persona a trav&eacute;s de la opci&oacute;n
		<b>listar pobladores</b>. Luego seleccione la pesta&ntilde;a c&oacute;nyuge y ver&aacute; un formulario de datos similar al de
		<i>nuevo poblador</i>.<br/>
		Complete los campos con los datos del c&oacute;nyuge y presione el bot&oacute;n
		<img src="css/images/boton-guardar.png" alt="boton guardar"></img> para almacenar los datos. Recuerde que los campos Apellido, 
		Nombres, Tipo y nro de documento y Localidad son obligatorios de completar.
	</p>
	<p>En caso de que el c&oacute;nyuge sea otro poblador que ya existe en la base de datos del sistema, puede seleccionarlo de una lista 
		presionando el bot&oacute;n <img src="css/images/boton-seleccionar-conyuge.png" alt="boton conyuge"></img> y haciendo doble clic
		sobre la persona en cuesti&oacute;n. Luego presione <img src="css/images/boton-guardar.png" alt="boton guardar"></img>.
	</p>
</div>

<div class="descripciones" id="hijos">
	<h2>C&oacute;mo cargar los hijos de un poblador</h2>
	<p>Para cargar los hijos de un poblador debe acceder al formulario de datos de la persona a trav&eacute;s de la opci&oacute;n <b>listar
	pobladores</b>.<br/>
	Luego seleccione la pesta&ntilde;a hijos y desde all&iacute; se administra la carga de los hijos del poblador.<br/>
	Para cargar un nuevo hijo presione el bot&oacute;n <img src="css/images/boton-nuevo-hijo.png" alt="boton nuevo hijo"></img> y complete
	los campos que aparecen en pantalla con los datos del hijo. Repita esta operaci&oacute;n con cada hijo que necesita cargar.
	</p>
</div>

<div class="descripciones" id="baja">
	<h2>C&oacute;mo eliminar un poblador o un c&oacute;nyuge</h2>
	<p>Para eliminar un poblador debe acceder a la lista de pobladores desde la opci&oacute;n <b>listar pobladores</b> del men&uacute;.<br/>
	Luego seleccione la persona que desea eliminar y presione el bot&oacute;n
	<img src="css/images/boton-eliminar.png" alt="boton eliminar"></img>.<br/>
	Cabe aclarar que para eliminar un poblador, no debe tener un c&oacute;nyuge asociado, ni hijos, ni ninguna solicitud a su nombre. En caso
	de tener alguna solicitud deber&aacute; darla de baja primero y luego eliminar el poblador; y lo mismo se aplica para los hijos.<br/>
	En el caso del c&oacute;nyuge, existe en la pesta&ntilde;a de c&oacute;nyuge el bot&oacute;n
	<img src="css/images/boton-desvincular.png" alt="boton desvincular"></img> el cual elimina la relaci&oacute;n entre dos pobladores. De
	esta manera se permite que sea posible eliminar uno o ambos pobladores. </p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
