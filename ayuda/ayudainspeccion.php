<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Informes de inspecci&oacute;n rural</h1>
	<ul>
		<li><a href=#alta>C&oacute;mo cargar un nuevo informe de inspecci&oacute;n</a></li>
		<li><a href=#mod>C&oacute;mo modificar un informe</a></li>
		<li><a href=#precio>C&oacute;mo definir el precio por hect&aacute;rea</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>Esta secci&oacute;n explica como se realiza la administraci&oacute;n de informes de inspecci&oacute;n de tierras rurales. C&oacute;mo
		se cargan nuevos informes, los formatos de los datos, c&oacute;mo se define el valor de un campo o tierra y c&oacute;mo se modifican
		los valores. Es importante destacar que los datos y valores que se ingresen en esta instancia son los que determinaran el valor final
		de la tierra solicitada al momento de la liquidaci&oacute;n de la cuenta.
	</p>
</div>
	<div class="descripciones">
	<img alt="ubicacion tierra" src="css/images/ubicacion-tierra.png"></img>
	<img alt="factores medio" src="css/images/factores-tierra.png"></img>
</div>
<div class="descripciones" id="alta">
	<h2>C&oacute;mo cargar un informe de inspecci&oacute;n</h2>
	<p>Para cargar un nuevo informe debe seleccionar del men&uacute; la opci&oacute;n <b>administrar informes de inspecci&oacute;n</b>.<br/>
		All&iacute; ver&aacute; una pantalla con las diferentes tareas de administraci&oacute;n de informes.<br/>
		Presione el bot&oacute;n <img src="css/images/boton-nuevo-informe.png" alt="boton nuevo informe"></img> para acceder al formulario de
		carga del nuevo informe.<br/>
		Una vez all&iacute; deber&aacute; ingresar los datos de la siguiente manera:
	</p>
	<p>Primero seleccione el expediente al cual corresponde el informe presionando el bot&oacute;n
		<img src="css/images/boton-buscar-expediente.png" alt="boton buscar expediente"></img> y seleccionando de la lista el expediente en
		cuesti&oacute;n. Es obligatorio seleccionar un expediente para asociarlo al informe que se est&aacute; dando de alta.<br/>
		Luego complete los datos de la tierra, tenga en cuenta que los datos que ingrese en esta pantalla son los que definir&aacute;n el
		valor final de la tierra al momento de la liquidaci&oacute;n.
	</p>
	<ul>
		<li>
			<img style="float:right; border:solid thin; margin-left:5px;" alt="superficie tierra" src="css/images/superficie-tierra.png"/>
		</li>
		<li>Ingrese la fecha en la que se realiz&oacute; la inspecci&oacute;n. Es un dato obligatorio.</li>
		<li>Ingrese la superficie total de la tierra solicitada. Es un dato obligatorio y debe completarlo con el siguiente formato: <br/>
		"hect&aacute;reas-&aacute;rea-centi&aacute;reas-dm2", por ejemplo, "790-23-58-09" &oacute; "1250-00-34" son valores v&aacute;lidos; y
		no lo son por ejemplo, "790-235809" &oacute; "1250.0034".</li>
		<li>Ingrese la superficie de invernada y de veranada. No es obligatorio pero puede influir en el precio de la tierra. Se debe
			utilizar el mismo formato que para la superficie total.</li>
		<li>Seleccione la zona agroecol&oacute;gica donde se encuentra la tierra. Es un campo obligatorio de cargar.</li>
		<li>Seleccione el punto de embarque m&aacute;s cercano a la tierra solicitada. Es obligatorio de completar.</li>
		<li>Seleccione el rango de distancia al punto de embarque m&aacute;s cercano que corresponde a la tierra en cuesti&oacute;n.</li>
		<li>
			<img style="float:right; border:solid thin; margin-left:5px;" alt="factores tierra" src="css/images/factores-tierra.png"/>
		</li>
		<li>Seleccione la receptividad o capacidad ganadera que ofrece la tierra.</li>
		<li>Active las casillas seg&uacute;n corresponde con lo factores del medio que se corresponden con esta tierra.</li>
		<li>Si considera relevante alg&uacute;n otro dato lo puede incorporar en el campo <i>observaciones</i>.</li>
		<li>Por &uacute;ltimo, presione el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"></img> para guardar los
			datos del informe.</li>
	</ul>
	<p>Si el alta tuvo &eacute;xito, el informe ya se encuentra en el sistema listo para consultar sus datos. Puede consultar el informe
		presionando el bot&oacute;n <img src="css/images/boton-ver-informe.png" alt="boton ver informe"></img> desde la pantalla de
		administraci&oacute;n de informes de inspecci&oacute;n.<br/>
		Cabe destacar que el precio de la hect&aacute;rea para la tierra a&uacute;n no se encuentra definido y lo debe definir manualmente,
		consulte la secci&oacute;n <a href=#precio>C&oacute;mo definir el precio por hect&aacute;rea</a>.
	</p>
</div>

<div class="descripciones" id="mod">
	<h2>C&oacute;mo modificar un informe</h2>
	<p>Si necesita realizar modificaciones a los datos del informe acceda a la opci&oacute;n <b>administrar informes de inspecci&oacute;n</b>
		del men&uacute;.<br/>
		Luego presione el bot&oacute;n <img src="css/images/boton-modificar-informe.png" alt="boton modificar informe"/> y seleccione de la
		lista el informe que desea modificar haciendo doble clic.<br/>
		Entonces acceder&aacute; al	formulario con los datos actuales del informe.
	</p>
	<p>
		<img style="float:right; border:solid thin; margin-left:5px;" alt="ubicacion tierra" src="css/images/ubicacion-tierra.png"/>
		Desde all&iacute; puede modificar los valores de los campos que desea. Tenga en cuenta que la mayor&iacute;a de estos valores puede
		alterar el precio final de la tierra por lo que tal vez quiera liquidar la cuenta nuevamente si ya lo hizo anteriormente.<br/>
		Para m&aacute;s datos sobre c&oacute;mo liquidar una cuenta consulte la secci&oacute;n
		<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuentas.php&&sector=#reliquidar">c&oacute;mo
			reliquidar/modificar una cuenta</a>.<br/>
		Recuerde de completar los campos que son obligatorios y de mantener el formato correcto para cada valor requerido. Consulte la
		secci&oacute;n <a href=#alta>c&oacute;mo cargar un nuevo informe de inspecci&oacute;n</a> si tiene dudas sobre el formato de los
		valores.
	</p>
	<p>Si necesita eliminar un informe deber&aacute; acceder a la misma pantalla de <b>administraci&oacute;n de informes de inspecci&oacute;n
		</b> y presionar el bot&oacute; <img src="css/images/boton-eliminar-informe.png" alt="boton eliminar informe"/>. Del mismo modo que
		en el caso anterior, seleccione el informe que desea eliminar. Por &uacute;ltimo, si est&aacute; seguro de concluir la
		operaci&oacute;n, confirme presionando el bot&oacute; <img src="css/images/boton-eliminar.png" alt="boton eliminar"/> al final del
		formulario.<br/>
		Es importante mencionar que un informe que ya tiene una cuenta liquidada no puede ser eliminado a menos que se elimine primero dicha
		cuenta.
	</p>
</div>

<div class="descripciones" id="precio">
	<h2>C&oacute;mo definir el precio por hect&aacute;rea</h2>
	<p>El precio de la hect&aacute;rea lo deber&aacute; definir un usuario autorizado para tal fin y deber&aacute; acceder a la pantalla de
		<b>ajustar precio hect&aacute;rea</b> del men&uacute; de opciones. <br/> Luego presione el bot&oacute;n
		<img src="css/images/boton-ajustar-precio.png" alt="boton ajustar precio"/> y acceder&aacute; al formulario de datos del informe.
	</p>
	<p>
		Desde all&iacute; presione el bot&oacute;n <img src="css/images/boton-valores-tierra.png" alt="boton valores tierra"/> a la derecha
		de la pantalla y se	abrir&aacute; la tabla de precios por hect&aacute;rea seg&uacute;n la ubicaci&oacute;n de la tierra y la
		capacidad ganadera.<br/>
		<img style="float:right; border:solid thin; margin:5px;" alt="tabla valores tierra" src="css/images/tabla-valores-tierra.png"/>
		Seleccione el precio correspondiente haciendo doble clic en el cuadro que lo contiene y el valor seleccionado se cargar&aacute;
		autom&aacute;ticamente al informe y se utilizar&aacute; para la liquidaci&oacute;n de la cuenta.<br/>
		Si lo desea tambi&eacute;n puede modificar desde all&iacute; los factores del medio que afectan a la tierra.
	</p>
	<p>
		Una vez concluidas las modificaciones debe presionar el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar"/>
		para salvar los cambios.
		Recuerde que si la cuenta ya hab&iacute;a sido creada y liquidada con un valor anterior deber&aacute; volver a realizar la
		liquidaci&oacute;n para que los valores coincidan y sean consistentes.
	</p>

</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
