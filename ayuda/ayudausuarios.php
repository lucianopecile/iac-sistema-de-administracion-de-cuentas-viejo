<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>


<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Usuarios</h1>
	
	<ul>
		<li><a href=#administrar>C&oacute;mo administrar usuarios</a></li>
		<li><a href=#clave>C&oacute;mo cambiar la contrase&ntilde;a</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>
		Esta secci&oacute;n explica en simples pasos c&oacute;mo funciona la administraci&oacute;n de usuarios. C&oacute;mo se dan de alta
		nuevos usuario, c&oacute;mo se otorgan permisos y c&oacute;mo se modifica la clave de acceso.
	</p>
</div>

<div class="descripciones" id="administrar">
	<h2>C&oacute;mo administrar usuarios</h2>
	<p>La administraci&oacute;n de usuarios consiste en dos funciones principales: altas, bajas y modificaciones de los usuarios y la cesi&oacute;n de
		permisos para cada uno de ellos.<br/>
		Todas estas tareas se realizan desde la opci&oacute;n <b>administrar</b> del submen&uacute; de <b>usuarios</b>.
	</p>
	<p>Para dar de alta un nuevo usuario acceda a la pantalla de administraci&oacute;n de usuarios mencionada y presione el bot&oacute;n
	<img alt="boton agregar" src="css/images/boton-agregar.png"/>. En la pantalla siguiente complete los datos requeridos del usuario, a
	saber:
	</p>
	<ul>
		<li>Seleccione el empleado que utilizar&aacute; dicho usuario.</li>
		<li>Ingrese el nombre de usuario.</li>
		<li>Presione el bot&oacute;n <img alt="boton restaurar clave" src="css/images/boton-restaurar-clave.png"/> que asignar&aacute; al
			usuario una contrase&ntilde;a por defecto igual al nombre de usuario definido.</li>
		<li>Seleccione el departamento o &aacute;rea al que pertenece el usuario.</li>
		<li>Por &uacute;ltimo, agregue a la lista de <i>items permitidos</i> las funcionalidades del men&uacute; para las cuales se le otorgar&aacute; permiso al
			usuario.</li>
	</ul>
	<p>Para la modificaci&oacute;n de los datos del usuario, as&iacute; como tambi&eacute;n de los permisos asignados, existe el bot&oacute;n
		<img alt="boton modificar" src="css/images/boton-modificar.png"/>. Seleccione el usuario y presione dicho bot&oacute;n, modifique los datos o
		permisos necesarios y seleccione <img alt="boton guardar" src="css/images/boton-guardar.png"/> para salvar los cambios.
	</p>
	<p>Del mismo modo, para eliminar un usuario existente, selecci&oacute;nelo y presione el bot&oacute;n
		<img alt="boton eliminar" src="css/images/boton-eliminar.png"/>.
	</p>

</div>

<div class="descripciones" id="clave">
	<h2>C&oacute;mo cambiar la contrase&ntilde;a</h2>
	<p>Para cambiar su contrase&ntilde;a de acceso debe acceder a la opci&oacute;n <b>cambiar contrase&ntilde;a</b> del men&uacute; principal.
		<br/>
		En la siguiente pantalla active la casilla <i>cambiar clave</i>, luego ingrese en el campo <i>clave</i> la nueva contrase&ntilde;a e
		ingrese nuevamente la contrase&ntilde;a en el siguiente campo para confirmar que no haya errores de tipeo.<br/>
		Por &uacute;ltimo, presione el bot&oacute;n <img src="css/images/boton-guardar.png" alt="boton guardar" /> para guardar la nueva
		contrase&ntilde;a.
	</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
