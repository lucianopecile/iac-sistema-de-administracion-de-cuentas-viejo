<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelosolicitudconcepto.php';
require_once '../config.php'; //Archivo con configuraciones.
$solicitudconcepto = new modelosolicitudconcepto();
$solicitudconcepto->putIdSolicitud($_POST['idsolicitud']);
$conceptosliq=$solicitudconcepto->TraerDetalle();
$totalgastos=0;
$conceptos="";

while ($varlab = mysql_fetch_object($conceptosliq))
{
$conceptos[]=array( "descripcion"=>$varlab->descripcion,
   					"valor"=>	$varlab->valor);
					$totalgastos=$totalgastos+$varlab->valor;

}

$cuotas = new modelocuota();
$cuotas->putIdCuenta($_POST['idcuenta']);
$cuotadesde=$_POST['cuotadesde']*1;
$cuotahasta=$_POST['cuotahasta']*1;
$arrcuotas=$cuotas->cuotas();
if(is_array($arrcuotas)){
$totalRows=count($arrcuotas);
if ( $totalRows != 0){
$cta=1;
$pdf=new FPDF();
$pdf->AddFont('barras','','barras.php');
foreach($arrcuotas as $row){
$nrocuota=$row['numerocuota'];
if($nrocuota>=$cuotadesde && $nrocuota<=$cuotahasta){
if($cta==1){
$pdf->AddPage();
$fila=5;
$columna=5;}
if($cta==2){
$fila=100;
$columna=5;}
if($cta==3){
$fila=195;
$columna=5;}
$amortizacionventa=($row['capital']);
$interesglobal=($row['interes']);
$deudaatrasada=0;
$deudaglobal=0;
$nrocuota=$row['numerocuota'];
//si la cuenta comienza con 'A' es de pastaje
if($_POST['nrocuenta'][0] == 'A')
{
    $taloncuota = "Pastaje";
}else{
    if($nrocuota==0)
        $taloncuota="Formalización";
    else
        $taloncuota="CUOTA ".$row['numerocuota']." / ".$row['cantcuotas'];
}
$stringcta=trim(sprintf("%s",$nrocuota));
$prestamo=$_POST['idcuenta'];
$stringpmo=trim(sprintf("%s",$prestamo));
$nropmo=str_pad($stringpmo,7, "0", STR_PAD_LEFT);
$nrocta=str_pad($stringcta,3, "0", STR_PAD_LEFT);
$oper="1";//operacion cobro normal cuota
$valorcuota=$row['montocuota']+$totalgastos;
if($valorcuota>999999.99){
echo "El valor a pagar mas los gastos fijos supera los $ 999.999,99 - No puede emitirse el talon";
return(false);
}
$productor=$row['solicitante']." - ".strtoupper($_POST['titulares']);
$fecha1=$vto1=$row['fechavenc'];
$fechaemision="Fecha de Emision : ".date('d/m/Y');
$cuenta=$row['cuenta'];
$convenio=$row['convenio'];
//registro los conceptos a cobrar en la cuota
$cuotas->putIdCuota($row['id']);
$cuotas->putConceptos($totalgastos);
$cuotas->guardarConceptos();
include('talon.php');
$cta=$cta+1;
if($cta>3 || $cta>$totalRows){
$cta=1;}
}
}
$pdf->Output();
return true;}else
{echo "No se pudieron recuperar las cuotas";}}
return(false);
?>