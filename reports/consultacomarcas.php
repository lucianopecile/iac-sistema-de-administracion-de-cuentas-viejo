<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniocomarcas.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelocuenta.php';
require_once '../modelos/modeloparametro.php';
require_once '../modelos/modelolocalidad.php';
require_once '../librerias/funcionesphp.php';
require_once '../config.php';

$fechadesde = $_POST["fechadesde"];
$fechahasta = $_POST["fechahasta"];
$condicion_fecha = "fechacuenta>'".cadenaAFecha($fechadesde)."' && fechacuenta<'".cadenaAFecha($fechahasta)."'";
$cuota=new modelocuota();
$localidad=new modelolocalidad();
$cuentas=new modelocuenta();

if(!isset($_POST["vwZonasDisponibles"]))
    $_POST["vwZonasDisponibles"]=array();


$nuevacondicion="";
$cuentas->putElegidas($_POST["vwZonasDisponibles"]);
$condicion=$cuentas->ObtenerZonas();
if(!empty($condicion) && !empty($condicion_fecha))
	$nuevacondicion="(".$condicion.") && (".$condicion_fecha.")";
else
	if(!empty($condicion))
		$nuevacondicion=$condicion;
	else
		$nuevacondicion=$condicion_fecha;

$resultado=$cuentas->traertotcuentaZonas($nuevacondicion);
$localidad=new modelolocalidad();

if(!$resultado)
{
	$Elegidas=$localidad->TraerElegidas($_POST["vwZonasDisponibles"]);
	$data['Elegidas']= $Elegidas;
	$this->view->show1("consultacomarcas.html", $data);
}else{
    $listadozonas=$localidad->TraerElegidas($_POST["vwZonasDisponibles"]);;
    $totalfilaszonas=count($listadozonas);
	while($varc = mysql_fetch_object($resultado))
	{
		if ($varc->tiposolicitud == RURAL)
			$tipo = "RURAL";
		else
			$tipo = "URBANA";
		$idcuenta = $varc->id;
		$cuota->putIdCuenta($idcuenta);
		//obtengo las cuotas adeudadas y vencidas de la cuenta
		$lista_cuotas = $cuota->listadoCuotasCuentaDeuda("");
		if(count($lista_cuotas) > 0)
		{
			$saldo_total=$cobrado_total=$int_mora_total=0;
			foreach ($lista_cuotas as $c)
			{
				$cuota->putIdCuota($c['id']);
				$cuota->traerCuota();
				$saldo_total += $cuota->getSaldo()*1;
				$cobrado_total += $cuota->getCobrado()*1;
				$int_mora_total += $cuota->getInteresMora()*1;
			}
			//genero el arreglo con todos los datos
			$listado[$i]['localidad'] = $varc->zona;
			$listado[$i]['saldototal'] = $saldo_total;
			$listado[$i]['cobradototal'] = $cobrado_total;
			$listado[$i]['moratotal'] = $int_mora_total;
			$listado[$i]['nrocuenta'] = $varc->nrocuenta;
			$listado[$i]['valorliquidacion'] = $varc->valorliquidacion;
			$listado[$i]['titular'] = $varc->apellido.", ".$varc->nombres;
			$listado[$i]['tipo'] = $tipo;
			$listado[$i]['fechacuenta'] = $varc->fechacuenta;
			$i++;	
		}
	}
}

if($listado)
{
	$pdf = new APDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P', "Legal");
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=50;
	$columna=10;
	$pdf->SetFont('Times','B');
	
	//======================ENCABEZADO==========================================
	$pdf->SetFont('Times','B',10);
	$pdf->SetXY($columna,$fila);
	$pdf->Write(4, "Fecha: ".date('d/m/Y'));
	$pdf->SetFont('Times','B');
	$pdf->SetXY($columna,50);
	$pdf->Cell(100,(5*($totalfilaszonas/2))+10,'',1,1,'C',0);
	if($listadozonas>0)
	{
		$pdf->SetXY($columna,$fila);
		$renglonzona="";
		foreach($listadozonas as $zonas)
		{
			if(empty($renglonzona)){
				$renglonzona=$zonas["descripcion"];
			}else{
				$renglonzona=$renglonzona." - ".$zonas["descripcion"];
				$fila=$fila+5;
				$pdf->SetXY($columna,$fila);
				$pdf->Write(4,$renglonzona);
				$renglonzona="";
			}
		}
		if(!empty($renglonzona))
		{
			$fila=$fila+5;
			$pdf->SetXY($columna,$fila);
			$pdf->Write(4,$renglonzona);
			$renglonzona="";
		}
	}else{
		$fila=$fila+5;
		$pdf->SetXY($columna,$fila);
		$pdf->Write(4," Todas Las Comarcas");
	}

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(172,10,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Localidad", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+20,$fila);
	$pdf->drawTextBox("Cuenta", 12, 10,'C','M', 1);
	$pdf->SetXY($columna+32,$fila);
	$pdf->drawTextBox("Titular", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+52,$fila);
	$pdf->drawTextBox("Tipo", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+72,$fila);
	$pdf->drawTextBox("Monto liquidado", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+92,$fila);
	$pdf->drawTextBox("Saldo total", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+112,$fila);
	$pdf->drawTextBox("Cobrado total", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+132,$fila);
	$pdf->drawTextBox("Inter�s por mora calculado", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+152,$fila);
	$pdf->drawTextBox("Deuda total  ", 20, 10,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+10;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($listado as $c)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(172,10,'',1,1,'C');
		//columna localidad
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($c['localidad'], 20, 10,'C','M', 1);
		//columna cuenta
		$pdf->SetXY($columna+20,$fila);
		$pdf->drawTextBox($c['nrocuenta'], 12, 10,'C','M', 1);
		//columna titular
		$pdf->SetXY($columna+32,$fila);
		$pdf->drawTextBox($c['titular'], 20, 10,'L','M', 1);
		//columna tipo
		$pdf->SetXY($columna+52,$fila);
		$pdf->drawTextBox($c['tipo'], 20, 10,'C','M', 1);
		//columna monto liquidado
		$pdf->SetXY($columna+72,$fila);
		$valor = "$ ".number_format($c['valorliquidacion'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 10,'R','M', 1);
		//columna saldo total
		$pdf->SetXY($columna+92,$fila);
		 $valor = "$ ".number_format($c['saldototal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 10,'R','M', 1);
		//columna cobrado total
		$pdf->SetXY($columna+112,$fila);
		$valor = "$ ".number_format($c['cobradototal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 10,'R','M', 1);
		//columna interes mora calculado
		$pdf->SetXY($columna+132,$fila);
		$valor = "$ ".number_format($c['moratotal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 10,'R','M', 1);
		//columna deuda totoal 
		$pdf->SetXY($columna+152,$fila);
		$valor = "$ ".number_format($c['saldototal']+$c['moratotal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 10,'R','M', 1);
		$fila=$fila+10;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=50;
			$columna=10;
			//======================ENCABEZADO==========================================
			$pdf->SetFont('Times','B',10);
			$pdf->SetXY($columna,$fila);
			$pdf->Write(4, "Fecha: ".date('d/m/Y'));
			$pdf->SetFont('Times','B');
			$pdf->SetXY($columna,50);
			$pdf->Cell(100,(5*($totalfilaszonas/2))+10,'',1,1,'C',0);
			if($listadozonas>0)
			{
				$pdf->SetXY($columna,$fila);
				$renglonzona="";
				foreach($listadozonas as $zonas)
				{
					if(empty($renglonzona)){
						$renglonzona=$zonas["descripcion"];
					}else{
						$renglonzona=$renglonzona." - ".$zonas["descripcion"];
						$fila=$fila+5;
						$pdf->SetXY($columna,$fila);
						$pdf->Write(4,$renglonzona);
						$renglonzona="";
					}
				}
				if(!empty($renglonzona))
				{
					$fila=$fila+5;
					$pdf->SetXY($columna,$fila);
					$pdf->Write(4,$renglonzona);
					$renglonzona="";
				}
			}else{
				$fila=$fila+5;
				$pdf->SetXY($columna,$fila);
				$pdf->Write(4," Todas Las Comarcas");
			}

			// Encabezado Fila
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(172,10,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Localidad", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+20,$fila);
			$pdf->drawTextBox("Cuenta", 12, 10,'C','M', 1);
			$pdf->SetXY($columna+32,$fila);
			$pdf->drawTextBox("Titular", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+52,$fila);
			$pdf->drawTextBox("Tipo", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+72,$fila);
			$pdf->drawTextBox("Monto liquidado", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+92,$fila);
			$pdf->drawTextBox("Saldo total", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+112,$fila);
			$pdf->drawTextBox("Cobrado total", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+132,$fila);
			$pdf->drawTextBox("Inter�s por mora calculado", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+152,$fila);
			$pdf->drawTextBox("Deuda total  ", 20, 10,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+10;
			//Fin Encabezado de Fila
			$pdf->SetFont('Times','',8);
		}
	}
	$pdf->Output();
}

?>