<?php
//Calculo Cuota 1
$valorenterocuota =intval($valorcuota);
$stringvc=trim(sprintf("%s",$valorenterocuota));
$n =trim(sprintf("%s",$valorcuota));
$aux = (string) $n;
$decimal="00";
if(strpos( $aux, "." )>0){
$decimal = trim(substr( $aux, strpos( $aux, "." )+1,strlen(trim($aux)) ));
if(strlen($decimal)==1){
$decimal=$decimal."0";}
$decimal=str_pad($decimal,2, "0", STR_PAD_LEFT);}
$entero=str_pad($stringvc,6, "0", STR_PAD_LEFT);
//Calculo Gastos
$valorenterogastos =intval($totalgastos);
$stringastos=trim(sprintf("%s",$valorenterogastos));
$ngastos =trim(sprintf("%s",$totalgastos));
$auxgastos = (string) $ngastos;
$decimalgastos="00";
if(strpos( $auxgastos, "." )>0){
$decimalgastos = trim(substr( $auxgastos, strpos( $auxgastos, "." )+1,strlen(trim($auxgastos)) ));
if(strlen($decimalgastos)==1){
$decimalgastos=$decimalgastos."0";}
$decimalgastos=str_pad($decimalgastos,2, "0", STR_PAD_LEFT);}
$enterogastos=str_pad($stringastos,4, "0", STR_PAD_LEFT);
//si la cuenta comienza con 'A' es de pastaje
if($_POST['nrocuenta'][0] == 'A')
{
    $titulo = "MONTO";
}else{
    $titulo = "AMORT. VENTA";
}
// Operacion
$operacion=$nropmo.$nrocta.$oper.$enterogastos.$decimalgastos;
// Fecha
$mdia = substr($fecha1, 0, 2);
$mmes = substr($fecha1, 3, 2);
$manio =substr($fecha1, 8, 2);
$m2dia =substr($fecha2, 0, 2);
$m2mes =substr($fecha2, 3, 2);
$m2anio =substr($fecha2, 8, 2);
$convenio=str_pad(trim($convenio),5, "0", STR_PAD_RIGHT);
$barcode=$convenio.$operacion.$manio.$mmes.$mdia.$entero.$decimal;
$cad = "313131313131313131313131313131313131";
$i=1;
$acumulador = 0;
//echo "Codigo de Barra ".$barcode."</br></br>";
for($i=0;$i<36;$i++){
$dig =1*(substr($barcode,$i,1));
$multiplo = 1*(substr($cad,$i,1));
$acumulador = $acumulador+($dig * $multiplo);
//echo "Posicion  : ".$i."&nbsp;Calculo ". $dig."  * ".$multiplo." =".$dig*$multiplo."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRODUCTO ACUMULADO  ".$acumulador."</br>";
}
$digito=$acumulador%10;
//echo "SUMA TOTAL DE PRODUCTOS ".$acumulador."</br>";
//echo "DIGITO VERIFICADOR ".$digito."</br>";
$barcode="*".$barcode.$digito."*";
$pdf->SetDrawColor(0, 0, 0);
$pdf->Image('../css/images/chequera1.jpg',$columna,$fila,200,0,'JPG');
$pdf->Image('../css/images/logoiac.jpg',$columna,$fila,20,0,'JPG');
$pdf->SetXY($columna,$fila);
$pdf->Cell(200,80,'',1,1,'C');
$pdf->SetXY($columna+45,$fila+5);
$pdf->Setfont('times','',16);
$pdf->Setfont('TIMES','',11);
$pdf->SetXY($columna+45,$fila+5);
$pdf->Write(4,utf8_decode('INSTITUTO AUTÁRQUICO DE COLONIZACIÓN Y FOMENTO RURAL -  IAC'));
$pdf->Setfont('TIMES','',4);
$pdf->SetXY($columna+81,$fila+70);
$pdf->Write(4,'TALON NO VALIDO COMO FACTURA');
$pdf->Setfont('TIMES','',5);
$pdf->SetXY($columna+81,$fila+73);
$pdf->Write(4,'El pago de la cuota queda legalmente acreditado mediante la presentaci�n del ticket con el sello y/o la firma del cajero del banco, de otras entidades');
$pdf->SetXY($columna+81,$fila+75);
$pdf->Write(4,'habilitadas o, por escritura mecanizada de seguridad.');
$pdf->Setfont('TIMES','',4);
$pdf->SetXY($columna+5,$fila+75);
$pdf->Write(4,$fechaemision);
$pdf->Setfont('times','B',8);
$pdf->SetXY($columna+22,$fila+18);
$pdf->Write(2,utf8_decode('Cuenta  Nº : ').$cuenta);
$pdf->Setfont('times','',8);
$pdf->SetXY($columna+3,$fila+25);
$pdf->Write(2,"TITULARES : ".$productor);
$fila=$fila+2;
$pdf->SetXY($columna+3,$fila+30);
	// Encabezado Fila Detalle Cuenta
	$pdf->SetFillColor(224, 224, 224);
	$pdf->SetFont('Times','',6);
    $pdf->SetXY($columna+3,$fila+28);
	$pdf->Cell(120,5,'',1,1,'C',1);
    $pdf->SetXY($columna+3,$fila+28);
	$pdf->drawTextBox("CONCEPTO", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+23,$fila+28);
	$pdf->drawTextBox($titulo, 20, 5,'C','M', 1);
	$pdf->SetXY($columna+43,$fila+28);
	$pdf->drawTextBox("INTERESES", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+63,$fila+28);
	$pdf->drawTextBox("DEUDA ATRASADA", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+83,$fila+28);
	$pdf->drawTextBox("INT. PUNITORIOS", 20, 5,'C','M', 1);
    $pdf->SetXY($columna+103,$fila+28);
	$pdf->drawTextBox("CUENTA", 20, 5,'C','M', 1);
    $pdf->SetXY($columna+3,$fila+33);
	$pdf->Cell(100,5,'',1,1,'C',0);
	$pdf->SetXY($columna+3,$fila+33);
	$pdf->drawTextBox($taloncuota, 20, 5,'C','M', 1);
	$pdf->SetXY($columna+23,$fila+33);
	$pdf->drawTextBox($amortizacionventa, 20, 5,'C','M', 1);
	$pdf->SetXY($columna+43,$fila+33);
	$pdf->drawTextBox($interesglobal, 20, 5,'C','M', 1);
	$pdf->SetXY($columna+63,$fila+33);
	$pdf->drawTextBox($deudaatrasada, 20, 5,'C','M', 1);
	$pdf->SetXY($columna+83,$fila+33);
	$pdf->drawTextBox($deudaglobal, 20, 5,'C','M', 1);
    $pdf->SetXY($columna+103,$fila+33);
	$pdf->drawTextBox($_POST['nrocuenta'], 20, 5,'C','M', 1);

$filaconceptos=$fila+40;
$columnaconceptos=$columna;
$i=0;
if(!empty($conceptos)){
foreach($conceptos as $varlab )
		{
		if($i<6)
			$columnaconceptos=$columna+3;
		else   
		    $columnaconceptos=$columna+35;
$pdf->SetXY($columnaconceptos,$filaconceptos);
$pdf->drawTextBox($varlab['descripcion'], 20, 5,'L','M', 1);
$pdf->SetXY($columnaconceptos+20,$filaconceptos);
$pdf->drawTextBox($varlab['valor'], 10, 5,'R','M', 1);

$filaconceptos=$filaconceptos+5;

$i=$i+1;
if($i==6)
$filaconceptos=$fila+40;
}	
}	
$fila=$fila-2;
$pdf->Setfont('times','B',10);
$pdf->SetXY($columna+145,$fila+30);
$pdf->Write(2,'Vencimiento :'.$vto1);
$pdf->SetDrawColor(192, 192, 192);
$pdf->Line($columna+145, $fila+33, $columna+200, $fila+33);
$pdf->SetXY($columna+145,$fila+40);
$pdf->Write(2,'Total a pagar  : '."$ ".number_format($valorcuota,2,',','.'));
$pdf->Line($columna+145, $fila+43, $columna+200, $fila+43);
$pdf->Setfont('times','',8);
$pdf->SetXY($columna+145,$fila+60);
$pdf->SetDrawColor(0, 0, 0);
$pdf->Setfont('barras','',24);
$pdf->SetXY($columna+81,$fila+60);
$pdf->MultiCell(152,1,$barcode);
$pdf->SetXY($columna+26,$fila+95);
?>