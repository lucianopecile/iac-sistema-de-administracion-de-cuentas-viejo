<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/disenioingresos.php' ;
require_once '../librerias/config.php';
require_once '../librerias/funcionesphp.php';
require_once '../modelos/modeloarchivocobro.php';
require_once '../config.php'; //Archivo con configuraciones.

$archivos = new Modeloarchivocobro();
//armado de la condicion de filtro por fechas
$condicion = " && fechaimportacion>='".cadenaAFecha($_POST['fechadesde'])."' && fechaimportacion<='".cadenaAFecha($_POST['fechahasta'])."' ";
$lista = $archivos->listadoArchivosMovimientos($condicion);
if(count($lista) <= 0)
{
	$mensaje = htmlentities("No hay archivos procesados.");
	$data['mensaje'] = $mensaje;
	printf("%s", $mensaje);
	return false;
}

$lista_sin_movs = $archivos->listadoArchivosSinMovimientos();
if(count($lista_sin_movs) > 0)
{
	$lista = array_merge($lista, $lista_sin_movs);
}

if($lista)
{
	$pdf = new APDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P', "Legal");
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=30;
	$columna=30;
	$pdf->SetFont('Times','B');

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(120,5,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+20,$fila);
	$pdf->drawTextBox("Nombre", 50, 5,'C','M', 1);
	$pdf->SetXY($columna+70,$fila);
	$pdf->drawTextBox("Monto", 30, 5,'C','M', 1);
	$pdf->SetXY($columna+100,$fila);
	$pdf->drawTextBox("Estado", 20, 5,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+5;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($lista as $a)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(120,5,'',1,1,'C');
		//columna fecha
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($a['fecha'], 20, 5,'C','M', 1);
		//columna nombre
		$pdf->SetXY($columna+20,$fila);
		$pdf->drawTextBox($a['nombre'], 50, 5,'L','M', 1);
		//columna monto
		$pdf->SetXY($columna+70,$fila);
			//si el monto es cero escribo "Sin movimientos"
		$valor = $a['montototal'] > 0?"$ ".number_format($a['montototal'],2,",",".")." ":"Sin movimientos"; 
		$pdf->drawTextBox($valor, 30, 5,'R','M', 1);
		//columna estado
		$pdf->SetXY($columna+100,$fila);
		$pdf->drawTextBox($a['estado'], 20, 5,'C','M', 1);
		$fila=$fila+5;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=30;
			$columna=30;

			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(120,5,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
			$pdf->SetXY($columna+20,$fila);
			$pdf->drawTextBox("Nombre", 50, 5,'C','M', 1);
			$pdf->SetXY($columna+70,$fila);
			$pdf->drawTextBox("Monto", 30, 5,'C','M', 1);
			$pdf->SetXY($columna+100,$fila);
			$pdf->drawTextBox("Estado", 20, 5,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+5;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	$pdf->Output();
}

?>