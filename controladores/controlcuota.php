<?php
require_once 'modelos/modelocuota.php';
require_once 'modelos/modelocuenta.php';
require_once 'modelos/modeloparametro.php';
require_once 'modelos/modelomovimiento.php';
require_once 'modelos/modelolog.php';

class ControlCuota
{
 
 	function __construct()
	{
		$this->view = new View();
	}
	
	
 
//---------------------------------------------------------------------------------

    public function mostrarcuota()
    // muestra todos los cuotas en un html con una tabla
    {
        $this->view->show1("cuotas.html", $data);
    }

//---------------------------------------------------------------------------------

	public function mostrarcuenta()
	// muestra todos los cuotas en un html con una tabla
	{
            $cuentas = new ModeloCuenta();
            $liztado = $cuentas->listadoTotal();
            $data['liztado'] = $liztado;
            $this->view->show1("listaeditar.html", $data);
	}

//-------------------------------------------------------------------------------	
	
    public function editarCuota()
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
        
        $cuota = new ModeloCuota();
        //si es alta o modificacion-eliminacion
        if (isset($_GET['idcuota']))
        { 			
            $cuota->putIdCuota($_GET['idcuota']);
            $c_ok = $cuota->traerCuota();
            if (!$c_ok)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n, int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
        }
        //guardo el LOG de re-liquidacion de cuenta
        $log->altaLog("Se modifica la couta Nro: ".$cuota->getIdCuota()." de la cuenta: ".$cuota->getIdCuenta());
        $data = $this->cargarPlantillaModificar($cuota);
        $this->view->show("editarcuota.html", $data);
    }
	
//---------------------------------------------------------------------------------------

    public function altaCuota()
    {
        $alta = new ModeloCuota();
        $this->cargavariables($alta, ALTA);
        $altaok = $alta->altaCuota();
        if (!$altaok)
        {
            $mensaje = htmlentities("No puede realizar esta operaci�n, int�ntelo m�s tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        return;
    }

//---------------------------------------------------------------------------------------

	public function modificarPagoManual()
	{
        $modifica = new ModeloCuota();
		$movimiento = new ModeloMovimiento();
		//levanto los datos de la cuota y de la vista
		$modifica->putIdCuota($_POST['idcuota']);
		$fechacobro = cadenaAFecha($_POST['fechapago']);
		$modifica->traerCuota();
		$cobrado_anterior = $modifica->getCobrado();
		$saldo = $modifica->getSaldo();
		$mora = $modifica->getInteresMora();
		$modifica->putIdUsrMod($_SESSION["s_idusr"]);
		$modifica->putFechaPago($fechacobro);
		$nueva_obs = $modifica->getObservacion()."\n".$_POST['observacion'];
		$modifica->putObservacion($nueva_obs);
		$cobrado = $_POST['montocobrado'];
		$modifica->putCobrado($cobrado + $cobrado_anterior);
		$gastos = $_POST['montogastos'];
		$modifica->putConceptos($gastos);
		//montos del registro de movimiento
		$total_mov = $cobrado;
		$mov_mora = 0;
		$mov_saldo = 0;
		$mov_gastos = $gastos;
		//resto lo cobrado primero al interes por mora
		if($cobrado >= $mora)
		{
			$modifica->putInteresMora(0);
			$cobrado -= $mora;
			$mov_mora = $mora;
		}else{
			$modifica->putInteresMora($mora - $cobrado);
			$mov_mora = $cobrado;
			$cobrado = 0;
		}
		//resto lo cobrado restante al saldo
		if($cobrado >= $saldo)
		{
			$modifica->putSaldo(0);
			$mov_saldo = $saldo;
			$cobrado -= $saldo;
		}else{
			$modifica->putSaldo($saldo - $cobrado);
			$mov_saldo = $cobrado;
			$cobrado = 0;
		}
        $modificado = $modifica->guardarCobro();
        if (!$modificado)
        {
            $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
		//registro de movimiento
		$movimiento->putCobrado($total_mov);
		$movimiento->putFecha($fechacobro);
		$movimiento->putIdCuota($_POST['idcuota']);
		$movimiento->putIdRegmov(MANUAL);
		$movimiento->putIdTipomov(PARCIAL);
		$movimiento->putMontoGastos(0);
		$movimiento->putMontoMora($mov_mora);
		$movimiento->putMontoSaldo($mov_saldo);
		$movimiento->putMontoGastos($mov_gastos);
		$movimiento->putIdArchivo("NULL");
		$mov_ok = $movimiento->registrarmovimiento();
        if (!$mov_ok)
        {
            $mensaje = htmlentities("No se pudo registrar el movimiento, corrobore los datos de la cuota");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        $vista = new view();
        $data['controlador'] = "cobro";
        $data['accion'] = "cuentapagomanual&&idcuenta=".$modifica->getIdCuenta();
        $vista->show1("bridgecustom.html",$data);
	}

//---------------------------------------------------------------------------------------
	
	public function modificarCuota()
	{
        $modifica = new ModeloCuota();
        $this->cargavariables($modifica,MODIFICAR);
        $modificado = $modifica->modificarMora();
        if (!$modificado)
        {
            $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        $vista = new view();
        $data['controlador'] = "cuota";
        $data['accion'] = "vercuotas&&idcuenta=".$modifica->getIdCuenta();
        $vista->show1("bridgecustom.html",$data);
	}

//-----------------------------------------------------------------------------------

    public function detalleCuotasMora()
    //retorna los datos de una cuenta y sus cuotas
	{
		$cuotas = new ModeloCuota();
	    if(isset($_GET['idcuenta']))
	    {
			$cuotas->putIdCuenta($_GET['idcuenta']);
			$liztado = $cuotas->listadoTotal();
            if(!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n, int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
            if (isset($_GET['cuotainicial']) || isset($_GET['cuotafinal']))
            {
            	$arrMora = $cuotas->calcularMoraRango($_GET['cuotainicial'], $_GET['cuotafinal'], $_GET['idcuenta']);
                if(!$arrMora)
                    return false;
                $data['cuotainicial'] = $_GET['cuotainicial'];
                $data['cuotafinal'] = $_GET['cuotafinal'];
            }
        }
        $data['arraymora'] = $arrMora;
        $idcuenta = $_GET['idcuenta'];
        $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
        $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
        $saldo = $deuda_vencida + $deuda_no_vencida;
        $data['liztado'] = $liztado;
        $data['idcuenta'] = $idcuenta;
        $data['solicitante'] = $cuotas->getSolicitante();
        $data['anioexpediente'] = $cuotas->getAnioExpediente();
        $data['letraexpediente'] = $cuotas->getLetraExpediente();
        $data['nroexpediente'] = $cuotas->getNroExpediente();
        $data['nrocuenta'] = $cuotas->getNroCuenta();
        $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
        $data['cuentacorriente'] = $cuotas->getCuentaCorriente();
        $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
        $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
        $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
        $data['saldo'] = number_format($saldo,2,',','.');
        $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
        $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
        if(isset($_GET['fechamora']))
            $data['fechamora'] = $_GET['fechamora'];
        else
            $data['fechamora'] = date('d/m/Y');

        $this->view->show1("detallecuotasmora.html", $data);
    }

//-----------------------------------------------------------------------------------

	public function verCuotas()
	//retorna los datos de las cuotas de un pago generado
	{
        $cuotas = new ModeloCuota();
        if(isset($_GET['idcuenta']))
        {
            //pongo la cuenta de las cuotas
            $cuotas->putIdCuenta($_GET['idcuenta']);
            $liztado = $cuotas->listadoTotal();
            if (!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
        }
        $idcuenta = $_GET['idcuenta'];
        $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
        $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
        $saldo = $deuda_vencida + $deuda_no_vencida;
        $data['liztado'] = $liztado;
        $data['idcuenta'] = $idcuenta;
        $data['solicitante'] = $cuotas->getSolicitante();
        $data['anioexpediente'] = $cuotas->getAnioExpediente();
        $data['letraexpediente'] = $cuotas->getLetraExpediente();
        $data['nroexpediente'] = $cuotas->getNroExpediente();
        $data['nrocuenta'] = $cuotas->getNroCuenta();
        $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
        $data['cuentacorriente'] = $cuotas->getCuentaCorriente();
        $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
        $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
        $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
        $data['saldo'] = number_format($saldo,2,',','.');
        $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
        $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');

        $this->view->show1("cuotascuentas.html", $data);
	}

//============================================================================================================================	
	
	public function cargarPlantillaModificar($parCuota) 
	{
		if(isset($_GET['operacion']))
			$quehacer=$_GET['operacion'];
		else
			$quehacer=MODIFICAR;
          
		switch($quehacer)
		{
			case MODIFICAR:
				$nombreboton="Guardar cambios";
				$nombreaccion="modificarcuota";
				break;
			default:  
				$nombreboton="";
				$nombreaccion="";  
		}

        switch ($quehacer)
		{
			case MODIFICAR:
				$parametros = array("TITULO"=>"Editar",
							"IDCUOTA"=>$parCuota->getIdCuota(),
                            "NROCUOTA"=>$parCuota->getNroCuota(),
							"IDCUENTA"=>$parCuota->getIdCuenta(),
                            "NROCUENTA"=>$parCuota->getNroCuenta(),
							"SOLICITANTE" =>$parCuota->getSolicitante(),
							"NROCUOTA" =>$parCuota->getNroCuota(), 
							"MONTOCUOTA" =>number_format($parCuota->getMontoCuota(),2,',','.'),
							"SALDO"=>number_format($parCuota->getSaldo(),2,',','.'),
							"FECHAVENC"=>fechaACadena($parCuota->getFechaVenc()), 
							"FECHAPAGO"=>fechaACadena($parCuota->getFechaPago()), 
		                    "INTERES"=>number_format($parCuota->getInteres(),2,',','.'),
							"INTERESMORA"=>number_format($parCuota->getInteresMora(),2,',','.'),
							"FECHACALCULOMORA"=>fechaACadena($parCuota->getFechaCalculoMora()),
							"MONTOCOBRADO"=>number_format($parCuota->getCobrado(),2,',','.'),
							"OBSERVACION"=>$parCuota->getObservacion(),
                            "nombreaccion"=>$nombreaccion,
							"nombreboton"=>$nombreboton
							);
							break;
			default:
				$parametros = array("TITULO"=>"Consultar",
							"IDCUOTA"=>$parCuota->getIdCuota(),
                            "NROCUOTA"=>$parCuota->getNroCuota(),
							"IDCUENTA"=>$parCuota->getIdCuenta(),
                            "NROCUENTA"=>$parCuota->getNroCuenta(),
							"SOLICITANTE" =>$parCuota->getSolicitante(),
							"NROCUOTA" =>$parCuota->getNroCuota(),
							"MONTOCUOTA" =>number_format($parCuota->getMontoCuota(),2,',','.'),
							"SALDO"=>number_format($parCuota->getSaldo(),2,',','.'),
							"FECHAVENC"=>fechaACadena($parCuota->getFechaVenc()),
							"FECHAPAGO"=>fechaACadena($parCuota->getFechaPago()),
		                    "INTERES"=>number_format($parCuota->getInteres(),2,',','.'),
							"INTERESMORA"=>number_format($parCuota->getInteresMora(),2,',','.'),
							"FECHACALCULOMORA"=>fechaACadena($parCuota->getFechaCalculoMora()),
							"MONTOCOBRADO"=>number_format($parCuota->getCobrado(),2,',','.'),
							"OBSERVACION"=>$parCuota->getObservacion(),
							"nombreboton"=>$nombreboton
		                    );
		}
		return $parametros;
	}

//----------------------------------------------------------------------------------

    public function cargavariables($clasecarga,$oper)
    //Carga las variables del html para volcarlas en la tabla
    {
        if ($oper==MODIFICAR)
            $clasecarga->putIdCuota($_POST["idcuota"]);		
            $clasecarga->putIdCuenta($_POST["idcuenta"]);
            $clasecarga->putNroCuota($_POST["nrocuota"]);
            $clasecarga->putMontoCuota($_POST["montocuota"]);
            $clasecarga->putSaldo($_POST["saldo"]);
            $clasecarga->putFechaVenc(cadenaAFecha($_POST["fechavenc"]));
            $clasecarga->putFechaPago(cadenaAFecha($_POST["fechapago"]));
            $clasecarga->putCobrado(redondeoCincoCent($_POST["montocobrado"]));		//valor redondeado
            $clasecarga->putInteres($_POST["interes"]);
            $clasecarga->putIdUsrCreador($_SESSION["s_idusr"]);
            $clasecarga->putIdUsrMod($_SESSION["s_idusr"]);
            $clasecarga->putInteresMora(redondeoCincoCent($_POST["interesmora"]));	//valor redondeado 
            $clasecarga->putFechaCalculoMora(cadenaAFecha($_POST["fechacalculomora"]));
            $nuevaobservacion = $_POST["observacionant"]."\n".$_POST["observacion"];
            $clasecarga->putObservacion($nuevaobservacion);
    }	

//=======================================================================================
   
	public function sumaDia($fecha,$dia)
	// suma dia a la fecha
	{
		// suma $dia dias a una fecha y retorna la fecha resultante
    	list($day,$mon,$year) = explode('/',$fecha);
		$day=($day*1)+$dia;
		$mon=$mon*1;
		$year=$year*1;
		
		$nuevafecha=date('d/m/Y',mktime(0,0,0,$mon,$day,$year));
		return($nuevafecha);
			
	}

//----------------------------------------------------------------------------------------

    public function reimprimirChequera()
    //
	{require_once 'modelos/modelocuenta.php';

	    $cuenta=new modelocuenta();
        $cuotas = new ModeloCuota();
        if(isset($_GET['idcuenta']))
        {
            //pongo la cuenta de las cuotas
            $cuotas->putIdCuenta($_GET['idcuenta']);
            $cuenta->putIdCuenta($_GET['idcuenta']);
            $cuenta->traercuenta();
            $liztado = $cuotas->listadoTotal();
            if (!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
            $idcuenta = $_GET['idcuenta'];
            $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
            $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
            $saldo = $deuda_vencida + $deuda_no_vencida;
            $data['liztado'] = $liztado;
            $data['idcuenta'] = $idcuenta;
			$data['idsolicitud'] = $cuenta->getIdSolicitud();
            $data['idcuota'] = $_GET['idcuota'];
            $data['solicitante'] = $cuotas->getSolicitante();
            $data['anioexpediente'] = $cuotas->getAnioExpediente();
            $data['letraexpediente'] = $cuotas->getLetraExpediente();
            $data['nroexpediente'] = $cuotas->getNroExpediente();
            $data['nrocuenta'] = $cuotas->getNroCuenta();
            $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
            $data['cuentacorriente'] = $cuotas->getCuentaCorriente();
            $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
            $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
            $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
            $data['saldo'] = number_format($saldo,2,',','.');
            $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
            $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
            if(isset($_GET['fechamora']))
                $data['fechamora'] = $_GET['fechamora'];
            else
                $data['fechamora'] = date('d/m/Y');
        }
        $this->view->show1("reimprimirchequera.html", $data);
    }

//---------------------------------------------------------------------------------

	public function IngresarTalon()
	// muestra todas las cuentas en un html con una tabla para imprimir talones de pagos
	{
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("ingresartalon.html", $data);
 	}


//--------------------------------------------------------------------------------------

	public function pagoParcial()
	//
	
	
	{require_once 'modelos/modelocuenta.php';

	    $cuenta=new modelocuenta();
        $cuotas = new ModeloCuota();
		$arraypagoparcial=array();
		$interesmora=0;
        if(isset($_GET['idcuenta']))
        {
            //pongo la cuenta de las cuotas
            $cuotas->putIdCuenta($_GET['idcuenta']);
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traercuenta();
            $liztado = $cuotas->listadoTotal();
            if (!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
			if (isset($_GET['idcuota'])) {
			
		    $arraypagoparcial=$this->datoscuota($_GET['idcuota']);
			$arrMora=$cuotas->calcularMoraRango($_GET['nrocuota'], $_GET['nrocuota'], $_GET['idcuenta']);
			foreach($arrMora as $intmora ){
			  $interesmora=number_format($intmora['intmora']+$intmora['moraanterior'],2,'.','');
			  $saldocuota=number_format($intmora['saldo'],2,'.','');
			  $deudacuota=number_format($intmora['deudamora'],2,'.','');
			  $capital=number_format($intmora['capital'],2,'.','');
			  $interes=number_format($intmora['interes'],2,'.','');
			  }

 		    }
            $idcuenta = $_GET['idcuenta'];
            $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
            $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
            $saldo = $deuda_vencida + $deuda_no_vencida;
            $data['liztado'] = $liztado;
			$data['saldocuota'] = $saldocuota;
			$data['deudacuota'] = $deudacuota;
			$data['intmora'] = $interesmora;
			$data['capital'] = $capital;
			$data['interes'] = $interes;
			$data['arraypagoparcial'] =  $arraypagoparcial;
            $data['idcuenta'] = $idcuenta;
			$data['idsolicitud'] = $cuenta->getIdSolicitud();
            $data['idcuota'] = $_GET['idcuota'];
            $data['solicitante'] = $cuotas->getSolicitante();
            $data['anioexpediente'] = $cuotas->getAnioExpediente();
            $data['letraexpediente'] = $cuotas->getLetraExpediente();
            $data['nroexpediente'] = $cuotas->getNroExpediente();
            $data['nrocuenta'] = $cuotas->getNroCuenta();
            $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
            $data['cuentacorriente'] = $cuotas->getCuentaCorriente();
            $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['cuentadeposito'] = $cuenta->getCuentaCorriente();
            $data['convenio'] =$cuenta->getConvenio();
            $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
            $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
            $data['saldo'] = number_format($saldo,2,',','.');
            $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
            $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
           if(isset($_GET['fechapago']))
	      	$data['fechapago']=$_GET['fechapago'];
		   else
    	    $data['fechapago']=date('d/m/Y');
	
        }
		$this->view->show1("pagoparcial.html", $data);
	}		


//--------------------------------------------------------------------------------------

	public function pagoGlobal()
	//
	{
		require_once 'modelos/modelocuenta.php';
		$cuenta=new modelocuenta();
        $cuotas = new ModeloCuota();
		$arraypagoparcial=array();
		$interesmora=0;
        if(isset($_GET['idcuenta']))
        {
            //pongo la cuenta de las cuotas
            $cuotas->putIdCuenta($_GET['idcuenta']);
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traercuenta();
            $liztado = $cuotas->listadoTotal();
            if (!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
			 if (isset($_GET['cuotainicial']) || isset($_GET['cuotafinal']))
            {
            	$arrMora = $cuotas->calcularMoraRango($_GET['cuotainicial'], $_GET['cuotafinal'], $_GET['idcuenta']);
                if(!$arrMora)
                    {
                $mensaje = htmlentities("No puede realizar esta operaci�n verifique el rango de cuotas ingresado");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
                $data['cuotainicial'] = $_GET['cuotainicial'];
                $data['cuotafinal'] = $_GET['cuotafinal'];
            }
  
            $idcuenta = $_GET['idcuenta'];
            $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
            $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
            $saldo = $deuda_vencida + $deuda_no_vencida;
            $data['liztado'] = $liztado;
			$data['arraypagoglobal'] =  $arrMora;
            $data['idcuenta'] = $idcuenta;
			$data['idsolicitud'] = $cuenta->getIdSolicitud();
            $data['idcuota'] = $_GET['idcuota'];
            $data['solicitante'] = $cuotas->getSolicitante();
            $data['anioexpediente'] = $cuotas->getAnioExpediente();
            $data['letraexpediente'] = $cuotas->getLetraExpediente();
            $data['nroexpediente'] = $cuotas->getNroExpediente();
            $data['nrocuenta'] = $cuotas->getNroCuenta();
            $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
            $data['cuentacorriente'] = $cuotas->getCuentaCorriente();
            $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['cuentadeposito'] = $cuenta->getCuentaCorriente();
            $data['convenio'] =$cuenta->getConvenio();
            $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
            $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
            $data['saldo'] = number_format($saldo,2,',','.');
            $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
            $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
           if(isset($_GET['fechapago']))
	      	$data['fechapago']=$_GET['fechapago'];
		   else
    	    $data['fechapago']=date('d/m/Y');
	
        }
		$this->view->show1("pagoglobal.html", $data);
	}		

//---------------------------------------------------------------------------------------
	
    public function datoscuota($parCuota)
    {
        $cuota = new ModeloCuota();
        $cuota->putIdCuota($parCuota);
        $existe = $cuota->traercuota();
        if($existe)
        {
            $fechadeposito = date('d/m/Y');
            $interesxmora = $cuota->getInteresMora();

            if(isset($_GET['fechapago']))
                $fechadeposito = $_GET['fechapago']; // es la fecha en que el emprendedor piensa hacer el pago de la cuota
                $arrcuotas[] = array("id"=>$cuota->getIdCuota(),
                                        "nrocuota"=>$cuota->getNroCuota(),
                                        "montoparcial"=>$cuota->getMontoCuota(),
                                        "saldo"=>$cuota->getSaldo(),
                                        "fechavencimiento"=>fechaACadena($cuota->getFechaVenc()),
                                        "fechapago"=>$fechadeposito,
                                        "intmora"=>$interesxmora,
                                        "diasmora"=>0
                                        );
        }					 
        return($arrcuotas);
    }	

//---------------------------------------------------------------------------------------

    public function descontarCobroCuota()
    {
        $cuota = new ModeloCuota();
        $movimiento = new ModeloMovimiento();
        //levanto los datos de la cuota y de la vista
        $cuota->putIdCuota($_POST['idcuota']);
        $cuota->traerCuota();
        $fecharesta = cadenaAFecha($_POST['fecharesta']);
        $descuento = redondeoCincoCent($_POST['montoresta']);	//cobrado redondeado a 1 decimal
        $cuota->putIdUsrMod($_SESSION["s_idusr"]);
        $cobrado_anterior = $cuota->getCobrado();
        $saldo = $cuota->getSaldo();
        $mora = $cuota->getInteresMora();
        //levanto la informacion de los movimientos de la cuota
        $movimiento->putIdCuota($_POST['idcuota']);
        $arr_cobros = $movimiento->sumaMovimientosCuota();
        if(!$arr_cobros)
        {
            $mensaje = htmlentities("Error al calcular los montos de movimientos, int�ntelo m�s tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
		
        $cobrado_real = $arr_cobros['cobradosaldo'] + $arr_cobros['cobradomora'];
        //no permito descontar a menos de lo cobrado real, segun los movimientos
        if(($cobrado_real - ($cobrado_anterior - $descuento)) > CERO_DECIMAL)
        {
           $mensaje = htmlentities("El descuento afecta al saldo o al inter�s cobrado a la cuota. No puede descontar ese monto");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        //montos del movimiento de descuento
        $total_mov = $descuento*(-1);
        $cuota->putCobrado($cobrado_anterior - $descuento);
        $nueva_obs = $cuota->getObservacion()."\n".$_POST['observacion'];
        $cuota->putObservacion($nueva_obs);
        $modificado = $cuota->guardarCobro();
        if (!$modificado)
        {
            $mensaje = htmlentities("No se pudo registrar el cobro, int�ntelo m�s tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        //registro de movimiento
        $movimiento->putCobrado($total_mov);
        $movimiento->putFecha($fecharesta);
        $movimiento->putIdCuota($_POST['idcuota']);
        $movimiento->putIdRegmov(MANUAL);
        $movimiento->putIdTipomov(DESCUENTO);
        $movimiento->putMontoGastos(0);
        $movimiento->putMontoMora(0);
        $movimiento->putMontoSaldo(0);
        $movimiento->putIdArchivo("NULL");
        $mov_ok = $movimiento->registrarmovimiento();
        if (!$mov_ok)
        {
            $mensaje = htmlentities("No se pudo registrar el movimiento, corrobore los datos de la cuota");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        
        $vista = new view();
        $data['controlador'] = "cobro";
        $data['accion'] = "cuentarestarcobro&&idcuenta=".$cuota->getIdCuenta();
        $vista->show1("bridgecustom.html",$data);
    }
}

?>
