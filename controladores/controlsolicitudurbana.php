<?php
require_once 'modelos/modelosolicitudurbana.php';
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelodestinotierra.php';
require_once 'modelos/modelotierraurbana.php';
require_once 'controladores/controltierraurbana.php';

class ControlSolicitudUrbana
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrarsolicitudurbana()
	// muestra todas las solicitudurbana en un html con una tabla
	{
		$solicitudurbana = new modelosolicitudurbana();
		$liztado = $solicitudurbana->listadoTotal();
		$data['liztado'] = $liztado;
		die;
		//$this->view->show1("solicitudurbana.html", $data);
 	}

//---------------------------------------------------------------------------------------
	
	public function altasolicitudurbana()
	{
		$alta= new modelosolicitudurbana();
		$clasetierraurbana = new controltierraurbana();
	    $altatiurbana=$clasetierraurbana->altatierraurbana($tierraurbana);
		
	    if($altatiurbana>0){ // si se pudo dar de alta tierra urbana
		
		$this->cargavariables($alta, ALTA);
		$alta->putIdTierraUrbana($altatiurbana);

		$altaok = $alta->altasolicitudurbana();
		if (!$altaok)
		{
			$mensaje = htmlentities("No se pudo dar de alta la solicitud, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		else{
		    $nuevoid=$alta->getIdSolicitud();
	        $data['controlador']="solicitudurbana";
		    $data['accion']="vertabsolicitudurbana&&idsol=".$nuevoid;
		    $this->view->show1("bridgecustom.html",$data);	
		 }	
		
		}
		else{
		
             $mensaje = htmlentities("No se pudo dar de alta la tierra urbana");
			 $data['mensaje'] = $mensaje;
			 $this->view->show1("mostrarerror.html", $data);
			 return;

		
		}
		
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificarsolicitudurbana()

	{
	    $tierraurbana=new modelotierraurbana();
		$modifica= new modelosolicitudurbana();
		$this->cargavariables($modifica,MODIFICAR);
		
        $modificado=$modifica->modificarsolicitudurbana();


	    $tierraurbana->putIdTierraUrbana($modifica->getIdTierraUrbana());
	    $clasetierraurbana = new controltierraurbana();
	    $tierraurbana=$clasetierraurbana->modificartierraurbana($tierraurbana);
		
		  
		
		
	   if (!$modificado){
	      $mensaje = htmlentities("No se pudo modificar la solicitud, int�ntelo m�s tarde");
	      $data['mensaje']=$mensaje;
    	  $this->view->show1("mostrarerror.html", $data);
		  return;
        }
	    $this->vertabsolicitudurbana();
			
	}
				
//---------------------------------------------------------------------------------------
	
	public function borrarsolicitudurbana()
	{
		$borra= new modelosolicitudurbana();
		$borra->putIdSolicitudUrbana($_POST['id']);
		$borrado=$borra->borrarsolicitudurbana();
		if (!$borrado)
		{
			$mensaje = htmlentities("No se pudo eliminar la solicitud, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarsolicitudurbana();		 
	}



//-----------------------------------------------------------------------------------

//retorna los datos de una solicitudurbana si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function versolicitudurbana()
	{
		
	$solicitudurbana = new modelosolicitudurbana();
    if (isset($_GET['idsol'])) { 
	
        $solicitudurbana->putIdSolicitudUrbana($_GET['idsol']);
	
       	$empent = $solicitudurbana->traersolicitudurbana();
	
     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede consultar la solicitud urbana, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$solicitud=new modelosolicitud;
	$destinotierra=new modelodestinotierra;
	
	$data=$this->cargarPlantillaModificar($solicitudurbana,$solicitud,$destinotierra);
	  $this->view->show("abmsolicitudurbana.html", $data);
	
	}	
//-----------------------------------------------------------------------------------


//retorna los datos de una solicitudurbana si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertabsolicitudurbana()
	{
	$tierraurbana = new modelotierraurbana();	
		
	$solicitudurbana = new modelosolicitudurbana();
	if (isset($_GET['idsol']) || (isset($_POST['idsolicitud'])) ) {

	   if (isset($_GET['idsol'])){
           $solicitudurbana->putIdSolicitud($_GET['idsol']);
	   	   $solicitudurbana->putIdPoblador($_GET['idpob']);
		}
       if (isset($_POST['idsolicitud'])){  
	        $solicitudurbana->putIdSolicitud($_POST['idsolicitud']);
		}
		
		$empent = $solicitudurbana->traersolicitudurbanaasociada();
		       $tierraurbana->putIdSolicitudUrbana($solicitudurbana->getIdSolicitudUrbana());
	           $clasetierraurbana = new controltierraurbana();
	           $parTierraUrbana=$clasetierraurbana->vertierraurbanaasociada($tierraurbana);
	            if (isset($_GET['idsol'])){
                   $solicitudurbana->putIdSolicitud($_GET['idsol']);
	   	            $solicitudurbana->putIdPoblador($_GET['idpob']);
		}
       if (isset($_POST['idsolicitud'])){  
	        $solicitudurbana->putIdSolicitud($_POST['idsolicitud']);
		}

     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede consultar la solicitud urbana, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$solicitud=new modelosolicitud;
	$destinotierra=new modelodestinotierra;
	
	$data=$this->cargarPlantillaModificar($solicitudurbana,$solicitud,$destinotierra);
	$nuevoarray=$data;
	if(is_array( $parTierraUrbana)){
     	$nuevoarray=array_merge($data,$parTierraUrbana);
	}
	  $this->view->show("tabsolicitudurbana.html", $nuevoarray);
	
	}	
		
//-----------------------------------------------------------------------------------
	 public function cargarPlantillaModificar($parSolicitudUrbana,$parSolicitud,$parDestinoTierra) 
{  
   
	
	$vdestino= $parDestinoTierra->TraerTodos();
	$vdestino['selected']=  $parSolicitudUrbana->getIdDestinoTierra();
	
	$idsolicitudurbana =  $parSolicitudUrbana->getIdSolicitudUrbana();
	$quehacer = "";
	if ($idsolicitudurbana== 0)
			$quehacer = ALTA;
	else
		if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
		  
	switch($quehacer)
	{
      case ALTA:
		
		$parSolicitudUrbana->putIdSolicitudUrbana("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altasolicitudurbana";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificarsolicitudurbana";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrarsolicitudurbana";  
      break;
      default:  
		    $nombreboton="Guardar";
	        $nombreaccion="modificarsolicitudurbana";
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parSolicitudUrbana->getIdSolicitudUrbana();
		$parametros = array(
                    "TITULOFORM" =>  "Solicitudes Urbanas -> Modificar",
                    "ID" => $parSolicitudUrbana->getIdSolicitud(),
					"IDSOLICITUDURBANA" => $parSolicitudUrbana->getIdSolicitudUrbana(),
					"IDSOLICITUD" => $parSolicitudUrbana->getIdSolicitud(),
					"TIERRASDECLARADAS" =>$parSolicitudUrbana->getTierrasDeclaradas(), 
					"ACTIVIDADLABORAL" =>$parSolicitudUrbana->getActividadLaboral(),
    				"USR_MOD"=>$parSolicitudUrbana->getUsrMod(),
					"TIENECONCESION"=>$parSolicitudUrbana->getTieneConcesion(),
					"DETALLECONCESION"=>$parSolicitudUrbana->getDetalleConcesion(),
					"IDDESTINOTIERRA"=>$parSolicitudUrbana->getIdDestinoTierra(),
					"LISTADESTINO"=>$vdestino,
					"NOVER"=>"style='visibility:hidden'",
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                  
                    );
        break;
		case BAJA:
        $idsoli=$parSolicitudUrbana->getIdSolicitudUrbana();
	    $parametros = array(
                   "TITULOFORM" =>  "Solicitudes Urbanas -> Eliminar",
                    "ID" => $parSolicitudUrbana->getIdSolicitud(),
					"IDSOLICITUDURBANA" => $parSolicitudUrbana->getIdSolicitudUrbana(),
					"IDSOLICITUD" => $parSolicitudUrbana->getIdSolicitud(),
					"TIERRASDECLARADAS" =>$parSolicitudUrbana->getTierrasDeclaradas(), 
					"ACTIVIDADLABORAL" =>$parSolicitudUrbana->getActividadLaboral(),
    				"USR_MOD"=>$parSolicitudUrbana->getUsrMod(),
					"TIENECONCESION"=>$parSolicitudUrbana->getTieneConcesion(),
					"DETALLECONCESION"=>$parSolicitudUrbana->getDetalleConcesion(),
					"IDDESTINOTIERRA"=>$parSolicitudUrbana->getIdDestinoTierra(),
					"LISTADESTINO"=>$vdestino,
					"NOVER"=>"style='visibility:hidden'",
					"nombreaccion"=>$nombreaccion,
					"DISA_MODI"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                    );
	     break;
		 case ALTA:
		 
 
         
  
	     $parametros = array(
	                "TITULOFORM" =>  "Solicitudes Urbanas -> Alta",
                    "ID" => $parSolicitudUrbana->getIdSolicitud(),
					"IDSOLICITUDURBANA" => $parSolicitudUrbana->getIdSolicitudUrbana(),
					"IDSOLICITUD" =>$parSolicitudUrbana->getIdSolicitud(),
					"TIERRASDECLARADAS" =>"", 
					"ACTIVIDADLABORAL" =>"",
    				"USR_MOD"=>$parSolicitudUrbana->getUsrMod(),
					"TIENECONCESION"=>0,
					"DETALLECONCESION"=>"",
					"IDDESTINOTIERRA"=>0,
					"LISTADESTINO"=>$vdestino,
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
	                "DISA_MODI"=>"",
                    "tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					   );
	
	     break;
		 default :
		 $idsoli=$parSolicitudUrbana->getIdSolicitudUrbana();
		$parametros = array(
                    "TITULOFORM" =>  "Solicitudes Urbanas -> Modificar",
                    "ID" => $parSolicitudUrbana->getIdSolicitud(),
					"IDSOLICITUDURBANA" => $parSolicitudUrbana->getIdSolicitudUrbana(),
					"IDSOLICITUD" => $parSolicitudUrbana->getIdSolicitud(),
					"TIERRASDECLARADAS" =>$parSolicitudUrbana->getTierrasDeclaradas(), 
					"ACTIVIDADLABORAL" =>$parSolicitudUrbana->getActividadLaboral(),
    				"USR_MOD"=>$parSolicitudUrbana->getUsrMod(),
					"TIENECONCESION"=>$parSolicitudUrbana->getTieneConcesion(),
					"DETALLECONCESION"=>$parSolicitudUrbana->getDetalleConcesion(),
					"IDDESTINOTIERRA"=>$parSolicitudUrbana->getIdDestinoTierra(),
					"LISTADESTINO"=>$vdestino,
					"NOVER"=>"style='visibility:hidden'",
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                    );
		 					
		 
		} 				



        return $parametros;
  }

//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdSolicitudUrbana($_POST["idsolicitudurbana"]);
		}
		
    $clasecarga->putIdSolicitud($_POST["idsolicitud"]);
    $clasecarga->putIdUsrCreador($_SESSION["s_idusr"]);
	$clasecarga->putIdUsrMod($_SESSION["s_idusr"]);
	
	$clasecarga->putTierrasDeclaradas($_POST["tierrasdeclaradas"]);
	$clasecarga->putActividadLaboral($_POST["actividadlaboral"]);
	$clasecarga->putTieneConcesion($_POST["tieneconcesion"]);
	$clasecarga->putDetalleConcesion($_POST["detalleconcesion"]);
		if($_POST["iddestinotierra"]==0){
		   $clasecarga->putIdDestinoTierra('NULL');       
		}
		else{
	      $clasecarga->putIdDestinoTierra($_POST["iddestinotierra"]);
		}

   
   }	
	
	


   



}

?>