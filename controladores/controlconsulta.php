<?php
require_once 'modelos/modelotipodoc.php';
require_once 'modelos/modelocuota.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modelocuenta.php';
require_once 'modelos/modeloparametro.php';
require_once 'modelos/modeloarchivocobro.php';
require_once 'modelos/modelomovimiento.php';
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelolog.php';

class ControlConsulta
{
 
 
 	function __construct()
	{

	    $this->view = new View();

	}

// =================================================================================

	public function concomarcas()
	{
		$localidad = new ModeloLocalidad();
		$parametros = array("LISTAZONASDISPONIBLES"=>$localidad->TraerTodos(),
							"LISTAZONASSELEGIDAS"=>array(),
							"FECHADESDE"=>'01/01/1999',
							"FECHAHASTA"=>date('d/m/Y')
						);
		$this->view->show("concomarcas.html", $parametros);
	}

//=================================================================================

	public function consultarcomarcas()
	{
		//fechas por defecto
		$fechadesde = '01/01/1999';
		$fechahasta = date('d/m/Y');
		//si recibo las fechas las asigno
		if(!empty($_POST["fechadesde"]))
			$fechadesde = $_POST["fechadesde"];
		if(!empty($_POST["fechahasta"]))
			$fechahasta = $_POST["fechahasta"];
		//armo la condicion de fechas de la cuenta
		$condicion_fecha = "fechacuenta>'".cadenaAFecha($fechadesde)."' && fechacuenta<'".cadenaAFecha($fechahasta)."'";
		$cuota = new ModeloCuota();
		$localidad = new ModeloLocalidad();
		$cuentas = new ModeloCuenta();

		if(!isset($_POST["vwZonasElegidas"]))
		{
			$_POST["vwZonasElegidas"] = array();
		}
		$nuevacondicion = "";
		$cuentas->putElegidas($_POST["vwZonasElegidas"]);
		$condicion = $cuentas->ObtenerZonas();
		if(!empty($condicion) && !empty($condicion_fecha))
		{
			$nuevacondicion = "(".$condicion.") && (".$condicion_fecha.")";
		}else{
			if(!empty($condicion))
				$nuevacondicion = $condicion;
			else
				$nuevacondicion = $condicion_fecha;
		}
		$resultado = $cuentas->traertotcuentaZonas($nuevacondicion);
		if(!$resultado)
		{
			$Elegidas=$localidad->TraerElegidas($_POST["vwZonasElegidas"]);
			$data['Elegidas'] = $Elegidas;
			$this->view->show1("consultacomarcas.html", $data);
		}else{
			$comarcas = new modelolocalidad();
 			while ($varc = mysql_fetch_object($resultado))
			{
				if ($varc->tiposolicitud == RURAL)
					$tipo = "RURAL";
				else
					$tipo = "URBANA";

				$idcuenta = $varc->id;
				$cuota->putIdCuenta($idcuenta);
				//obtengo las cuotas adeudadas y vencidas de la cuenta
				$lista_cuotas = $cuota->listadoCuotasCuentaDeuda("");
				if(count($lista_cuotas) > 0)
				{
					$saldo_total=$cobrado_total=$int_mora_total=0;
					foreach ($lista_cuotas as $c)
					{
						$cuota->putIdCuota($c['id']);
						$cuota->traerCuota();
						$saldo_total += $cuota->getSaldo()*1;
						$cobrado_total += $cuota->getCobrado()*1;
						$int_mora_total += $cuota->getInteresMora()*1;
					}
					$data_cuenta[$i]['localidad'] = $varc->zona;
					$data_cuenta[$i]['saldototal'] = $saldo_total;
					$data_cuenta[$i]['cobradototal'] = $cobrado_total;
					$data_cuenta[$i]['moratotal'] = $int_mora_total;
					$data_cuenta[$i]['nrocuenta'] = $varc->nrocuenta;
					$data_cuenta[$i]['valorliquidacion'] = $varc->valorliquidacion;
					$data_cuenta[$i]['titular'] = $varc->apellido.", ".$varc->nombres;
					$data_cuenta[$i]['tipo'] = $tipo;
					$data_cuenta[$i]['fechacuenta'] = $varc->fechacuenta;
					$i++;
				}
			}
			$Elegidas=$localidad->TraerElegidas($_POST["vwZonasElegidas"]);
			$data['arrcuentas'] = $arrcuentas;
			$data['Elegidas']= $Elegidas;
			$data['cuentas'] = $data_cuenta;
			$data['fechadesde'] = $fechadesde;
			$data['fechahasta'] = $fechahasta;
			$this->view->show1("consultacomarcas.html", $data);
		}
	}

//---------------------------------------------------------------------------------

	public function consultaDeudores()
	//
	{
		$this->view->show1("consultadeudores.html", $data);
 	}

//=======================================================================================================================

	public function listarDeudores()
	//genera un listado con los deudores entre las fechas $desde y $hasta calculando las deudas a la fecha
	{
		$cuenta = new ModeloCuenta();
		$cuota = new ModeloCuota();
		$i=0;
		//tomo por defecto el corriente a�o
		$desde = date('01/01/Y');
		$hasta = date('31/12/Y');
		if($_POST['fechadesde'] != "")
			$desde = $_POST['fechadesde'];
		if($_POST['fechahasta'] != "")
			$hasta = $_POST['fechahasta'];
		$condicion = "&& fechavencimiento>'".cadenaAFecha($desde)."' && fechavencimiento<'".cadenaAFecha($hasta)."'";
		$lista_cuentas = $cuenta->listadoTotal();
		if(count($lista_cuentas) <= 0)
		{
			$mensaje = htmlentities("No hay cuentas para listar.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return false;
		}
		foreach ($lista_cuentas as $varc)
		{
			$idcuenta = $varc['id'];
			$cuota->putIdCuenta($idcuenta);
			//obtengo las cuotas adeudadas y vencidas de la cuenta
			$lista_cuotas = $cuota->listadoCuotasCuentaDeuda($condicion);
			if(count($lista_cuotas) > 0)
			{
				$saldo_total=$cobrado_total=$int_mora_total=$int_mora_fecha=0;
				foreach ($lista_cuotas as $c)
				{
					$cuota->putIdCuota($c['id']);
					$cuota->traerCuota();
					$saldo_total += $cuota->getSaldo()*1;
					$cobrado_total += $cuota->getCobrado()*1;
					$int_mora_total += $cuota->getInteresMora()*1;
				}
				//obtengo los datos de la proxima cuota a pagar
				$prox_cuota = $cuota->proximaCuotaVencer();
				$nro_prox_cuota = $prox_cuota['nrocuota'];
				$venc_prox_cuota = $prox_cuota['fechavenc'];
				//obtengo la mora total a la fecha indicada
				$_GET['fechamora'] = $hasta;
				$arr_mora = $cuota->calcularMoraRango($lista_cuotas[0]['nrocuota'], $lista_cuotas[count($lista_cuotas)-1]['nrocuota'], $idcuenta);
				if(!$arr_mora)
				{
					$mensaje = htmlentities("Error en el c�lculo de intereses, corrobore los datos");
					$data['mensaje'] = $mensaje;
					$this->view->show1("mostrarerror.html", $data);
					return false;
				}
				foreach ($arr_mora as $c)
				{
					$int_mora_fecha += ($c['intmora'] + $c['moraanterior']);
				}
				if(($saldo_total>0) || ($int_mora_total>0))
				{
					//genero el arreglo con todos los datos
					$data_cuenta[$i]['saldototal'] = $saldo_total;
					$data_cuenta[$i]['cobradototal'] = $cobrado_total;
					$data_cuenta[$i]['moratotal'] = $int_mora_total;
					$data_cuenta[$i]['morafecha'] = $int_mora_fecha;
					$data_cuenta[$i]['nroproxcuota'] = $nro_prox_cuota;
					$data_cuenta[$i]['vencproxcuota'] = $venc_prox_cuota;
					$data_cuenta[$i]['nrocuenta'] = $varc['nrocuenta'];
					$data_cuenta[$i]['valorliquidacion'] = $varc['valorliquidacion'];
					$data_cuenta[$i]['titular'] = $varc['solicitante'];
					$data_cuenta[$i]['tipo'] = $varc['tipo'];
					$i++;
				}
			}
		}
		$data['cuentas'] = $data_cuenta;
		$data['fechadesde'] = $desde;
		$data['fechahasta'] = $hasta;

		$this->view->show1("deudores.html", $data);
	}

//---------------------------------------------------------------------------------

	public function mostrarCuenta()
	// muestra todas las cuentas en un html con una tabla
	{
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("listaestadocuenta.html", $data);
 	}

//-----------------------------------------------------------------------------------

    public function estadoCuenta()
    //
    {
        $cuotas = new ModeloCuota();
        $mov = new ModeloMovimiento();
        $sol = new ModeloSolicitud();
        $cuenta = new ModeloCuenta();
        $loc = new ModeloLocalidad();                
        
        if(isset($_GET['idcuenta']))
        {
            $idcuenta = $_GET['idcuenta'];
            //pongo la cuenta de las cuotas
            $cuotas->putIdCuenta($idcuenta);
            $liztado = $cuotas->listadoTotal();
            if (!$liztado)
            {
                $mensaje = htmlentities("No puede realizar esta operaci�n int�ntelo m�s tarde");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }            
            //obtengo el nro de la primer cuota del plan total (formalizacion + plan cuotas)
            $nroPrimerCuota = $liztado[0]['nrocuota'];            
            //$cant_cuotas = count($liztado)-1;
            $cant_cuotas = ($nroPrimerCuota==0)?count($liztado)-1:count($liztado);            
            //$arr_mora = $cuotas->calcularMoraRango(0, $cant_cuotas ,$idcuenta);
            $arr_mora = $cuotas->calcularMoraRango($nroPrimerCuota, $cant_cuotas ,$idcuenta);
            
            if(!$arr_mora)
            {                              
                $mensaje = htmlentities("Error en el calculo de intereses, corrobore los datos");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }            

            foreach($liztado as $c)
            {
                    //calculo la mora y la deuda total acumulada en la DB
                    $total_mora += $c['interesmora'];
                    $total_deuda += $c['interesmora']+$c['saldo'];

                    //calculo los gastos fijos de cada cuota segun los movimientos hechos
                    $mov->putIdCuota($c['id']);
                    $arr_movs = $mov->sumaMovimientosCuota();
                    $total_gastos_fijos += $arr_movs['cobradogastos'];
            }
            $cuenta->putIdCuenta($idcuenta);
            $cuenta->traerCuenta();
            $sol->putIdSolicitud($cuenta->getIdSolicitud());
            $sol->traersolicitud();
            $loc->putIdLocalidad($sol->getIdLocalidad());
            $loc->traerlocalidad();
        }
        $deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
        $deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
        $saldo = $deuda_vencida + $deuda_no_vencida;
        $data['liztado'] = $liztado;
        $data['idcuenta'] = $idcuenta;
        $data['solicitante'] = $cuotas->getSolicitante();
        $data['anioexpediente'] = $cuotas->getAnioExpediente();
        $data['letraexpediente'] = $cuotas->getLetraExpediente();
        $data['nroexpediente'] = $cuotas->getNroExpediente();
        $data['nrocuenta'] = $cuotas->getNroCuenta();
        $data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
        $data['superficie'] = number_format($sol->getSuperficie(),2,',','');
        $data['unidad'] = $sol->getTipoSolicitud()==1?"hect�reas":"metros cuadrados"; //defino la unidad de medida de la tierra
        $data['localidad'] = $loc->getDescripcion();
        $data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
        $data['deudavencida'] = number_format($deuda_vencida,2,',','.');
        $data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
        $data['saldo'] = number_format($saldo,2,',','.');
        $data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
        $data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
        $data['totalgastosfijos'] = number_format($total_gastos_fijos,2,',','.');
        $data['arraymora'] = $arr_mora;
        $data['totalmora'] = $total_mora;
        $data['totaldeuda'] = $total_deuda;
        $data['obs'] = $cuenta->getObservacion();

        $this->view->show1("estadocuotas.html", $data);
    }

//=======================================================================================================================

	public function consultaSaldos()
	//genera un listado con los saldos y deudas a la fecha indicada de todas las cuentas
	{
		$cuenta = new ModeloCuenta();
		$cuota = new ModeloCuota();
		$i=0;
		//tomo por defecto el corriente a�o
		$fecha = date('31/12/Y');
		if(isset($_GET['fechamora']) && $_GET['fechamora'] != "")
			$fecha = $_GET['fechamora'];
		$lista_cuentas = $cuenta->listadoTotal();
		if(count($lista_cuentas) <= 0)
		{
			$mensaje = htmlentities("No hay cuentas para listar.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return false;
		}
		foreach ($lista_cuentas as $varc)
		{
			$idcuenta = $varc['id'];
			$cuota->putIdCuenta($idcuenta);
			//obtengo las cuotas adeudadas y vencidas de la cuenta
			$lista_cuotas = $cuota->listadoCuotasCuentaDeuda("");
			if(count($lista_cuotas) > 0)
			{                            
				$saldo_total = $cobrado_total = $int_mora_total = $int_mora_fecha = 0;
				foreach ($lista_cuotas as $c)
				{                                    
					$cuota->putIdCuota($c['id']);
					$cuota->traerCuota();
					$saldo_total += $cuota->getSaldo()*1;
					$cobrado_total += $cuota->getCobrado()*1;
					$int_mora_total += $cuota->getInteresMora()*1;
				}
				//obtengo la mora total a la fecha indicada
				$_GET['fechamora'] = $fecha;
				$arr_mora = $cuota->calcularMoraRango($lista_cuotas[0]['nrocuota'], $lista_cuotas[count($lista_cuotas)-1]['nrocuota'], $idcuenta);
				if(!$arr_mora)
				{
					$mensaje = htmlentities("Error en el c�lculo de intereses, corrobore los datos");
					$data['mensaje'] = $mensaje;
					$this->view->show1("mostrarerror.html", $data);
					return false;
				}
				foreach ($arr_mora as $c)
				{
					$int_mora_fecha += ($c['intmora'] + $c['moraanterior']);
				}
				if(($saldo_total>0) || ($int_mora_total>0))
				{
					//genero el arreglo con todos los datos
					$data_cuenta[$i]['saldototal'] = $saldo_total;
					$data_cuenta[$i]['cobradototal'] = $cobrado_total;
					$data_cuenta[$i]['morafecha'] = $int_mora_fecha;
					$data_cuenta[$i]['nrocuenta'] = $varc['nrocuenta'];
					$data_cuenta[$i]['expediente'] = $varc['expediente'];
					$data_cuenta[$i]['valorliquidacion'] = $varc['valorliquidacion'];
					$data_cuenta[$i]['titular'] = $varc['solicitante'];
					$data_cuenta[$i]['tipo'] = $varc['tipo'];
					$i++;
				}
			}
		}
		$data['cuentas'] = $data_cuenta;
		$data['fechamora'] = $fecha;

		$this->view->show1("saldos.html", $data);
	}

//=======================================================================================================================

	public function consultarArchivosIngresos()
	{
		$archivos = new Modeloarchivocobro();
		//armado de la condicion de filtro por fechas
		$fechadesde = date('01/m/Y');
		$fechahasta = date('d/m/Y');
		$condicion = " && fechaimportacion>='".cadenaAFecha($fechadesde)."' && fechaimportacion<= '".cadenaAFecha($fechahasta)."' ";
		if(isset($_GET['fechadesde']) && $_GET['fechadesde'] != "")
		{
			$fechadesde = $_GET['fechadesde'];
			$condicion = " && fechaimportacion>='".cadenaAFecha($fechadesde)."' ";
			if(isset($_GET['fechahasta']) && $_GET['fechahasta'] != "")
			{
				$fechahasta = $_GET['fechahasta'];
				$condicion .= " && fechaimportacion<='".cadenaAFecha($fechahasta)."' ";
			}
		}
		$lista = $archivos->listadoArchivosMovimientos($condicion);
		$data['liztado'] = $lista;
		$lista_sin_movs = $archivos->listadoArchivosSinMovimientos();
		$data['sinmovs'] = $lista_sin_movs;
		$data['fechadesde'] = $fechadesde;
		$data['fechahasta'] = $fechahasta;
		$this->view->show1("ingresos.html", $data);
	}

//=======================================================================================================================

	public function consultarMovimientos()
	{
		if(isset($_GET['idarchivo']) && $_GET['idarchivo'] > 0)
		{
			$mov = new ModeloMovimiento();
			$mov->putIdArchivo($_GET['idarchivo']);
			$lista_movs = $mov->listadoMovimientosArchivo();
			$data['liztado'] = $lista_movs;
		}
		$this->view->show1("movimientosarchivo.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function vermovimiento()
	{
		$mov = new modelomovimiento();
		if(isset($_GET['idcuota']))
		{
		   $mov->putIdCuota($_GET['idcuota']);
		   $liztado = $mov->traermovimiento();
		}
		$data['liztado'] = $liztado;
		if(isset($_GET['nrocuota']))
			$data['nrocuota'] = $_GET['nrocuota'];

		$this->view->show1("movimiento.html", $data);
	}


}

?>