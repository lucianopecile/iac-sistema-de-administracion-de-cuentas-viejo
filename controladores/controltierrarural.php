<?php
require_once 'modelos/modelotierrarural.php';
require_once 'modelos/modeloseccion.php';
require_once 'modelos/modelofraccion.php';
require_once 'modelos/modelocolonia.php';
require_once 'modelos/modeloensanche.php';
require_once 'modelos/modelolegua.php';



class ControlTierraRural
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrartierrarural()
	// muestra todas las tierrarural en un html con una tabla
	{
		$tierrarural = new modelotierrarural();
		$liztado = $tierrarural->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("tierrarural.html", $data);
 	}

//---------------------------------------------------------------------------------------
	
	public function altatierrarural()
	{
		$alta= new modelotierrarural();
		$this->cargavariables($alta, ALTA);
		$altaok = $alta->altatierrarural();
		if (!$altaok)
		{
			$mensaje = "En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde";
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return(0);
		}
		else{
		
		  return($altaok);
		}
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificartierrarural()
	{
		$modifica= new modelotierrarural();
		$this->cargavariables($modifica,MODIFICAR);
		
        $modificado=$modifica->modificartierrarural();
	    
        
	   if (!$modificado){
	      $mensaje= "En este momento no se puede modificar la tierra rural";
	      $data['mensaje']=$mensaje;
    	  $this->view->show1("mostrarerror.html", $data);
		  return(false);
        }
		else{
	      return(true);
		}	
	}
		
//---------------------------------------------------------------------------------------
	
	public function borrartierrarural()
	{
		$borra= new modelotierrarural();
		$borra->putIdTierraRural($_POST['id']);
		$borrado=$borra->borrartierrarural();
		if (!$borrado)
		{
			$mensaje ="En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde";
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrartierrarural();		 
	}



//-----------------------------------------------------------------------------------

//retorna los datos de una tierrarural si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertierrarural()
	{
		
	$tierrarural = new modelotierrarural();
    if (isset($_GET['idtierra'])) { 
	
        $tierrarural->putIdTierraRural($_GET['idtierra']);
	
       	$empent = $tierrarural->traertierrarural();
	
     	if (!$empent){
 	       $mensaje= "En este momento no se puede realizar la operacion para ver solicitud urbana, intentelo mas tarde";
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$seccion=new modeloseccion;
	$fraccion=new modelofraccion;
	$colonia=new modelocolonia;
	$ensanche=new modeloensanche;
	$legua=new modelolegua;
	
	$data=$this->cargarPlantillaModificar($tierrarural,$seccion,$fraccion,$colonia,$ensanche,$legua);
	  $this->view->show("abmtierrarural.html", $data);
	
	}	
//-----------------------------------------------------------------------------------

//retorna los datos de una tierrarural si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertierraruralasociada($tierrarural)
	{
		
	    $empent = $tierrarural->traertierraruralasociada();

     	if (!$empent){
 	       $mensaje= "No se pudo localizar la tierra rural asociada";
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
       
	
  
	
	$seccion=new modeloseccion;
	$fraccion=new modelofraccion;
	$colonia=new modelocolonia;
	$ensanche=new modeloensanche;
	$legua=new modelolegua;
	
	$data=$this->cargarPlantillaModificar($tierrarural,$seccion,$fraccion,$colonia,$ensanche,$legua);
	return($data);
	
	}	
	
	
//-----------------------------------------------------------------------------------
	 public function cargarPlantillaModificar($parTierraRural,$parSeccion,$parFraccion,$parColonia,$parEnsanche,$parLegua) 
{  
   
	
	$vseccion= $parSeccion->TraerTodos();
	$vseccion['selected']=  $parTierraRural->getIdSeccion();
    $vfraccion= $parFraccion->TraerTodos();
	$vfraccion['selected']=  $parTierraRural->getIdFraccion();
	$vcolonia= $parColonia->TraerTodos();
	$vcolonia['selected']=  $parTierraRural->getIdColonia();
    $vensanche= $parEnsanche->TraerTodos();
	$vensanche['selected']=  $parTierraRural->getIdEnsanche();
	$vlegua= $parLegua->TraerTodos();
	$vlegua['selected']=  $parTierraRural->getIdLegua();

	$idtierrarural =  $parTierraRural->getIdTierraRural();
	$quehacer = "";
	if ($idtierrarural == 0)
			$quehacer = ALTA;
	else
		if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
	  
	switch($quehacer)
	{
      case ALTA:
		
		$parTierraRural->putIdTierraRural("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altatierrarural";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificartierrarural";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrartierrarural";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parTierraRural->getIdTierraRural();
		$parametros = array(
                    "TITULOFORM" =>  "Tierras Rurales -> Modificar",
                    "ID" => $parTierraRural->getIdTierraRural(),
					"IDSECCION" => $parTierraRural->getIdSeccion(),
					"IDFRACCION" =>$parTierraRural->getIdFraccion(), 
					"IDCOLONIA" =>$parTierraRural->getIdColonia(), 
					"IDENSANCHE" =>$parTierraRural->getIdEnsanche(), 
					"IDLEGUA" =>$parTierraRural->getIdLegua(), 
					"NROLOTE" =>$parTierraRural->getNroLote(),
    				"OBSERVACION"=>$parTierraRural->getObservacion(),
					"LISTASECCION"=>$vseccion,
					"LISTAFRACCION"=>$vfraccion,
					"LISTACOLONIA"=>$vcolonia,
					"LISTAENSANCHE"=>$vensanche,
					"LISTALEGUA"=>$vlegua,
					"NOVERTR"=>"style='visibility:hidden'",
					"nombreaccionTR"=>$nombreaccion,
					"CONFIGURACIONTR"=>"",
					"SOLOLECTURATR"=>"",
					"ENAB_DISATR"=>"",
					"DISA_MODITR"=>"disabled='disabled'",
					"nombrebotonTR"=>$nombreboton
                  
                    );
        break;
		case BAJA:
        $idsoli=$parTierraRural->getIdTierraRural();
	    $parametros = array(
                    "TITULOFORM" =>  "Tierras Rurales -> Eliminar",
                    "ID" => $parTierraRural->getIdTierraRural(),
					"IDSECCION" => $parTierraRural->getIdSeccion(),
					"IDFRACCION" =>$parTierraRural->getIdFraccion(), 
					"IDCOLONIA" =>$parTierraRural->getIdColonia(), 
					"IDENSANCHE" =>$parTierraRural->getIdEnsanche(), 
					"IDLEGUA" =>$parTierraRural->getIdLegua(), 
					"NROLOTE" =>$parTierraRural->getNroLote(),
    				"OBSERVACION"=>$parTierraRural->getObservacion(),
					"LISTASECCION"=>$vseccion,
					"LISTAFRACCION"=>$vfraccion,
					"LISTACOLONIA"=>$vcolonia,
					"LISTAENSANCHE"=>$vensanche,
					"LISTALEGUA"=>$vlegua,
					"NOVERTR"=>"style='visibility:hidden'",
					"nombreaccion1TR"=>$nombreaccion,
					"DISA_MODITR"=>"",
					"SOLOLECTURATR"=>"readonly='readonly'",
					"ENAB_DISATR"=>"disabled='disabled'",
					"nombrebotonTR"=>$nombreboton
                    );
	     break;
		 case ALTA:
		 
 
         
  
	     $parametros = array(
	                "TITULOFORM" =>  "Tierras Rurales -> Agregar",
                    "ID" => 0,
					"IDSECCION" => 0,
					"IDFRACCION" =>0, 
					"IDCOLONIA" =>0, 
					"IDENSANCHE" =>0, 
					"IDLEGUA" =>0, 
					"NROLOTE" =>0,
    				"OBSERVACION"=>"",
					"LISTASECCION"=>$vseccion,
					"LISTAFRACCION"=>$vfraccion,
					"LISTACOLONIA"=>$vcolonia,
					"LISTAENSANCHE"=>$vensanche,
					"LISTALEGUA"=>$vlegua,
					"CONFIGURACIONTR"=>"",
				    "SOLOLECTURATR"=>"",
					"ENAB_DISATR"=>"",					
	                 "DISA_MODITR"=>"",
					"nombreaccionTR"=>$nombreaccion,
					"nombrebotonTR"=>$nombreboton,
					   );
	
	     break;
		 default :
		 $idsoli=$parTierraRural->getIdTierraRural();
		$parametros = array(
                    "TITULOFORM" =>  "Tierras Rurales -> Ver",
                    "ID" => $parTierraRural->getIdTierraRural(),
					"IDSECCION" => $parTierraRural->getIdSeccion(),
					"IDFRACCION" =>$parTierraRural->getIdFraccion(), 
					"IDCOLONIA" =>$parTierraRural->getIdColonia(), 
					"IDENSANCHE" =>$parTierraRural->getIdEnsanche(), 
					"IDLEGUA" =>$parTierraRural->getIdLegua(), 
					"NROLOTE" =>$parTierraRural->getNroLote(),
    				"OBSERVACION"=>$parTierraRural->getObservacion(),
					"LISTASECCION"=>$vseccion,
					"LISTAFRACCION"=>$vfraccion,
					"LISTACOLONIA"=>$vcolonia,
					"LISTAENSANCHE"=>$vensanche,
					"LISTALEGUA"=>$vlegua,
					"nombreaccionTR"=>$nombreaccion,
					"NOVERTR"=>"style='visibility:hidden'",
					"CONFIGURACIONTR"=>"style='visibility:hidden'",
					"SOLOLECTURATR"=>"readonly='readonly'",
					"ENAB_DISATR"=>"disabled='disabled'",
					"DISA_MODITR"=>"",
					"nombrebotonTR"=>$nombreboton
                    );
		 					
		 
		} 				



        return $parametros;
  }

//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdTierraRural($_POST["idtierrarural"]);
		}
		
   
   
	
	$clasecarga->putNroLote($_POST["nrolote"]);
     $obs=$_POST["observaciontr"];
		if(!empty($obs)){
		   $nuevaobservacion=$_POST["observacionanttr"]."\n".$_POST["observaciontr"];
		 } 
		 else{
		    $nuevaobservacion=$_POST["observacionanttr"];
		 } 
		$clasecarga->putObservacion($nuevaobservacion);
		if($_POST["idseccion"]==0){
		   	$clasecarga->putIdSeccion('NULL');       
		}
		else{
	       	$clasecarga->putIdSeccion($_POST["idseccion"]);

		}
	    if($_POST["idfraccion"]==0){
		     $clasecarga->putIdFraccion('NULL');       
		}
		else{
	       $clasecarga->putIdFraccion($_POST["idfraccion"]);
		}
   
   
   if($_POST["idcolonia"]==0){
		   	$clasecarga->putIdColonia('NULL');       
		}
		else{
	       	$clasecarga->putIdColonia($_POST["idcolonia"]);

		}
	    if($_POST["idensanche"]==0){
		     $clasecarga->putIdEnsanche('NULL');       
		}
		else{
	       $clasecarga->putIdEnsanche($_POST["idensanche"]);
		}
		 if($_POST["idlegua"]==0){
		     $clasecarga->putIdLegua('NULL');       
		}
		else{
	       $clasecarga->putIdLegua($_POST["idlegua"]);
		}
   }	
	
	


   



}

?>