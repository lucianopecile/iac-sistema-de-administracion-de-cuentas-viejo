<?php
require_once 'modelos/modeloarrendamiento.php';
require_once 'modelos/modelosolicitudrural.php';
require_once 'modelos/modelounidad.php';

class ControlArrendamiento
{
  	function __construct()
	{
	    $this->view = new View();
	}
 
	
//============================================================================
	
	public function verarrendamiento()
	//retorna los datos de un arrendamiento si se carg� el id, sino retorna campos en blanco
	{	
		$arr = new ModeloArrendamiento();
		$s_rural = new ModeloSolicitudRural();

		if (isset($_GET['idsolicitudrural'])) //si es modificacion o eliminacion
		{			
		    $s_rural->putIdSolicitudRural($_GET['idsolicitudrural']);
			$s_ok = $s_rural->traersolicitudrural();
			$arr->putIdArrendamiento($s_rural->getIdArrendamiento());
			$arr_ok = $arr->traerArrendamiento();
			if (!$arr_ok || !$s_ok)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
	    } 
	    $unidad = new ModeloUnidad(); 
		$data = $this->cargarPlantillaModificar($arr, $unidad);
		$this->view->show("abmarrendamiento.html", $data);
	}

//============================================================================
	
	public function altaarrendamiento()
	// carga el nuevo arrendamiento en el modelo
	{
		$alta = new ModeloArrendamiento();
		$this->cargavariables($alta, ALTA);
		$result = $alta->altaarrendamiento();
		if(!$result)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
	    	$this->view->show1("mostrarerror.html", $data);
		}
		else{
		  return $result;
		}  
	}

//============================================================================	

	public function modificararrendamiento()
	{
		$modifica = new ModeloArrendamiento();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificararrendamiento();
		if(!$modificado)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
	    	$this->view->show1("mostrarerror.html", $data);
		}
		return $modificado;
	}
	
//============================================================================
	
	public function borrararrendamiento()
	{
		$borra = new ModeloArrendamiento();
		$borra->putIdArrendamiento($_GET['id']);
		$borrado = $borra->borrarextanjero();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
		}
		return $borrado; 
	}

//============================================================================

	public function cargarPlantillaModificar($parArrendamiento, $parUnidad) 
	{
		$vu = $parUnidad->traerTodos();
		$vu['selected'] = $parArrendamiento->getIdUnidad();  
		$idarr= $parArrendamiento->getIdArrendamiento();

		$quehacer = "";
		if ($idarr == 0)
			$quehacer = ALTA;
		else
			$quehacer = MODIFICAR;
		
		switch($quehacer)
		{
			case ALTA:	      
	        $nombreboton="Guardar";
		    $nombreaccion="altaarrendamiento";
			break;	 
			
			case MODIFICAR:
			$nombreboton="Guardar";
			$nombreaccion="modificararrendamiento";
			break;
			
			case BAJA:
			$nombreboton="Eliminar";
			$nombreaccion="borrararrendamiento";  
			break;

			default:  
			$nombreboton="";
			$nombreaccion="";
		}
	 
		switch ($quehacer)
		{
			case MODIFICAR:
				$parametros = array(
                    "TITULO"=>"Modificar",
                    "ID"=>$parArrendamiento->getIdArrendamiento(),
					"NOMBRE"=>$parArrendamiento->getNombre(),
					"PROPIETARIO"=>$parArrendamiento->getPropietario(), 
					"SUPERFICIE"=>$parArrendamiento->getSuperficie(),
					"IDUNIDAD"=>$parArrendamiento->getIdUnidad(),
					"LISTAUNIDAD"=>$vu,
					"EXPLOTACION"=>$parArrendamiento->getExplotacion(),
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					);
        			break;

        case BAJA:
			$parametros = array(
					"TITULO" => "Eliminar",
                    "ID"=>$parArrendamiento->getIdArrendamiento(),
					"NOMBRE"=>$parArrendamiento->getNombre(),
					"PROPIETARIO"=>$parArrendamiento->getPropietario(), 
					"SUPERFICIE"=>$parArrendamiento->getSuperficie(),
					"IDUNIDAD"=>$parArrendamiento->getIdUnidad(),
					"LISTAUNIDAD"=>$vu,
					"EXPLOTACION"=>$parArrendamiento->getExplotacion(),
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					);
					break;
		 
		case ALTA:
		 	$parametros = array(
					"TITULO" => "Alta",
                    "ID"=>0,
					"NOMBRE"=>"",
					"PROPIETARIO"=>"", 
					"SUPERFICIE"=>"",
					"IDUNIDAD"=>0,
					"LISTAUNIDAD"=>$vu,
					"EXPLOTACION"=>"",
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					);
					break;

		default:
			$parametros = array(
                    "TITULO" => "Consultar",
                   "ID"=>$parArrendamiento->getIdArrendamiento(),
					"NOMBRE"=>$parArrendamiento->getNombre(),
					"PROPIETARIO"=>$parArrendamiento->getPropietario(), 
					"SUPERFICIE"=>$parArrendamiento->getSuperficie(),
					"IDUNIDAD"=>$parArrendamiento->getIdUnidad(),
					"LISTAUNIDAD"=>$vu,
					"EXPLOTACION"=>$parArrendamiento->getExplotacion(),
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					);
		} 				
        return $parametros;
	}


//============================================================================

	public function cargavariables($clasecarga, $op)
	//carga las variables de la clase en el alta o en la modificacion
	{
		if ($op == MODIFICAR)
			$clasecarga->putIdArrendamiento($_POST["idarrendamiento"]);
      
		$clasecarga->putNombre($_POST["nombre"]);
        $clasecarga->putPropietario($_POST["propietario"]);
        $clasecarga->putSuperficie($_POST["superficie"]);
        if ($_POST['idunidad'] == 0)
        	$clasecarga->putIdUnidad("NULL");
        else 
        	$clasecarga->putIdUnidad($_POST["idunidad"]);
        
        $clasecarga->putExplotacion($_POST["explotacion"]);
	}

}

?>