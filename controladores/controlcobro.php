<?php
require_once 'modelos/modelocuota.php';
require_once 'modelos/modelocobro.php';
require_once 'modelos/modelocuenta.php';
require_once 'modelos/modeloarchivocobro.php';
require_once 'modelos/modelomovimiento.php';
require_once 'modelos/modeloparametro.php';
require_once 'modelos/modelomovimientoexterno.php';
require_once 'modelos/modelolog.php';

class ControlCobro
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	//	$this->report=new FPDF();

	}
	
//---------------------------------------------------------------------------------

	public function mostrarCuentaPagoManual()
	// muestra todos los cuotas en un html con una tabla
	{
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
        $this->view->show1("listapagomanual.html", $data);
	}

//-------------------------------------------------------------------------------

	public function pagoManual()
	{
		$cuota = new ModeloCuota();
		
		if (isset($_GET['idcuota']))
		{
			$cuota->putIdCuota($_GET['idcuota']);
			$c_ok = $cuota->traerCuota();
            if (!$c_ok)
			{
				$mensaje = htmlentities("No puede realizar esta operaci�n, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
	    }
		$data = $this->cargarPlantillaModificar($cuota);
		$this->view->show("pagomanual.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cuentaPagoManual()
	//muestra la pantalla para realizar un pago manual de las cuotas
	{
		$cuenta = new ModeloCuenta();
		$cobro_cuenta = new ModeloCobro();
		$cuotas = new ModeloCuota();
		if(isset($_GET['idcuenta']) && $_GET['idcuenta'] > 0)
		{
			$cobro_cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traerCuenta();
			$cuotas->putIdCuenta($_GET['idcuenta']);
			$liztado = $cuotas->listadoTotal();
			if(isset($_GET['fechadiferir']) && isset($_GET['cuotadesde']))
			{
				$this->diferir($cobro_cuenta);
				$liztado = $cuotas->listadoCuotasCuenta();
			}
			if(!$liztado)
			{
				$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver cuotas, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			$idcuenta = $_GET['idcuenta'];
			$deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
			$deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
			$saldo = $deuda_vencida + $deuda_no_vencida;
			$data['liztado'] = $liztado;
			$data['idcuenta'] = $idcuenta;
			$data['solicitante'] = $cuotas->getSolicitante();
			$data['anioexpediente'] = $cuotas->getAnioExpediente();
			$data['letraexpediente'] = $cuotas->getLetraExpediente();
			$data['nroexpediente'] = $cuotas->getNroExpediente();
			$data['nrocuenta'] = $cuotas->getNroCuenta();
			$data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
			$data['cuentacorriente'] = $cuotas->getCuentaCorriente();
			$data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['deudavencida'] = number_format($deuda_vencida,2,',','.');
			$data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
			$data['saldo'] = number_format($saldo,2,',','.');
			$data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
			$data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
			$data['observacionant'] = $cuenta->getObservacion();
		}else{
			$mensaje = htmlentities("No se seleccion� correctamente la cuenta, int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->view->show1("cuentapagomanual.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cargarPlantillaModificar($parCuota)
	{
		$nombreboton="Guardar cobro";
		$nombreaccion="modificarpagomanual";

 		$parametros = array("TITULO"=>"Cobro manual",
					"IDCUOTA"=>$parCuota->getIdCuota(),
					"NROCUOTA"=>$parCuota->getNroCuota(),
					"IDCUENTA"=>$parCuota->getIdCuenta(),
					"NROCUENTA"=>$parCuota->getNroCuenta(),
					"SOLICITANTE"=>$parCuota->getSolicitante(),
					"NROCUOTA"=>$parCuota->getNroCuota(),
					"MONTOCUOTA"=>number_format($parCuota->getMontoCuota(),2,',','.'),
					"SALDO"=>number_format($parCuota->getSaldo(),2,',','.'),
					"SALDOSINFORMATO"=>$parCuota->getSaldo(),
					"FECHAVENC"=>fechaACadena($parCuota->getFechaVenc()),
					"FECHAPAGO"=>fechaACadena($parCuota->getFechaPago()),
					"INTERES"=>number_format($parCuota->getInteres(),2,',','.'),
					"INTERESMORA"=>number_format($parCuota->getInteresMora(),2,',','.'),
					"INTERESMORASINFORMATO"=>$parCuota->getInteresMora(),
					"FECHAMORA"=>fechaACadena($parCuota->getFechaCalculoMora()),
					"COBRADOTOTAL"=>number_format($parCuota->getCobrado(),2,',','.'),
					"GASTOSFIJOS"=>$parCuota->calcularGastosCobrados($parCuota->getIdCuota()),
					"OBSERVACION"=>$parCuota->getObservacion(),
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					);
		return $parametros;
	}

//---------------------------------------------------------------------------------

	public function mostrarcuenta()
	// muestra todos los cuotas en un html con una tabla
	{		
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
        $this->view->show1("listadiferir.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function diferirVencimientos()
	//muestra la pantalla para diferir los vencimientos de las cuotas
	{
		$cuenta = new ModeloCuenta();
		$cobro_cuenta = new ModeloCobro();
		$cuotas = new ModeloCuota();
		if(isset($_GET['idcuenta']) && $_GET['idcuenta'] > 0)
		{
			$cobro_cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traerCuenta();
			$cuotas->putIdCuenta($_GET['idcuenta']);
			$liztado = $cuotas->listadoTotal();
			if(isset($_GET['fechadiferir']) && isset($_GET['cuotadesde']))
			{
				$this->diferir($cobro_cuenta);
				$liztado = $cuotas->listadoCuotasCuenta();
			}
			if(!$liztado)
			{
				$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver cuotas, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			$idcuenta = $_GET['idcuenta'];
			$deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
			$deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
			$saldo = $deuda_vencida + $deuda_no_vencida;
			$data['liztado'] = $liztado;
			$data['idcuenta'] = $idcuenta;
			$data['solicitante'] = $cuotas->getSolicitante();
			$data['anioexpediente'] = $cuotas->getAnioExpediente();
			$data['letraexpediente'] = $cuotas->getLetraExpediente();
			$data['nroexpediente'] = $cuotas->getNroExpediente();
			$data['nrocuenta'] = $cuotas->getNroCuenta();
			$data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
			$data['cuentacorriente'] = $cuotas->getCuentaCorriente();
			$data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['deudavencida'] = number_format($deuda_vencida,2,',','.');
			$data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
			$data['saldo'] = number_format($saldo,2,',','.');
			$data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
			$data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
			$data['observacionant'] = $cuenta->getObservacion();
			if(isset($_GET['fechadiferir'])){
				$data['cuotadesde'] = $_GET['cuotadesde'];
				$data['fechadiferir'] = $_GET['fechadiferir'];
			}else{
				$data['cuotadesde'] = 1;
				$data['fechadiferir'] = date('d/m/Y');
			}
		}else{
			$mensaje = htmlentities("No se seleccion� correctamente la cuenta, int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->view->show1("diferir.html", $data);
	}

//==============================================================================================

	public function importarCobro()
	{
		$importar = new modelocobro();
		$this->view->show1("importar.html", $data);
	}
	
//----------------------------------------------------------------------------------------------

	public function subirarchivo()
	{
		//tomo el valor de un elemento de tipo texto del formulario
		$cadenatexto = $_POST["cadenatexto"];
		//datos del arhivo
		$nombre_archivo = strtolower($_FILES['userfile']['name']);
		$tipo_archivo = $_FILES['userfile']['type'];
		$tamano_archivo = $_FILES['userfile']['size'];
		//compruebo si las caracteristicas del archivo son las que deseo
		if ((!strpos($tipo_archivo, "text")==0) || ($tamano_archivo > 100000))
		{
			$mensaje = htmlentities("El tama�o o el tipo del archivo ".$nombre_archivo." no es correcto");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}else{
			if(move_uploaded_file($_FILES['userfile']['tmp_name'], "cobros/".$nombre_archivo))
			{
				$id_archivo = $this->guardarCobro($nombre_archivo,$tipo_archivo,$tamano_archivo);
				if($id_archivo)
				{
					$res = $this->procesarcobro($nombre_archivo, $id_archivo);
					$data['nombre'] = $nombre_archivo;
					$data['tipo'] = $tipo_archivo;
					$data['size'] = $tamano_archivo;
					//mensaje de resultado del procesamiento, con error o exito
					if($res){
						$data['mensaje'] = "&eacute;xito";
						$estado = EXITO;
					}else{
						$data['mensaje'] = "errores";
						$estado = ERRORES;
					}
					//registro el estado del archivo procesado
					$archivo = new Modeloarchivocobro();
					$archivo->putEstado($estado);
					$archivo->registrarEstado($id_archivo);
					$this->view->show1("archivoimportado.html",$data);
				}
			}else{
				$mensaje = htmlentities("Ocurri� un error al importarse el archivo. No pudo guardarse");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
		}
	}

//======================================Guardar Datos Archivo Cobros====================

	public function guardarCobro($nombre,$tipo,$size)
	// guarda en la DB los datos del archivo importado siempre q no exista de antemano, sino falla
	{
		$cobros = new modeloarchivocobro();
		$cobros->putNombre(strtolower($nombre));
		$cobros->putTipo($tipo);
		$cobros->putSize($size);
		$cobros->putIdUsr($_SESSION["s_idusr"]);
		$existe = $cobros->traerarchivo($nombre);
		if(!$existe)
		{
			$guardo = $cobros->AltaArchivoCobro();
			if(!$guardo)
			{
				$mensaje = htmlentities("Error al registrar la importacion del archivo ".$nombre.". Int�ntelo de nuevo");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return false;
			}else{
				//return true;
				//devulevo el id del archivo dado de alta
				$f_ok = $cobros->traerArchivoNombre();
				if(!$f_ok)
				{
					$mensaje = htmlentities("Error al registrar la importacion del archivo ".$nombre.". Int�ntelo de nuevo");
					$data['mensaje'] = $mensaje;
					$this->view->show1("mostrarerror.html", $data);
					return false;
				}
				return $cobros->getIdArchivoCobro();
			}
		}else{
			$mensaje = htmlentities("Error. El archivo ya fue importado y procesado anteriormente. No se puede procesar nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return(false);
		}
	}

//======================================================================================

	public function procesarcobro($nombre_archivo, $id_archivo)
	{
		$nombre_sin_extension = explode(".", $nombre_archivo);
		$fd = fopen ("./cobros/".$nombre_archivo, "r");
		if($fd)
		{
			$result = true;
			$nro_linea = 1;
			while (!feof($fd))
			{
				$cadena = "";
				$i = 1;
				while($i<=33 && !feof($fd))
				{
					$char = fgetc($fd);
					if(!($char == "\n" || $char == "\r"))
						$cadena = $cadena.$char;
					$i++;
				}
				$linea = $this->procesarlinea($cadena, $nombre_sin_extension[0], $nro_linea, $id_archivo);
				if(!$linea)
					$result = false;
				$nro_linea++;
			}
		}else{
			$mensaje = htmlentities("Error al procesar el archivo. Corrobore el tipo de archivo e int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return false;
		}
		fclose ($fd);
		return $result;
	}

	public function procesarlinea($datoscobro, $nombre_archivo, $nro_linea, $id_archivo)
	//procesa una linea del archivo de corbos importado
	{
		//traigo el id del archivo para registrar en el movimiento
		$result = true;
		//si la linea esta en blanco la salteo
		if($datoscobro == "")
			return true;
		//extraigo de la linea todos los valores
		$anio = substr($datoscobro,0,2);
		$mes = substr($datoscobro,2,2);
		$dia = substr($datoscobro,4,2);
		$montoentero = substr($datoscobro,6,6);
		$montodecimal = substr($datoscobro,12,2);
		$nrocuenta = substr($datoscobro,14,7);
		$nrocuota = substr($datoscobro,21,3);
		$tipooper = substr($datoscobro,24,1);
		$gastos_entero = substr($datoscobro, 25, 4);
		$gastos_decimal = substr($datoscobro, 29, 2);
		$oper = $tipooper*1;
		$idcuenta = $nrocuenta*1;
    	$nrocuota = $nrocuota*1;
		$monto_cobro = ($montoentero*1)+($montodecimal*1/100);
		$monto_gastos = ($gastos_entero*1)+($gastos_decimal*1/100);
		$monto_cobro = $monto_cobro - $monto_gastos;
		$fechaproceso = date('d/m/Y');
		$fechacobro = $dia."/".$mes."/20".$anio;
               
		$tipomovimiento = $oper;
		$registro = 2;
		//importe de gastos fijos procesado, y demas movimientos
		$movimiento_gastos = $monto_gastos;
		$movimiento_saldo = 0;
		$movimiento_mora = 0;
		$total_movimiento = $monto_cobro + $monto_gastos;
		//controlo que la fecha sea valida
                
		if(!is_date($fechacobro))
		{
			//ERROR, la fecha de cobro no es valida.
			$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
			if($fd)
			{
				$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Fecha de cobro no v�lida. Se omite la l�nea. \n";
				fwrite($fd, $linea);
			}
			fclose ($fd);
			return false;
		}
		//rechazo los montos negativos
		if($monto_cobro < 0 || $monto_gastos < 0)
		{
			//ERROR, uno de los montos es un valor menor que 0.
			$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
			if($fd)
			{
				$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Monto inv�lido. Se omite la l�nea. \n";
				fwrite($fd, $linea);
			}
			fclose ($fd);
			return false;
		}
		//obtengo el nro de cuenta para mensajes de error
		$cuenta = new ModeloCuenta();
		$cuenta->putIdCuenta($idcuenta);
		$cuenta->traerCuenta();
		$nrocuenta = $cuenta->getNroCuenta();
		//traigo la(s) cuota(s) a pagar
		$cuotas = new modelocuota();
		$cuotas->putIdCuenta($idcuenta);
		$cuotas->putNroCuota($nrocuota);
		
                if($oper == 9)
                    $existe=false;
                else
                    $existe = $cuotas->traerCuotaCuenta();
		
                if($existe)
		{
			//si es cobro normal
			if($oper == 1)
			{
				//obtengo los valores actuales de la cuota y deuda
				$saldo = $cuotas->getSaldo()*1;
				$montocobrado = $cuotas->getCobrado()*1;
				//si el saldo es mayor o igual a lo cobrado, es valido
				if($saldo >= $monto_cobro)
				{
					$nuevosaldo = $saldo-$monto_cobro;
					$movimiento_saldo = $monto_cobro;	// importe restado al saldo
				}else{
					// ERROR, cobrado es mayor que el saldo
					$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
					if($fd)
					{
						$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Monto cobrado mayor al saldo de la cuota ".$nrocuota.", cuenta ".$nrocuenta.". Se imput� el cobro igualmente.\n";
						fwrite($fd, $linea);
					}
					fclose ($fd);
					$nuevosaldo = 0;
					$movimiento_saldo = $saldo;	// importe restado al saldo
					$result = false;
				}
				//calculo y asigno los nuevos valores		
				$montocobrado = $montocobrado+$monto_cobro;
				$cuotas->putCobrado($montocobrado);
                                
				$cuotas->putSaldo($nuevosaldo);
				$cuotas->putFechaPago(cadenaAFecha($fechacobro));
				$cuotas->putIdUsrMod($_SESSION["s_idusr"]);
				$cuotas->guardarCobro();
			}
			elseif($oper == 2)		//si es cobro parcial de una cuota
			{
				//obtengo los valores actuales de la cuota y deuda
				$saldo = $cuotas->getSaldo()*1;
				$montocobrado = $cuotas->getCobrado()*1;
				$interesmora = $cuotas->getInteresMora()*1;
				$totaldeuda = $saldo+$interesmora;
				//si la deuda (saldo + mora) es mayor o igual a lo cobrado, es valido
				if($totaldeuda >= $monto_cobro)
				{					
					//descuento primero de la deuda por mora si tiene
					if($interesmora <= $monto_cobro)
					{
						$movimiento_mora = $interesmora;		//monto transferido para la mora
						$movimiento_saldo = ($monto_cobro-$interesmora);	//monto transferido al saldo
						$nuevosaldo = $saldo - ($monto_cobro-$interesmora);
						$interesmora = 0;
					}else{
						$movimiento_mora = $monto_cobro;	//monto transferido para la mora
						$movimiento_saldo = 0;	//monto transferido al saldo
						$interesmora -= $monto_cobro;
						$nuevosaldo = $saldo;
					}
				}else{
					; // ERROR, cobrado es mayor que el saldo + int mora
					$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
					if($fd)
					{
						$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Monto cobrado mayor a la deudade la cuota ".$nrocuota.", cuenta".$nrocuenta.". Se imput� el cobro igualmente.\n";
						fwrite($fd, $linea);
					}
					fclose ($fd);
					$movimiento_mora = $interesmora;
					$movimiento_saldo = $saldo;
					$nuevosaldo = 0;
					$interesmora = 0;
					$result = false;
				}
				$montocobrado = $montocobrado+$monto_cobro;
				$cuotas->putCobrado($montocobrado);
				$cuotas->putSaldo($nuevosaldo);
				$cuotas->putFechaPago(cadenaAFecha($fechacobro));
				$cuotas->putIdUsrMod($_SESSION["s_idusr"]);
				$cuotas->guardarCobro();
				$cuotas->putInteresMora($interesmora);
				$cuotas->guardarMora();
			}
			elseif($oper==3)	//si es el cobro de varias cuotas
			{
				$prox_cuota = true;
				//mientras haya monto para cobrar... cobro, utilizo el valor 0.05 para quitar decimales
				while ($monto_cobro >= CERO_DECIMAL)
				{
					$hubo_movimiento = false;	//cada vez que se registra un valor en una cuota se activa flag $hubo_movimiento
					//si hay una nueva cuota la cobro, sino imputo el monto restante a la ultima cuota
					if($prox_cuota)
					{
						//obtengo los valores actuales de la cuota y deuda
						$saldo = $cuotas->getSaldo()*1;
						$montocobrado = $cuotas->getCobrado()*1;
						$interesmora = $cuotas->getInteresMora()*1;
						$totaldeuda = $saldo+$interesmora;
						//si no hay saldo => no hay mora y no hay deuda, salteo esta cuota para el cobro
						if($saldo >= CERO_DECIMAL)
						{
							//antes de obtener la deuda debo asegurarme que el interes por mora haya sido calculado si la cuota esta vencida
							$fecha_calculo_mora = fechaACadena($cuotas->getFechaCalculoMora());
							$fecha_venc = fechaACadena($cuotas->getFechaVenc());
							if(($fecha_calculo_mora == "" && comparar_fechas($fecha_venc, $fechacobro) < 0 ) || 
								($fecha_calculo_mora != "" && comparar_fechas($fecha_calculo_mora, $fechacobro) < 0 ))
							{
								//si la fecha de calculo de mora es menor a la fecha de cobro, el interes por mora esta desactualizado
								//imputo el cobro y registro una advertencia
								// ADVERTENCIA, el monto de interes mora a cobrar esta desactualizado o NO calculado
								$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
								if($fd)
								{
									$linea = date(DATE_ATOM)." AVERTENCIA: La cuota ".$cuotas->getNroCuota()." de la cuenta ".$nrocuenta." se cobr� vencida, y el inter�s por mora no fue calculado o se encuentra desactualizado. Se imputa el cobro igualmente. \n";
									fwrite($fd, $linea);
								}
								fclose ($fd);
								$result = false;
							}
							//comienzo a calcular los cobros
							if($totaldeuda <= $monto_cobro)
							{
								$movimiento_mora = $interesmora;		//monto transferido para la mora
								$movimiento_saldo = $saldo;				//monto transferido al saldo
								$nuevosaldo = 0;
								$interesmora = 0;
								$montocobrado += $totaldeuda;
								$monto_cobro -= $totaldeuda;
							}else{
								if($interesmora <= $monto_cobro)
								{
									$movimiento_mora = $interesmora;		//monto transferido para la mora
									$movimiento_saldo = ($monto_cobro-$interesmora);	//monto transferido al saldo
									$nuevosaldo = $totaldeuda - $monto_cobro;
									$interesmora = 0;
									$montocobrado += $monto_cobro;
								}else{
									$movimiento_mora = $monto_cobro;	//monto transferido para la mora
									$movimiento_saldo = 0;	//monto transferido al saldo
									$interesmora -= $monto_cobro;
									$nuevosaldo = $saldo;
									$montocobrado += $monto_cobro;
								}
								$monto_cobro = 0;
							}
							//fin de calculo de cobros
							$total_movimiento = $movimiento_saldo + $movimiento_mora + $movimiento_gastos;
							$cuotas->putCobrado($montocobrado);
							$cuotas->putSaldo($nuevosaldo);
							$cuotas->putFechaPago(cadenaAFecha($fechacobro));
							$cuotas->putIdUsrMod($_SESSION['s_idusr']);
							$cuotas->guardarCobro();
							$cuotas->putInteresMora($interesmora);
							$cuotas->putFechaCalculoMora(cadenaAFecha($fecha_calculo_mora));
							$cuotas->guardarMora();
							$hubo_movimiento = true;	//cada vez que se registra un valor en una cuota se activa flag $hubo_movimiento
						}
					}else{
						$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
						if($fd)
						{
							$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Monto cobrado mayor a la deuda de la cuenta ".$nrocuenta.". Se imput� el monto restante a la �ltima cuota.\n";
							fwrite($fd, $linea);
						}
						fclose ($fd);
						//cobro todo el monto restante a la ultima cuota de la cuenta
						$nrocuota--;
						$cuotas->putIdCuenta($idcuenta);
						$cuotas->putNroCuota($nrocuota);
						//traigo los datos de la ultima cuota
						$cuotas->traerCuotaCuenta();
						$montocobrado += $monto_cobro;
						//imputo el cobro
						$cuotas->putCobrado($montocobrado);
						$cuotas->putFechaPago(cadenaAFecha($fechacobro));
						$cuotas->putIdUsrMod($_SESSION['s_idusr']);
						$cuotas->guardarCobro();
						$hubo_movimiento = true;	//cada vez que se registra un valor en una cuota se activa flag $hubo_movimiento
						$movimiento_saldo = 0;
						$movimiento_mora = 0;
						$movimiento_gastos = 0;
						$total_movimiento = $monto_cobro;
						//se cobro todo, termina el bucle
						$monto_cobro = 0;
						$result = false;
					}
					if($hubo_movimiento)
					{
						//cargo los movimiento de la cuota en la DB
						$movimientos = new ModeloMovimiento();
						$movimientos->putIdCuota($cuotas->getIdCuota());
						$movimientos->putCobrado($total_movimiento);
						$movimientos->putMontoSaldo($movimiento_saldo);
						$movimientos->putMontoMora($movimiento_mora);
						$movimientos->putMontoGastos($movimiento_gastos);
						$movimiento_gastos = 0;	//los gastos fijos se cobran una sola vez por boleta
						$movimientos->putFecha(cadenaAFecha($fechaproceso));
						$movimientos->putIdTipomov($tipomovimiento);
						$movimientos->putIdRegmov($registro);
						$movimientos->putIdArchivo($id_archivo);
						$movimientos->registrarmovimiento();
					}
					//busco si hay para cobrar una proxima cuota
					$nrocuota++;
					$cuotas = new ModeloCuota();
					$cuotas->putIdCuenta($idcuenta);
					$cuotas->putNroCuota($nrocuota);
					$prox_cuota = $cuotas->traerCuotaCuenta();
				}
				return $result;
			}else{
				//ERROR, la operacion indicada no existe
				$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
				if($fd)
				{
					$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". Operaci�n inv�lida. Se omite la l�nea. \n";
					fwrite($fd, $linea);
				}
				fclose ($fd);
				return false;
			}
			$movimientos = new modelomovimiento();
			$movimientos->putIdCuota($cuotas->getIdCuota());
			$movimientos->putCobrado($total_movimiento);
			$movimientos->putMontoSaldo($movimiento_saldo);
			$movimientos->putMontoMora($movimiento_mora);
			$movimientos->putMontoGastos($movimiento_gastos);
			$movimientos->putFecha(cadenaAFecha($fechaproceso));
			$movimientos->putIdTipomov($tipomovimiento);
			$movimientos->putIdRegmov($registro);
			$movimientos->putIdArchivo($id_archivo);
			$movimientos->registrarmovimiento();
		}else{
                        if($oper == 9){
                            $mregistro = substr($datoscobro,14,10);
                            $movimientosexterno = new  modelomovimientoexterno();
			    $movimientosexterno->putCobrado($monto_cobro);
			    $movimientosexterno->putFecha(cadenaAFecha($fechaproceso));
			    $movimientosexterno->putIdTipomov($tipooper);
			    $movimientosexterno->putIdMovimientoExterno($mregistro);
			    $movimientosexterno->putIdArchivo($id_archivo);
                            $movimientosexterno->putDetalleBoleta($datoscobro);
                            $movimientosexterno->putFechaCobro(cadenaAFecha($fechacobro));
                            $movimientosexterno->registrarmovimiento();
  
                        }else{   
			//ERROR, la cuota de la cuenta indicada no existe
			$fd = fopen ("./cobros/".$nombre_archivo.".log", "a");
			if($fd)
			{
				$linea = date(DATE_ATOM)." ERROR: L�nea ".$nro_linea.". La cuenta/cuota indicada no existe. Se omite la l�nea.\n";
				fwrite($fd, $linea);
			}
			fclose ($fd);
			return false;
                        }
		}
		
		return $result;
	}

//---------------------------------------------------------------------------------------
	public function datoscobro($parcuota)
	{
		$cuota = new modelocuota();
		$cuota->putIdCuota($parcuota);
		$existe = $cuota->traercuota();
		if($existe)
		{
			$monto = $cuota->getMontoCuota();
			$fechavenc = fechaACadena($cuota->getFechaVenc());
			$fechadeposito = date('d/m/Y');
			$interesxmora = $cuota->getInteresMora();

			if(isset($_GET['fechapago']))
				$fechadeposito = $_GET['fechapago']; //es la fecha en que el poblador piensa hacer el pago de la cuota

			$arrcuotas[]=array("id"=>$cuota->getIdCuota(),
								"nrocuota"=>$cuota->getNroCuota(),
								"montoparcial"=>$cuota->getMontoCuota(),
								"fechapago"=>$fechadeposito,
								"montocuota"=>$cuota->getMontoCuota(),
								"cobrototal"=>$cuota->getCobrado(),
								"intmora"=>$interesxmora,
								"diasmora"=>0
								);
		}
		return($arrcuotas);
	}

//---------------------------------------------------------------------------------------
	
	public function diferir($parCobro)
	{
		$parCobro->traercobro();
		$this->cargavariables($parCobro);
		$this->diferirfechas($parCobro);
		$nuevaobservacion = "\n".$_GET["observacionant"]."\n".date('d/m/Y')." Difiri� vencimientos. ".$_GET["observacion"]." [".$_SESSION['s_username']."]";
		$cuenta = new ModeloCuenta();
		$cuenta->putIdCuenta($_GET["idcuenta"]);
		$cuenta->putObservacion($nuevaobservacion);
		$cuenta->guardarobservacion();
		$data['controlador'] = "cobro";
		$data['accion'] = "diferirvencimientos&&idcuenta=".$_GET["idcuenta"];
		$this->view->show1("bridgecustom.html",$data);
	}

//----------------------------------------------------------------------------------

	public function cargavariables($clasecarga)
	//Carga las variables del html para volcarlas en la tabla
	{
		//carga las variables de la clase
		$clasecarga->putIdCuenta($_GET["idcuenta"]);
		$clasecarga->putPrimerVenc(cadenaAFecha($_GET["fechadiferir"]));
		$clasecarga->putCuotaDiferir($_GET["cuotadesde"]);
	}

//==============================================================================================

	public function diferirfechas($cuenta)
	{
		$cuotas = new modelocuota();
		$cuotadesde = $cuenta->getCuotaDiferir()*1;
		$cuotas->putIdCuenta($cuenta->getIdCuenta());
		$arrcuotas = $cuotas->listadoTotal();
		$totalRows = count($arrcuotas);

		if ( $totalRows != 0)
		{
			$fechavencimiento = $cuenta->getPrimerVenc();
			$cantmeses = $cuenta->getCantMeses();
			$cantcuotas = $cuenta->getCantCuotas();
			foreach($arrcuotas as $row)
			{
				$cuotas->putIdCuota($row['id']);
				$cuotas->traercuota();
				$nrocuota = $cuotas->getNroCuota()*1;
				if($nrocuota >= $cuotadesde)
				{
					//si la fecha de calculo de mora es mayor al nuevo vencimiento, le asigno el nuevo vencimiento
					$fec_calculo_mora = $cuotas->getFechaCalculoMora();
					if($fec_calculo_mora!="" && (comparar_fechas($fec_calculo_mora, $fechavencimiento)))
						$cuotas->putFechaCalculoMora($fechavencimiento);
					$cuotas->putFechaVenc($fechavencimiento);
					$cuotas->modificarVencimiento();
					//calcula proxima fecha vencimiento
					if($cantmeses <= 12)
					{
						$fechavencimiento = $this->sumaMes($fechavencimiento, $cantmeses);
					}
				}
			}
		}
		return(true);
	}


//==============================================================================================
   public function sumaDia($fecha,$dia)// suma dia a la fecha
   {
        // suma $dia dias a una fecha y retorna la fecha resultante
    	list($year,$mon,$day) = explode('-',$fecha);
		$nuevafecha=date('Y-m-d',mktime(0,0,0,$mon,$day+$dia,$year));

    	list($year2,$mon2,$day2) = explode('-',$nuevafecha);
		$nuevafecha=date('Y-m-d',mktime(0,0,0,$mon2,$day,$year2));
		return($nuevafecha);
			
	}
//==============================================================================================
   public function sumaMes($fecha,$cant)//suma un mes a la fecha
   {
        // suma $dia dias a una fecha y retorna la fecha resultante
    	list($year,$mon,$day) = explode('-',$fecha);
		$nuevafecha=date('Y-m-d',mktime(0,0,0,$mon+$cant,$day,$year));
    	list($year2,$mon2,$day2) = explode('-',$nuevafecha);
		$nuevafecha=date('Y-m-d',mktime(0,0,0,$mon2,$day,$year2));
		return($nuevafecha);
			
	}

//---------------------------------------------------------------------------------

	public function mostrarCuentaRestarCobro()
	// muestra todos los cuotas en un html con una tabla
	{
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
        $this->view->show1("listarestarcobro.html", $data);
	}

//-------------------------------------------------------------------------------

	public function restarCobroCuota()
	{
		$cuota = new ModeloCuota();

		if (isset($_GET['idcuota']))
		{
			$cuota->putIdCuota($_GET['idcuota']);
			$c_ok = $cuota->traerCuota();
            if (!$c_ok)
			{
				$mensaje = htmlentities("No puede realizar esta operaci�n, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
	    }
		$data = $this->cargarPlantillaDescuento($cuota);
		$this->view->show("restarcobrocuota.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cuentaRestarCobro()
	//muestra la pantalla para realizar un pago manual de las cuotas
	{
		$cuenta = new ModeloCuenta();
		$cuotas = new ModeloCuota();
		if(isset($_GET['idcuenta']) && $_GET['idcuenta'] > 0)
		{
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traerCuenta();
			$cuotas->putIdCuenta($_GET['idcuenta']);
			$liztado = $cuotas->listadoTotal();
			if(!$liztado)
			{
				$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver cuotas, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			$idcuenta = $_GET['idcuenta'];
			$deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
			$deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
			$saldo = $deuda_vencida + $deuda_no_vencida;
			$data['liztado'] = $liztado;
			$data['idcuenta'] = $idcuenta;
			$data['solicitante'] = $cuotas->getSolicitante();
			$data['anioexpediente'] = $cuotas->getAnioExpediente();
			$data['letraexpediente'] = $cuotas->getLetraExpediente();
			$data['nroexpediente'] = $cuotas->getNroExpediente();
			$data['nrocuenta'] = $cuotas->getNroCuenta();
			$data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
			$data['cuentacorriente'] = $cuotas->getCuentaCorriente();
			$data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['deudavencida'] = number_format($deuda_vencida,2,',','.');
			$data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
			$data['saldo'] = number_format($saldo,2,',','.');
			$data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
			$data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
			$data['observacionant'] = $cuenta->getObservacion();
		}else{
			$mensaje = htmlentities("No se seleccion� correctamente la cuenta, int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->view->show1("cuentarestarcobro.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cargarPlantillaDescuento($parCuota)
	{
		$nombreboton="Descontar";
		$nombreaccion="descontarcobrocuota";

 		$parametros = array("TITULO"=>"Descontar cobro de",
					"IDCUOTA"=>$parCuota->getIdCuota(),
					"NROCUOTA"=>$parCuota->getNroCuota(),
					"IDCUENTA"=>$parCuota->getIdCuenta(),
					"NROCUENTA"=>$parCuota->getNroCuenta(),
					"SOLICITANTE"=>$parCuota->getSolicitante(),
					"NROCUOTA"=>$parCuota->getNroCuota(),
					"MONTOCUOTA"=>number_format($parCuota->getMontoCuota(),2,',','.'),
					"SALDO"=>number_format($parCuota->getSaldo(),2,',','.'),
					"FECHAVENC"=>fechaACadena($parCuota->getFechaVenc()),
					"INTERES"=>number_format($parCuota->getInteres(),2,',','.'),
					"INTERESMORA"=>number_format($parCuota->getInteresMora(),2,',','.'),
					"COBRADOTOTAL"=>number_format($parCuota->getCobrado(),2,',','.'),
					"COBRADOTOTALSINFORMATO"=>$parCuota->getCobrado(),
					"FECHARESTA"=>date('d/m/Y'),
					"OBSERVACION"=>"Motivo del descuento:   .-",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					);
		return $parametros;
	}

}
?>