<?php 
require_once 'modelos/modelousuario.php';
require_once 'modelos/modeloperfil.php';
require_once 'modelos/modeloempleado.php';
require_once 'modelos/modelomenu.php';
require_once 'modelos/modelodepartamento.php';




class controlusuario
{



//============================================================================

	function __construct()
	{
	    //Creamos una instancia de nuestro mini motor de plantillas
	    $this->view = new View();
	}

//============================================================================
   public function loginusuario()
   {   

    	include('vista/login.php');
		$modusuario = new modelousuario();
        
		
		$login = $_POST['username'];
		$modusuario->putUsuario($login);
		
          $clave = $_POST['password']; 
        
        if($clave!='' )  {
	      	//Le pedimos al modelo todos los items
	    	$datousuario = $modusuario->traerusuario();
	         
	         if($datousuario->clave != $clave )  {
		          return false;
                }
		     else{
		       if($clave==md5(trim($datousuario->usuario))){
			      $_SESSION["cambiaclave"] =true;
			   }
			   else{
			      $_SESSION["cambiaclave"] =false;
			   }  
	        	 $acceso = $datousuario->acceso;
		         $_SESSION["s_username"] = $datousuario->usuario;
		         $_SESSION["s_idusr"] = $datousuario->id;
		         $_SESSION["s_iddel"] = $datousuario->iddelegacion;
                 $_SESSION["acceso"] = $acceso;
			 $modusuario->putIdUsuario($datousuario->id);	 
			 $arrayperfil=$modusuario->traerperfil();
	         $_SESSION["perfilusuario"]=$arrayperfil;
		     //Pasamos a la vista toda la informaci�n que se desea representar
		     //$data['datosusuario'] = $datousuario;

			 return($arrayperfil);
             
			 }
            
			 
		}
		else{
		 return true;
    	}
  
    
   }
   
//============================================================================

//---------------------------------------------------------------------------------
// muestra todas las usuarios en un html con una tabla


	 
	public function mostrarusuario()
	{
	$usuarios = new modelousuario();
 

	$liztado = $usuarios->listadoTotal();
        
      
	$data['liztado'] = $liztado;
	
	
	$this->view->show1("usuario.html", $data);
       
			
 	}	 

//-------------------------------------------------------------------------------	
//retorna los datos de una usuario si se carg� el id y carga la pantalla ABM
	
	public function verusuario()
	{
		
	
	$usuarios = new modelousuario();
    if (isset($_GET['idusr'])) { //si es modificacion o eliminacion			
    
	
	
           $usuarios->putIdUsuario($_GET['idusr']);
	
	       $usr = $usuarios->traerdatosusuario();
	
	        if (!$usr){
	          echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
  	         $this->mostrarusuario();  
	         return;
            }
	
    }
	$perfil=new modeloperfil();
	$empleado=new modeloempleado();
	$departamento=new modelodepartamento();
	$menu=new modelomenu();
	$data=$this->cargarPlantillaModificar($usuarios,$perfil,$menu,$empleado,$departamento);
					
	
	$this->view->show("abmusuario.html", $data);
				
    
	
	}
	
	

//============================================================================
       public function cargarPlantillaModificar($parUsuario,$parPerfil,$parMenu,$parEmpleado,$parDepartamento) 
{  
    $usuarios = new modelousuario();
    $listausuarios=$usuarios->TraerTodos();
	$listaempleados=$parEmpleado->TraerTodos();
	$listaempleados['selected']=  $parUsuario->getIdEmpleado();
	$listadepartamento=$parDepartamento->TraerTodos();
	$listadepartamento['selected']=$parUsuario->getIdDepartamento();
		
	if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altausuario";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificarusuario";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrarusuario";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:
				$idusr=$parUsuario->getIdUsuario();

	      $parametros = array(
                    "TITULO" =>  "ADMINISTRACION USUARIOS",
                    "ID" => $parUsuario->getIdUsuario(),
					"NOMBRECOMPLETO" => $parUsuario->getNombreCompleto(), 
					"USUARIO" => $parUsuario->getUsuario(),
					"CLAVE" => $parUsuario->getClave(),
					"IDEMPLEADO"=> $parUsuario->getIdEmpleado(),
					"LISTAEMPLEADOS"=>$listaempleados,
					"LISTADEPARTAMENTO"=>$listadepartamento,
                    "IDDEPARTAMENTO"=>$parUsuario->getIdDepartamento(),
                    "LISTAUSUARIOS"=>$listausuarios,
                    "LISTAMENUDISPONIBLE" => $parMenu->TraerTodosMenu("iditems not in(select iditems from perfiles where perfiles.idusuario='$idusr')"),
					"LISTAMENUELEGIDO" => $parUsuario->TraerDetalle(), 
					"DISA_MODI"=>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
						$idusr=$parUsuario->getIdUsuario();

		  $parametros = array(
                    "TITULO" =>  "ADMINISTRACION USUARIOS",
                    "ID" => $parUsuario->getIdUsuario(),
					"NOMBRECOMPLETO" => $parUsuario->getNombreCompleto(), 
					"LISTADEPARTAMENTO"=>$listadepartamento,
                    "IDDEPARTAMENTO"=>$parUsuario->getIdDepartamento(),
					"USUARIO" => $parUsuario->getUsuario(),
					"CLAVE" => $parUsuario->getClave(),
					"IDEMPLEADO"=> $parUsuario->getIdEmpleado(),
					"LISTAEMPLEADOS"=>$listaempleados,
                    "LISTAUSUARIOS"=>$listausuarios,

                    "LISTAMENUDISPONIBLE" => $parMenu->TraerTodosMenu("iditems not in(select iditems from perfiles where perfiles.idusuario='$idusr')"),
					"LISTAMENUELEGIDO" => $parUsuario->TraerDetalle(), 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
"TITULO" =>  "ADMINISTRACION USUARIOS",
                    "ID" => $parUsuario->getIdUsuario(),
					"NOMBRECOMPLETO" => $parUsuario->getNombreCompleto(), 
					"LISTADEPARTAMENTO"=>$listadepartamento,
                    "IDDEPARTAMENTO"=>$parUsuario->getIdDepartamento(),
					"USUARIO" => $parUsuario->getUsuario(),
					"CLAVE" => $parUsuario->getClave(),
					"IDEMPLEADO"=> $parUsuario->getIdEmpleado(),
					"LISTAEMPLEADOS"=>$listaempleados,
                    "LISTAUSUARIOS"=>$listausuarios,

                    "LISTAMENUDISPONIBLE" => $parMenu->TraerTodosMenu(""),
					"LISTAMENUELEGIDO" => $parUsuario->TraerDetalle(), 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :
						$idusr=$parUsuario->getIdUsuario();

		 $parametros = array(
  "TITULO" =>  "ADMINISTRACION USUARIOS",
                    "ID" => $parUsuario->getIdUsuario(),
					"NOMBRECOMPLETO" => $parUsuario->getNombreCompleto(), 
					"USUARIO" => $parUsuario->getUsuario(),
					"CLAVE" => $parUsuario->getClave(),
					"IDEMPLEADO"=> $parUsuario->getIdEmpleado(),
					"LISTAEMPLEADOS"=>$listaempleados,
					                    "LISTAUSUARIOS"=>$listausuarios,
					"LISTADEPARTAMENTO"=>$listadepartamento,
                    "IDDEPARTAMENTO"=>$parUsuario->getIdDepartamento(),
                    "LISTAMENUDISPONIBLE" => $parMenu->TraerTodosMenu("iditems not in(select iditems from perfiles where perfiles.idusuario='$idusr')"),
					"LISTAMENUELEGIDO" => $parUsuario->TraerDetalle(), 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }

//============================================================================
       public function cargarPlantillaModClave($parUsuario) 
{  
    $usuarios = new modelousuario();
	
		

    $nombreboton="Guardar";
	$nombreaccion="modificarclave";

  
	$idusr=$parUsuario->getIdUsuario();

	      $parametros = array(
                    "TITULO" =>  "ADMINISTRACION USUARIOS",
                    "ID" => $parUsuario->getIdUsuario(),
					"USUARIO" => $parUsuario->getUsuario(),
					"CLAVE" => $parUsuario->getClave(),
					"DISA_MODI"=>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	  

        return $parametros;
  }


//============================================================================
   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdUsuario($_POST["id"]);
		}
        $clasecarga->putUsuario($_POST["usuario"]);
        $clasecarga->putClave($_POST["clave"]);
        $clasecarga->putIdDepartamento($_POST["iddepartamento"]);
        $clasecarga->putIdEmpleado($_POST["idempleado"]);
        $clasecarga->putNombreCompleto($_POST["nombrecompleto"]);
          if(!isset($_POST["vwMenuElegido"])){ 
	       	$_POST["vwMenuElegido"]=array();
		
		   }
	    $clasecarga->putPerfil(($_POST["vwMenuElegido"]));
   
   }


	
	
	
	
	
	
// --------------------------------------------------------------------------
	
	
	
	public function altausuario()
	{
	   $alta= new modelousuario();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altausuario();
	   if (!$altaok){
	     echo "No se pudo agregar el Usuario, o no se pudo dar de alta el Perfil Seleccionado";
        }
	    $this->mostrarusuario();
		 
	}
// --------------------------------------------------------------------------
	public function modificarusuario()
	{
		   
       $modifica= new modelousuario();
	   
	    $this->cargavariables($modifica,MODIFICAR);
		
	     $modificado=$modifica->modificarusuario();
        
	   if (!$modificado){
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	    $this->mostrarusuario();
			
	}
// --------------------------------------------------------------------------
	public function modificarclave()
	{
		   
       $modifica= new modelousuario();
	   
	    $this->cargavariables($modifica,MODIFICAR);
		
	     $modificado=$modifica->modificarclave();
        
	   if (!$modificado){
	     $data['resultado']="ATENCION : No se pudo realizar el cambio de clave";
		 $data['mensaje']="Contactese con el Administrador de Usuarios";
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	 	else{
		   $data['resultado']="El cambio de clave se realizo con exito";
	       $data['mensaje']="Salga del Sistema y vuelva a ingresar";
	
		}
	   $this->view->show1("bridgereingreso.html", $data);
    		
	}
	
	
// --------------------------------------------------------------------------
	
	public function borrarusuario()
	{
	 
       $borra= new modelousuario();
	   $borra->putIdUsuario($_POST['id']);
	  
	   $borrado=$borra->borrarusuario();
       if (!$borrado){
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	    $this->mostrarusuario();
		 
	}

//============================================================================
   public function logoutusuario()
   {   
     session_start(); 

     $_SESSION = array(); 

      session_destroy(); 

    	
    $data="";
	$this->view->show("bridge.html", $data);
    
   }


//-------------------------------------------------------------------------------	
//retorna los datos de una usuario si se carg� el id y carga la pantalla ABM
	
	public function cambiarclave()
	{
		
	
	$usuarios = new modelousuario();
    if (isset($_SESSION["s_idusr"])) { //si es modificacion o eliminacion			
      
	
	
           $usuarios->putIdUsuario($_SESSION["s_idusr"]);
	
	       $usr = $usuarios->traerdatosusuario();
	
	        if (!$usr){
	          echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
  	         
	         return;
            }
	
    }
	
	$menu=new modelomenu();
	$data=$this->cargarPlantillaModClave($usuarios);
					
	
	$this->view->show("cambioclave.html", $data);
				
    
	
	}




} 
   
?>
