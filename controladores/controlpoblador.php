<?php
require_once 'modelos/modelopoblador.php';
require_once 'modelos/modelotipodoc.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modeloestadocivil.php';
require_once 'modelos/modeloextranjero.php';
require_once 'controladores/controlextranjero.php';



class ControlPoblador
{
  	function __construct()
	{

	    $this->view = new View();
	}
 
//============================================================================
	 
	public function mostrarpoblador()
	// envia a la vista un listado de todos los pobladores y su localidad
	{
		$pobladores = new modelopoblador();
	 
		$liztado = $pobladores->listadototal();
	      
		$data['liztado'] = $liztado;
		
		$this->view->show1("poblador.html", $data);
 	}
 	
//============================================================================

	public function listapoblador()
	//lista los pobladores con un mismo apellido, sino recibe apellido lista todos los pobladores
	{
		$pobladores = new modelopoblador();

		if (isset($_POST['apellido']))
	    {
			$pobladores->putApellido($_POST['apellido']);
			$liztado = $pobladores->listadonombre();
		} else {
			$liztado = $pobladores->listadototal();
		}   

		$data['liztado'] = $liztado;
		
		$this->view->show1("poblador.html", $data);
	}	
	
//============================================================================
	
	public function elegirpoblador()
	// envia a la vista los pobladores elegidos segun el apellido.
	{
		$pobladores = new modelopoblador();
		
	    if (isset($_GET['nombrepob']))
			$pobladores->putApellido($_GET['nombrepob']);
		else
			$pobladores->putApellido("");
		 
	    $liztado = $pobladores->listadonombre();

	    $data['liztado'] = $liztado;
		
		$this->view->show1("elegirpoblador.html", $data);
	}
	
//============================================================================
	
	public function verpoblador()
	//retorna los datos de una poblador si se carg� el id, sino retorna campos en blanco
	{	
		$pobladores = new modelopoblador();

		if (isset($_GET['idpob'])) //si es modificacion o eliminacion
		{			
		    $pobladores->putIdPoblador($_GET['idpob']);
			$empent = $pobladores->traerpoblador();
			
			if (!$empent)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
	    }   
		$tipodoc = new modelotipodoc;
		$localidad = new modelolocalidad;
		$estadocivil = new modeloestadocivil;
		$data = $this->cargarPlantillaModificar($pobladores, $tipodoc, $localidad, $estadocivil);
		$this->view->show("abmpoblador.html", $data);
	}

//============================================================================
	
	public function vertabpoblador()
	//retorna los datos de una poblador si se carg� el id, sino retorna campos en blanco
	{	
		$pobladores = new modelopoblador();

		if (isset($_GET['idpob'])) //si es modificacion o eliminacion
		{			
		    $pobladores->putIdPoblador($_GET['idpob']);
			$empent = $pobladores->traerpoblador();
			
			if (!$empent)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
	    }  

		$tipodoc = new modelotipodoc;
		$localidad = new modelolocalidad;
		$estadocivil = new modeloestadocivil;
		$data = $this->cargarPlantillaModificar($pobladores, $tipodoc, $localidad, $estadocivil);
		$this->view->show("tabpoblador.html", $data);
	    
	}
	
//============================================================================

	public function altapoblador()
	// carga el nuevo poblador en el modelo
	{
		$alta = new modelopoblador();
		$this->cargavariables($alta, ALTA);
		if ($alta->existedocumento())
		{
			$mensaje = htmlentities("El n�mero de documento ingresado ya pertenece a otro poblador");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$result = $alta->altapoblador();
		if (!$result)
		{	
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
        }
        // si vienen cargados datos de extranjero
		if (isset($_POST['carta']) || isset($_POST['fechacarta']) || isset($_POST['juzgado']) || isset($_POST['visadorpor']) || isset($_POST['fechallegada']) || isset($_POST['procedencia']) || isset($_POST['medio']))
		{
			$ctrl_extranjero = new ControlExtranjero();
			$ext_ok = $ctrl_extranjero->altaextranjero($result);
			if (!$ext_ok)
			{	
				$mensaje = htmlentities("No se pudo completar la operaci�n, compruebe los resultados e int�ntelo nuevamente m�s tarde");
				$alta->putIdPoblador($result);
				$alta->borrarpoblador();
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
	        }
		}
        $vista = new view();
        $data['controlador'] = "poblador";
        $data['accion'] = "vertabpoblador&&idpob=".$result;
		$vista->show1("bridgecustom.html",$data);	
	}

//============================================================================	

	public function modificarpoblador()
	{
		$modifica = new modelopoblador();
		$this->cargavariables($modifica, MODIFICAR);
		if ($modifica->existedocumento())
		{
			$mensaje = htmlentities("El n�mero de documento ingresado ya pertenece a otro poblador");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$modificado = $modifica->modificarpoblador();
		if (!$modificado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		// si vienen cargados datos de extranjero
		if (isset($_POST['carta']) || isset($_POST['fechacarta']) || isset($_POST['juzgado']) || isset($_POST['visadorpor']) || isset($_POST['fechallegada']) || isset($_POST['procedencia']) || isset($_POST['medio']))
		{
			$ctrl_extranjero = new ControlExtranjero();
			if ($_POST['idextranjero'] > 0)
				$mod_ok = $ctrl_extranjero->modificarextranjero();
			else
				$mod_ok = $ctrl_extranjero->altaextranjero($modifica->getIdPoblador());
			if (!$mod_ok)
			{	
				$mensaje = htmlentities("No se pudo completar la operaci�n, compruebe los resultados e int�ntelo nuevamente m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
	        }
		}
		$vista = new view();
        $data['controlador'] = "poblador";
        $data['accion'] = "vertabpoblador&&idpob=".$_POST['idpob'];
		$vista->show1("bridgecustom.html",$data);	
	}

//============================================================================

	public function borrarpoblador()
	{
		$borra = new modelopoblador();
		// si es extranjero borro tambien el extranjero
		$extranjero = new ModeloExtranjero();
		$extranjero->putIdPoblador($_POST['idpob']);
		$extranjero->traerExtranjeroAsociado();
		if ($extranjero->getIdExtranjero() > 0)
		{
			$ext_borrado = $extranjero->borrarextanjero();
			if (!$ext_borrado)
			{
				$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
		}
		$borra->putIdPoblador($_POST['idpob']);
		$borrado = $borra->borrarpoblador();
		if (!$borrado)
		{
			$mensaje = htmlentities("No se pudo completar la operaci�n, compruebe los resultados e int�ntelo nuevamente m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarpoblador();
	}

//============================================================================

	public function cargarPlantillaModificar($parPoblador, $parTipodoc, $parLocalidad, $parEstadoCivil) 
	{  
		$vd = $parTipodoc->TraerTodos();
		$vd['selected'] = $parPoblador->getIdTipodoc();
		$vl = $parLocalidad->TraerTodosL();
		$vl['selected'] = $parPoblador->getIdLocalidad();
		$ve = $parEstadoCivil->TraerTodos();
		$ve['selected'] = $parPoblador->getIdEstadoCivil();

		$idpob = $parPoblador->getIdPoblador();
		
		$quehacer = "";
		if ($idpob == 0)
			$quehacer = ALTA;
		else
			if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
		
		switch($quehacer)
		{
			case ALTA:	      
	        $nombreboton="Guardar";
		    $nombreaccion="altapoblador";
			break;	 
			
			case MODIFICAR:
			$nombreboton="Guardar";
			$nombreaccion="modificarpoblador";
			break;
			
			case BAJA:
			$nombreboton="Eliminar";
			$nombreaccion="borrarpoblador";  
			break;

			//por defecto tambien es MODIFICAR
			default:  
			$nombreboton="Guardar";
			$nombreaccion="modificarpoblador";
		}
	 
		switch ($quehacer)
		{
			case MODIFICAR:
				$parametros = array(
                    "TITULO" => "Datos",
					"IDOPERACION" => 2,
					"IDPOBLADOR" => $parPoblador->getIdPoblador(), /* variable id del poblador que alcanza a las tres pesta�as */				
                    "ID" => $parPoblador->getIdPoblador(),
					"IDTIPODOC" => $parPoblador->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parPoblador->getDocumento(),
					"NOMBRE" => $parPoblador->getNombres(),
					"APELLIDO" => $parPoblador->getApellido(),
					"DOMICILIOLEGAL" => $parPoblador->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parPoblador->getDomicilioPostal(),
					"NACIONALIDAD" => $parPoblador->getNacionalidad(),
					"LUGARNACIMIENTO" => $parPoblador->getLugarNac(),
					"FECHANAC" => fechaACadena($parPoblador->getFechaNac()),
					"NOMBREPADRE" => $parPoblador->getNombrePadre(),
					"NOMBREMADRE" => $parPoblador->getNombreMadre(),
					"VIVEPADRE" => $parPoblador->getVivePadre(),
					"VIVEMADRE" => $parPoblador->getViveMadre(),
					"NACPADRE" => $parPoblador->getNacionalidadPadre(),
					"NACMADRE" => $parPoblador->getNacionalidadMadre(),
					"TELEFONO" => $parPoblador->getTelefono(),
					"CARACTERISTICA" => $parPoblador->getCaracTel(),
					"IDCONYUGE" => $parPoblador->getIdConyuge(),
					"IDLOCALIDAD" => $parPoblador->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => $parPoblador->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => "",
				);
        			break;

        case BAJA:
			$parametros = array(
					"TITULO" => "Eliminar",
					"IDOPERACION" => 3,
					"IDPOBLADOR" => $parPoblador->getIdPoblador(), /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => $parPoblador->getIdPoblador(),
					"IDTIPODOC" => $parPoblador->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parPoblador->getDocumento(),
					"NOMBRE" => $parPoblador->getNombres(),
					"APELLIDO" => $parPoblador->getApellido(),
					"DOMICILIOLEGAL" => $parPoblador->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parPoblador->getDomicilioPostal(),
					"NACIONALIDAD" => $parPoblador->getNacionalidad(),
					"LUGARNACIMIENTO" => $parPoblador->getLugarNac(),
					"FECHANAC" => fechaACadena($parPoblador->getFechaNac()),
					"NOMBREPADRE" => $parPoblador->getNombrePadre(),
					"NOMBREMADRE" => $parPoblador->getNombreMadre(),
					"VIVEPADRE" => $parPoblador->getVivePadre(),
					"VIVEMADRE" => $parPoblador->getViveMadre(),
					"NACPADRE" => $parPoblador->getNacionalidadPadre(),
					"NACMADRE" => $parPoblador->getNacionalidadMadre(),
					"TELEFONO" => $parPoblador->getTelefono(),
					"CARACTERISTICA" => $parPoblador->getCaracTel(),
					"IDCONYUGE" => $parPoblador->getIdConyuge(),
					//TODO traer solamente la localidad y no la lista e ID
					"IDLOCALIDAD" => $parPoblador->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
				    //TODO traer solamente el estado civil y no la lista e ID
					"IDESTADOCIVIL" => $parPoblador->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,					
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => "",
					);
					break;
		 
		case ALTA:
		 	$parametros = array(
					"TITULO" => "Nuevo",
		 			"IDOPERACION" => 1,
		 			"IDPOBLADOR" => 0, /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => 0,
					"IDTIPODOC" => 0, 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> "",
					"NOMBRE" => "",
					"APELLIDO" => "",
					"DOMICILIOLEGAL" => "",
					"DOMICILIOPOSTAL" => "",
					"NACIONALIDAD" => "",
					"LUGARNACIMIENTO" => "",
		 			"FECHANAC" => "",
					"NOMBREPADRE" => "",
					"NOMBREMADRE" => "",
					"VIVEPADRE" => 0,
					"VIVEMADRE" => 0,
					"NACPADRE" => "",
					"NACMADRE" => "",
					"TELEFONO" => "",
					"CARACTERISTICA" => "",
		 			"IDCONYUGE" => 0,
					"IDLOCALIDAD" => 0,
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => 1,
					"LISTAESTADOCIVIL" => $ve,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
		 			"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => "",
					);
					break;

		// por defecto tambien es MODIFICAR
		default:
			$parametros = array(
                    "TITULO" => "Datos",
					"IDOPERACION" => 2,
					"IDPOBLADOR" => $parPoblador->getIdPoblador(), /* variable id del poblador que alcanza a las tres pesta�as */				
                    "ID" => $parPoblador->getIdPoblador(),
					"IDTIPODOC" => $parPoblador->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parPoblador->getDocumento(),
					"NOMBRE" => $parPoblador->getNombres(),
					"APELLIDO" => $parPoblador->getApellido(),
					"DOMICILIOLEGAL" => $parPoblador->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parPoblador->getDomicilioPostal(),
					"NACIONALIDAD" => $parPoblador->getNacionalidad(),
					"LUGARNACIMIENTO" => $parPoblador->getLugarNac(),
					"FECHANAC" => fechaACadena($parPoblador->getFechaNac()),
					"NOMBREPADRE" => $parPoblador->getNombrePadre(),
					"NOMBREMADRE" => $parPoblador->getNombreMadre(),
					"VIVEPADRE" => $parPoblador->getVivePadre(),
					"VIVEMADRE" => $parPoblador->getViveMadre(),
					"NACPADRE" => $parPoblador->getNacionalidadPadre(),
					"NACMADRE" => $parPoblador->getNacionalidadMadre(),
					"TELEFONO" => $parPoblador->getTelefono(),
					"CARACTERISTICA" => $parPoblador->getCaracTel(),
					"IDCONYUGE" => $parPoblador->getIdConyuge(),
					"IDLOCALIDAD" => $parPoblador->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => $parPoblador->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => "",
					);
		} 				
        return $parametros;
	}


//============================================================================

	public function cargavariables($clasecarga, $op)
	//carga las variables de la clase en el alta o en la modificacion
	{
		if ($op == MODIFICAR)
			$clasecarga->putIdPoblador($_POST["idpob"]);

        $clasecarga->putIdTipodoc($_POST["idtipodoc"]);
        $clasecarga->putDocumento($_POST["documento"]);
        $clasecarga->putNombres(strtoupper($_POST["nombres"]));
        $clasecarga->putApellido(strtoupper($_POST["apellido"]));
        $clasecarga->putNacionalidad(strtoupper($_POST["nacionalidad"]));
        $clasecarga->putLugarNac(strtoupper($_POST["lugarnacimiento"]));
        $clasecarga->putFechaNac(cadenaAFecha($_POST["fechanac"]));
        $clasecarga->putNombrePadre(strtoupper($_POST["nombrepadre"]));
        $clasecarga->putNombreMadre(strtoupper($_POST["nombremadre"]));
        $clasecarga->putVivePadre($_POST["vivepadre"]);
        $clasecarga->putViveMadre($_POST["vivemadre"]);
        $clasecarga->putNacionalidadPadre(strtoupper($_POST["nacpadre"]));
        $clasecarga->putNacionalidadMadre(strtoupper($_POST["nacmadre"]));
        $clasecarga->putTelefono($_POST["telefono"]);
        $clasecarga->putCaracTel($_POST["caracteristica"]);
        $clasecarga->putDomicilioPostal(strtoupper($_POST["domiciliopostal"]));
        $clasecarga->putDomicilioLegal(strtoupper($_POST["domiciliolegal"]));
		$clasecarga->putIdLocalidad($_POST["idlocalidad"]);
		$clasecarga->putIdEstadoCivil($_POST["idestadocivil"]);

		if($_POST["idconyuge"] == 0)
			$clasecarga->putIdConyuge('NULL');
		else
			$clasecarga->putIdConyuge($_POST["idconyuge"]);	     
	}

//=================================================================================================	 
	
	public function verificardocumento()
	// verifica que el numero de documento ingresado no se repita en el modelo
	{
		$poblador = new modelopoblador();
		$poblador->putDocumento("");

		if (isset($_GET['doc']) && !empty($_GET['doc']))
			$poblador->putDocumento($_GET['doc']);

	    $existe = $poblador->existedocumento();
	    
	    if ($existe)
	    	echo "CUIDADO! Ya existe un poblador con ese n&uacute;mero de documento";
	    
	    return;
	}	

}

?>