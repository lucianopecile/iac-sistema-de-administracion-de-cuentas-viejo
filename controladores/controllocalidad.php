<?php

require_once 'modelos/modelolocalidad.php';




class controllocalidad
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con las localidades exitentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrarlocalidad()
    {
        $localidades = new modelolocalidad();
        $liztado = $localidades->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("localidad.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function verlocalidad()
	{

    $localidades = new modelolocalidad();
   
    if (isset($_GET['idloc'])) { 
       $localidades->putIdLocalidad($_GET['idloc']);
	 
	   $locent=$localidades->traerlocalidad();
       if (!$locent){
	          $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
       }
	}   

	$data=$this->cargarPlantillaModificar($localidades);
	$this->view->show("abmlocalidad.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altalocalidad()
	{
	   $alta= new modelolocalidad();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altalocalidad();
	   if (!$altaok){
	         $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	   else{	
	   $this->view->show1("bridge.html","");	
	   }
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	public function modificarlocalidad()
	{
		   
       $modifica= new modelolocalidad();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificarlocalidad();
        
	   if (!$modificado){
	         $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrarlocalidad();
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function borrarlocalidad()
	{
	 
       $borra= new modelolocalidad();
	   $borra->putIdLocalidad($_POST['id']);
	   $borrado=$borra->borrarlocalidad();
       if (!$borrado){
	          $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrarlocalidad();
		 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($parLocalidad) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	
	   $listacomarcas = $parLocalidad->traercomarcas();
       $listacomarcas['selected']=$parLocalidad->getDepende();
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altalocalidad";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificarlocalidad";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrarlocalidad";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Localidad",
                    "ID" => $parLocalidad->getIdLocalidad(),
					"DESCRIPCION" => $parLocalidad->getDescripcion(),
					"CODPOS" => $parLocalidad->getCodPos(),
					"DEPENDE" => $parLocalidad->getDepende(),
					"LISTACOMARCAS"=>$listacomarcas,
					
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminando Localidad",

                   "ID" => $parLocalidad->getIdLocalidad(),
					"DESCRIPCION" => $parLocalidad->getDescripcion(),
					"CODPOS" => $parLocalidad->getCodPos(),
					"DEPENDE" => $parLocalidad->getDepende(),
					"LISTACOMARCAS"=>$listacomarcas,
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de Localidad",
                    "ID" => 0,
					"DESCRIPCION" => "",
					"CODPOS" => 0,
					"DEPENDE" => 0,
					"LISTACOMARCAS"=>$listacomarcas,

                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $parLocalidad->getIdLocalidad(),
					"DESCRIPCION" => $parLocalidad->getDescripcion(),
					"CODPOS" => $parLocalidad->getCodPos(),
					"DEPENDE" => $parLocalidad->getDepende(),
					"LISTACOMARCAS"=>$listacomarcas,
					 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdLocalidad($_POST["id"]);
		}
        $clasecarga->putDescripcion($_POST["descripcion"]);
		
        $clasecarga->putCodPos($_POST["codpos"]);
        $clasecarga->putDepende($_POST["depende"]);
   }

}

?>